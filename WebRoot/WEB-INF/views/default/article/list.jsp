<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>知识管理</title>
	<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>消防知识列表</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<div class="row table-search">
					<div class="col-xs-2">
						<a href="${pageContext.servletContext.contextPath}/article/add.shtml">
							<span class="btn btn-success btn-sm">新增</span>
						</a>
					</div>
					<div class="col-xs-10 text-right">
						<form action="" method="post" class="form-inline">
							<div class="form-group">
								<span>标题:</span>
								<input type="text" class="form-control input-sm" value="${param.title}" name="title" placeholder="标题">
							</div>
							<div class="form-group">
								<span>分类:</span>
								<select class="form-control input-sm" name="categoryId">
									<option value="-1">请选择</option>
									<c:forEach var="ac" items="${articleCategoryList}" varStatus="s">
										<option value="${ac.id}" ${param.categoryId==ac.id?'selected':''}>${ac.name}</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-group">
								<span>状态:</span>
								<select class="form-control input-sm" name="status">
									<option value="-1">请选择</option>
									<option value="1" ${param.status==1?'selected':''}>正常</option>
									<option value="0" ${param.status==0?'selected':''}>禁用</option>
								</select>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-sm">搜索</button>
							</div>
						</form>
					</div>
				</div>
				<table class="table">
					<thead>
					<tr>
						<th>标题</th>
						<th>分类</th>
						<th>新建人</th>
						<th>新建时间</th>
						<th>状态</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="art" items="${page.items}" varStatus="s">
						<tr>
							<td>${art.title}</td>
							<td>${art.categoryName}</td>
							<td>${art.createrName}</td>
							<td><fmt:formatDate pattern="YYYY/MM/dd HH:mm"  value="${art.createTime}" /></td>
							<td>${art.status==1?'正常':'禁用'}</td>
							<td>
								<a href="${ctx}/article/view.shtml?id=${art.id}" class="label label-success">查看</a>
								<a href="${ctx}/article/edit.shtml?id=${art.id}" class="label label-success">编辑</a>
								<a  href="javascript:;"  url="${ctx}/article/delete.shtml?id=${art.id}" data-info="确认删除吗?" class="ajax-get label label-danger">删除</a>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${empty page}">
						<td colspan="6" class="td-center">无相关数据</td>
					</c:if>
					</tbody>
				</table>
				<div class="clearfix">
					<div class="pull-right">
						<mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
					</div>
				</div>
			</div>
		</section>
	</div>
</section>
</body>
</html>