<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
	String ctxPath = pageContext.getRequest().getServletContext().getContextPath();	
    request.setAttribute("ctx", ctxPath);    
%>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    
    <link rel="shortcut icon" href="${ctx}/images/favicon.png" type="image/png">
    
    
    <link rel="stylesheet" href="${ctx}/vendor/font-awesome/css/font-awesome.min.css">    
    <link rel="stylesheet" href="${ctx}/vendor/pace/theme.css">
    
  


    <script src="${ctx}/vendor/jquery.min.js" type="text/javascript"></script>
    <script src="${ctx}/vendor/jquery-extends.js" type="text/javascript"></script>
   
    <script src="${ctx}/vendor/jquery.placeholder.js" type="text/javascript"></script>
    <script src="${ctx}/vendor/jquery.slimscroll.js" type="text/javascript"></script>
    <script src="${ctx}/vendor/pace/pace.min.js" type="text/javascript"></script>
    <script src="${ctx}/vendor/modernizr.js" type="text/javascript"></script>
    
    
   
    
    <!--[if lt IE 9]>
    <script src="${ctx}/vendor/html5shiv.js"></script>
    <script src="${ctx}/vendor/respond.min.js"></script>
    <![endif]-->
    
    <%@include file="/common/include-bootstrap.jsp" %>
    
    <link rel="stylesheet" href="${ctx}/css/animate.min.css">
    <link rel="stylesheet" href="${ctx}/css/main.css">
    <link rel="stylesheet" href="${ctx}/css/palette.css">
    
    
    <script type="text/javascript">
        var webPath = '${ctx}';
        var imagePath = '${imagePath}';
        var ctx = '<%=request.getContextPath() %>';
    </script>
 
 
    <script src="${ctx}/vendor/jquery-art-dialog/art_dialog.js" type="text/javascript"></script>    
    <script src="${ctx}/js/function.js" type="text/javascript"></script>
    
