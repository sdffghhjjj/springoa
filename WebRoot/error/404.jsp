<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page session="false" %>  
<%
	String ctxPath = pageContext.getRequest().getServletContext().getContextPath();	
    request.setAttribute("ctx", ctxPath);    
%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <link rel="shortcut icon" href="${ctx}/images/favicon.png" type="image/png">
  <title>404 PAGE NOT FOUND</title>
  <meta name="description" content="Baiding管理系统">
  <link rel="stylesheet" href="${ctx}/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="${ctx}/css/animate.min.css">
  <link rel="stylesheet" href="${ctx}/vendor/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="${ctx}/css/main.css">
</head>
<body class="bg-white center-wrapper">

<div class="center-content text-center">
  <div class="error-number">404</div>
  <div class="mg-b-lg">PAGE NOT FOUND</div>
  <p>Sorry, but the page you were trying to view does not exist.
    <br>
    <a href="${pageContext.request.contextPath}">Return Home</a>
  </p>
  <ul class="mg-t-lg error-nav">
    <li>
      <a href="javascript:;">&copy;
        <span id="year" class="mg-r-xs"></span>Cameo</a>
    </li>
    <li>
      <a href="javascript:;">About</a>
    </li>
    <li>
      <a href="javascript:;">Help</a>
    </li>
    <li>
      <a href="javascript:;">Status</a>
    </li>
  </ul>
</div>

<script type="text/javascript">
  var el = document.getElementById("year"),  year = (new Date().getFullYear());
  el.innerHTML = year;
</script>
</body>
</html>
