package cn.tanghom.app.system.model;

import java.io.Serializable;

/**
 * 联动选项信息
 *
 * @author tanghom <tanghom@qq.com> 2016-6-7
 */
public class ItemInfo implements Serializable {
    private static final long serialVersionUID = -3863379131613608832L;
    private Long id;
    private String name;//名称
    private String value;//值
    private String groupKey;//分组标识
    private Long groupId;//分组ID
    private Integer sort = 0;//排序
    private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public void setGroupKey(String groupKey) {
        this.groupKey = groupKey;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long grouId) {
        this.groupId = grouId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
