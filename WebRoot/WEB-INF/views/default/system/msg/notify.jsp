<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>消息</title>
	<script src="${pageContext.request.contextPath}/vendor/jquery.min.js" type="text/javascript"></script>
</head>
<body>
<form class="form-horizontal" action="${pageContext.request.contextPath}/msg/notify.shtml" id="form">
	消息内容：<textarea rows="3" cols="50" name="msg"></textarea>
	<input type="button" onclick="sub()" value="发送">
</form>
<script type="text/javascript">
function sub(){
		$.ajax({
			type : "POST",
			data : $("#form").serialize(),
			url : $("#form").attr("action"),
			dataType : 'json',
			success:function(data) {
				alert(data.info);
			}
		});
}
	</script>

</body>
</html>
