package cn.tanghom.support.quartz;

import cn.tanghom.support.utils.LoggerUtils;
import cn.tanghom.support.utils.YunPianSms;

import org.activiti.conf.CommonKey;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * 消息服务器运行状态检测，如果出错，发送短信给配置的手机号码
 */
public class MsgQuartz {

    public void job() {
        String url = CommonKey.application.getProperty("msg_server_url");
        String phone_number = CommonKey.application.getProperty("notify_phone","18613236735");
        System.out.println(url);
        int s = 0;
        String ret = "程序运行异常";
        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        try {
            HttpGet method = new HttpGet(url);
            response = client.execute(method);
            LoggerUtils.info(response);
            int stat = response.getStatusLine().getStatusCode();
            if (stat != HttpStatus.SC_OK) {
                ret = stat+"";
            } else {
                s = 1;
                ret = "程序运行正常";
            }
        } catch (Exception e) {
            ret = e.getMessage();
            e.printStackTrace();
        } finally {
            try {
            	if(response!=null){
            		response.close();
            	}
            } catch (Exception e) {
                ret = e.getMessage();
                e.printStackTrace();
            }
        }
        if(s==0){
            System.out.println(YunPianSms.getUserInfo());
            System.out.println(ret);
            System.out.println(YunPianSms.sendSms(ret,phone_number));
        }
        LoggerUtils.info(ret);
    }

    public static void main(String[] args) {
        new MsgQuartz().job();
    }

}
