package cn.tanghom.support.shiro.filter;

import cn.tanghom.app.system.model.User;
import cn.tanghom.app.system.service.UserService;

import org.activiti.conf.CommonKey;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.springframework.beans.factory.annotation.Autowired;


import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * <p>User: 每次把用户信息放入request中，暂时没用。
 * <p>Date: 15-11-15
 * <p>Version: 1.0
 */
public class SysUserFilter extends PathMatchingFilter {
	@Autowired
	private UserService userService;

    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
    	   String username = (String) SecurityUtils.getSubject().getPrincipal();
           User user = this.userService.findByLoginName(username);
           request.setAttribute(CommonKey.CURRENT_USER, user);
          
    	return true;
    }
}
