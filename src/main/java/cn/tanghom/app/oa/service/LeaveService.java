package cn.tanghom.app.oa.service;


import cn.tanghom.app.oa.model.Leave;
import cn.tanghom.support.page.Pager;
import org.activiti.engine.runtime.ProcessInstance;

import java.util.List;
import java.util.Map;

/**
 * 请假业务实现
 *
 * @author tanghom<tanghom@qq.com> on 2016/6/30
 */
public interface LeaveService {

    /**
     * 保存请假
     *
     * @param leave
     */
    void save(Leave leave);

    /**
     * 更新保存
     *
     * @param leave
     */
    void update(Leave leave);

    /**
     * 根据iD删除请假
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询请假
     *
     * @param id
     * @return
     */
    Leave findById(Long id);

    /**
     * 按条件查询所有请假
     *
     * @return
     */
    Pager<Leave> findPageByParams(String userId, Integer status, Long index);

    ProcessInstance startWorkflow(Leave entity, Map<String, Object> variables);
}
