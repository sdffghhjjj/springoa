package cn.tanghom.app.oa.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import cn.tanghom.app.oa.model.Resume;

@Repository
public interface ResumeRepository extends MongoRepository<Resume, Long> {

    Resume findById(Long id);
    
    List<Resume> findByName(String name); 
    
    Page<Resume> queryFirst10ByName(String name, Pageable pageable);
    
    Page<Resume> findByStatus(int status, Pageable pageable);
}