﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
  <head> 
  	<%@include file="/common/head.jsp"%>  
<title>选取审批人员</title>
<script type="text/javascript">

</script>
</head>
<body>
	<div title="候选人" style="width:986px;height:364px;">
		<div class="easyui-layout" fit="true">
			<div region="west" split="true" style="width:120px;">
				<ul class="easyui-tree">
					<li>
						<span>部门</span>
						<ul>
							<c:choose>
								<c:when test="${empty groupList }">
									暂无部门信息，请联系管理员！
								</c:when>
								<c:otherwise>
									<c:forEach items="${groupList}" var="group">
										<li><a href="javascript:void(0);" onclick="addTab('${group.name}','${group.id }','${taskDefKey }','${multiSelect }');"><span>${group.name}</span></a></li>
									</c:forEach>
								</c:otherwise>
							</c:choose>  
						</ul>
					</li>
				</ul>
			</div>
			<div region="center" border="false" border="false">
				<div id="userTabs" class="easyui-tabs" fit="true">
					<div title="Home" style="padding:10px;">
						<div class="well well-small">
							<span class="badge" iconCls="icon-save" plain="true" id="tishi" title="提示">提示</span>
							<p>
								请点击左侧<span class="label-info"><strong>部门</strong></span>来选择人员，选择人员后点击<span class="label-info"><strong>确认</strong></span>键设定人员！
							</p>
						</div>
					</div>
				</div>
			</div>
			<div region="south" border="false" style="text-align:right;height:30px;line-height:30px;">
				<a class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0)" onclick="set_chooseUser();">确认</a>
				<a class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0)" onclick="destroy_chooseUser('${taskDefKey }');">取消</a>
			</div>
		</div>
	</div>
    
</body>

</html>
