// 图片上传demo
var the_uploader;
$(function () {
    var $ = jQuery,
        $list = $('#fileList'),
    // 优化retina, 在retina下这个值是2
        ratio = window.devicePixelRatio || 1,

    // 缩略图大小
        thumbnailWidth = 100 * ratio,
        thumbnailHeight = 100 * ratio,
        uploader;
    // 初始化Web Uploader
    uploader = WebUploader.create({
        // 自动上传。
        auto: true,
        multi:false, //是否上传多个文件;
        // swf文件路径
        swf: webPath + '/js/Uploader.swf',
        // 文件接收服务端。
        server: webPath + '/file/upload.shtml',
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#filePicker',
        // 只允许选择文件，可选。
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        },
        fileSingleSizeLimit: 1 * 1024 * 1024
    });

    // 当有文件添加进来的时候
    uploader.on('fileQueued', function (file) {
        var $li = $(
                '<div id="' + file.id + '" class="file-item thumbnail">' +
                '<img>' +
                '<div class="info">' + file.name + '</div>' +
                '<a href="javascript:;" class="file-cancel" onclick="delFile(\'' + file.id + '\')">×</a>' +
                '</div>'
            ),
            $img = $li.find('img');

        $list.append($li);

        // 创建缩略图
        uploader.makeThumb(file, function (error, src) {
            if (error) {
                $img.replaceWith('<span>不能预览</span>');
                return;
            }

            $img.attr('src', src);
        }, thumbnailWidth, thumbnailHeight);
    });

    // 文件上传过程中创建进度条实时显示。
    uploader.on('uploadProgress', function (file, percentage) {
        var $li = $('#' + file.id),
            $percent = $li.find('.progress span');

        // 避免重复创建
        if (!$percent.length) {
            $percent = $('<p class="progress"><span></span></p>')
                .appendTo($li)
                .find('span');
        }

        $percent.css('width', percentage * 100 + '%');
    });
    uploader.on('error', function (type) {
        if (type == "F_EXCEED_SIZE") {
            msg.info("文件大小不能超过1M");
        }
    });
    // 文件上传成功，给item添加成功class, 用样式标记上传成功。
    uploader.on('uploadSuccess', function (file, ret) {
        var $li = $('#' + file.id)
        if (0 == ret.status) {
            uploadError(file.id)
            return false;
        }
        $li.append('<input type="hidden" name="fileId[]" value="' + ret.id + '"/>');
        $('#' + file.id).addClass('upload-state-done');
    });

    // 文件上传失败，现实上传出错。
    uploader.on('uploadError', function (file) {
        uploadError(file.id)
    });

    function uploadError(fileId) {
        var $li = $('#' + fileId),
            $error = $li.find('div.error');

        // 避免重复创建
        if (!$error.length) {
            $error = $('<div class="error"></div>').appendTo($li);
        }

        $error.text('上传失败');
    }

    // 完成上传完了，成功或者失败，先删除进度条。
    uploader.on('uploadComplete', function (file) {
        $('#' + file.id).find('.progress').remove();
    });
    the_uploader = uploader;
});
function delFile(fileId) {
    var $li = $('#' + fileId);
    var id = $li.find("[name='fileId[]']").val();
    $.ajax({
        type: "post",
        dataType: "json",
        data: {id: id},
        url: webPath + "/file/del.shtml",
        success: function (data) {
            if (1 == data) {
                var file = the_uploader.getFile(fileId);
                if (typeof(file) != "undefined") {
                    the_uploader.removeFile(file);
                }
                $li.remove();
            } else {
                alert("删除失败，请重试");
            }
        }
    });
}