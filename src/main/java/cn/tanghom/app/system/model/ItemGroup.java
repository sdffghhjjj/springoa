package cn.tanghom.app.system.model;

import java.io.Serializable;

/**
 * 联动选项分组
 *
 * @author tanghom <tanghom@qq.com> 2016-6-7
 */
public class ItemGroup implements Serializable {
    private static final long serialVersionUID = -513061469641344833L;
    private Long id;
    private String groupKey;//唯一标识
    private String name;//名称
    private String remark;//描述
    private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public void setGroupKey(String groupKey) {
        this.groupKey = groupKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
