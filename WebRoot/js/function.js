$(function () {
//ajax get请求
    $('.ajax-get').click(function () {
        var target = $(this).attr('url');
        var info = $(this).attr('data-info');
        msg.confirm(info, function () {
            if (target) {
                $.ajax({
                    type: "GET",
                    url: target,
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 1) {
                            msg.info(data.info);
                            setTimeout(function () {
                                if (data.url) {
                                    location.href = data.url;
                                } else {
                                    location.reload();
                                }
                            }, 1000);
                        } else {
                            msg.info(data.info);
                        }
                    }
                });
            }
        }, "操作提示");
        return false;
    });
});
//验证信息
//验证空信息
function checkEmpty(obj,msg){
    var check = false;
    var val = obj.val();
    if(val == ""){
        obj.err(msg);
    }else{
        check = true;
        obj.closeErr();
    }
    return check;
}