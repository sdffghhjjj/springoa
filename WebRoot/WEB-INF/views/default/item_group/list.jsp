<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>选项分类管理</title>
	<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">

		<section class="panel panel-default">
			<div class="panel-heading">
				<b>选项分类列表</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<div class="row table-search">
					<div class="col-xs-2">
						<a href="${pageContext.servletContext.contextPath}/item/add.shtml">
							<span class="btn btn-success btn-sm">新增</span>
						</a>
					</div>
					<div class="col-xs-10 text-right">
						<form action="" method="post" class="form-inline">
							<div class="form-group">
								<span>名称:</span>
								<input type="text" class="form-control input-sm" value="${param.name}" name="name" placeholder="名称">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-sm">搜索</button>
							</div>
						</form>
					</div>
				</div>
				<table class="table">
					<thead>
					<tr>
						<th>名称</th>
						<th>注释</th>
						<th>状态</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="ac" items="${articleCategoryList}">
						<tr>
							<td>${ac.name}</td>
							<td>${ac.remark}</td>
							<td>${ac.status==1?'启用':'禁用'}</td>
							<td>
								<a href="${pageContext.servletContext.contextPath}/item/item_list.shtml?groupKey=${ac.groupKey}" class="label label-success">查看</a>
								<a href="${pageContext.servletContext.contextPath}/item/edit.shtml?id=${ac.id}" class="label label-success">编辑</a>								
							</td>
						</tr>
					</c:forEach>
					<c:if test="${empty articleCategoryList}">
						<td colspan="4" class="td-center">无相关数据</td>
					</c:if>
					</tbody>
				</table>
			</div>
		</section>
	</div>
</section>
</body>
</html>