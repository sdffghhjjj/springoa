<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" session="false" %>  
<%
	String ctxPath = pageContext.getRequest().getServletContext().getContextPath();	
    request.setAttribute("ctx", ctxPath);    
%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <link rel="shortcut icon" href="${ctx}/images/favicon.png" type="image/png">
  <title>500错误页面</title>
  <meta name="description" content="Baiding管理系统">
  <link rel="stylesheet" href="${ctx}/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="${ctx}/css/animate.min.css">
  <link rel="stylesheet" href="${ctx}/vendor/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="${ctx}/css/main.css">
</head>
<body class="bg-white center-wrapper">

<div class="center-content text-center">
  <a href="${pageContext.request.contextPath}">Return Home</a>
   <br>
  <div class="error-number">系统出现错误:<%= exception.toString()%></div>
  <div class="mg-b-lg"><p>错误原因：<%=exception.getMessage() %>  </p> </div>
     
   <pre class="text-left">
     <%        
     	 java.io.ByteArrayOutputStream ostr = new java.io.ByteArrayOutputStream();
     	 exception.printStackTrace(new java.io.PrintStream(ostr));
     	 out.print(ostr);
     %>
   </pre>
  <ul class="mg-t-lg error-nav">
    <li>
      <a href="javascript:;">&copy;
        <span id="year" class="mg-r-xs"></span>Cameo</a>
    </li>
    <li>
      <a href="javascript:;">About</a>
    </li>
    <li>
      <a href="javascript:;">Help</a>
    </li>
    <li>
      <a href="javascript:;">Status</a>
    </li>
  </ul>
</div>

<script type="text/javascript">
  var el = document.getElementById("year"),  year = (new Date().getFullYear());
  el.innerHTML = year;
</script>
</body>
</html>
