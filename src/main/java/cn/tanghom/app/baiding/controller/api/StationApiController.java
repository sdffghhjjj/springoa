package cn.tanghom.app.baiding.controller.api;

import cn.tanghom.app.system.model.Organization;
import cn.tanghom.app.system.service.OrganizationService;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 消防大队和消防站相关接口
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/api/station")
@Api(description = "消防大队和消防站相关接口")
public class StationApiController {

    @Resource
    private OrganizationService organizationService;

    /**
     * 根据父级ID查询消防站
     *
     * @param parentId 父级ID
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "根据父级ID查询消防大队或消防站", notes = "parentId=0时返回消防大队，parentId=消防大队ID时返回下面的消防站", produces = "application/json")
    @ResponseBody
    public String list(@ApiParam(name = "parentId", value = "父级ID", required = true) @RequestParam Long parentId) {
        try {
            if (null == parentId) {
                return CommonUtils.msgCallback(false, "查询失败，请传入父级ID！", null);
            } else {
                List<Organization> organizationList = organizationService.queryListByParentId(parentId);
                return CommonUtils.msgCallback(true, "请求成功", JsonUtil.toJSONString(organizationList));
            }
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "请求失败，请重试！", null);
        }

    }

}
