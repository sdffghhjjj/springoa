package cn.tanghom.app.system.controller;

import cn.tanghom.app.system.model.Resources;
import cn.tanghom.app.system.model.Role;
import cn.tanghom.app.system.service.ResourcesService;
import cn.tanghom.app.system.service.RoleService;
import cn.tanghom.support.page.Pager;
import cn.tanghom.support.utils.CommonUtils;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 资源管理控制层
 *
 * @author tanghom
 */
@Controller
@RequestMapping("role")
public class RoleController {

    @Resource
    private RoleService roleService;
    @Resource
    private ResourcesService resourcesService;
    /**
     * @param model
     * @param name
     * @param key
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "list", method = {RequestMethod.POST, RequestMethod.GET})
    public String list(String name, String key, Long pageNo, Model model) {
        Pager<Role> page = roleService.findPageByParams(name, key, pageNo);
        model.addAttribute("page", page);
        return CommonKey.WEB_PATH + "/system/role/list";
    }

    /**
     * 新增角色页面
     *
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add() {
        return CommonKey.WEB_PATH + "/system/role/edit";
    }

    /**
     * 编辑角色页面
     *
     * @return
     */
    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String edit(Long id, Model model) {
        Role role = roleService.findById(id);
        model.addAttribute("role", role);
        return CommonKey.WEB_PATH + "/system/role/edit";
    }

    /**
     * 保存角色
     *
     * @param role   角色对象
     * @param userId 用户ID数组
     * @return
     */
    @RequestMapping(value = "save", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @ResponseBody
    public String add(Role role, Long userId[]) {
        try {
            String result = roleService.saveOrUpdate(role, userId);
            return result;
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "操作失败，请重试！", "", null);
        }
    }

    /**
     * 删除角色
     *
     * @param id 角色ID
     * @return 结果
     */
    @ResponseBody
    @RequestMapping(value = "delete", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    public String delete(Long id) {
        try {
            roleService.delete(id);
            return CommonUtils.msgCallback(true, "删除角色成功", "", null);
        } catch (Exception ex) {
            return CommonUtils.msgCallback(false, "删除角色失败", "", null);

        }
    }


    /**
     * 编辑角色页面
     *
     * @return
     */
    @RequestMapping(value = "permissions", method = RequestMethod.GET)
    public String permissions(Long roleId, Model model) {
        model.addAttribute("roleId", roleId);
        return CommonKey.WEB_PATH + "/system/role/permissions";
    }
    /**
     * save_role_res
     *
     * @return
     */
    @RequestMapping(value = "save_role_res", method = RequestMethod.POST)
    @ResponseBody
    public String saveRoleRes(Long roleId, Long resIds[]) {
        try {
            roleService.saveRoleRes(roleId,resIds);
            return CommonUtils.msgCallback(true, "保存成功", null);
        } catch (Exception ex) {
            return CommonUtils.msgCallback(false, "保存失败"+ex.getMessage(), null);

        }
    }
}
