package cn.tanghom.support.db;

import java.util.List;
import java.util.Map;

public interface ActiveRecordDao {

	/**
	 * 添加或修改
	 * @param table
	 * @param whereMap
	 * @return
	 */
	public int saveOrUpdate(String table, Map<String, Object> paramMap);
	
	
	/**
	 * 按照条件修改
	 * @param table
	 * @param where is condition string
	 * @return
	 */
	public int update(String table, Map<String, Object> updateValueMap,String ...where);
	
	/**
	 * 按照条件删除
	 * @param table
	 * @param whereMap
	 * @return
	 */
	public int delete(String table, String condition);
	
	/**
	 * 批量增删改
	 * @param sql
	 * @param paramList
	 * @return
	 */
	public int[] batchExecute(String sql, List<Object[]> paramList);
	
	/**
	 * 分页查询
	 * @param sql
	 * @param paramMap
	 * @return
	 */
	public List<Map<String, Object>> find(String sql, Map<String, Object> paramMap);
	
	/**
	 * 查询全部
	 * @param sql
	 * @param paramMap
	 * @return
	 */
	public List<Map<String, Object>> findAll(String sql, Map<String, Object> paramMap);
	
	/**
	 * 查询记录数
	 * @param sql
	 * @param paramMap
	 * @return
	 */
	public int count(String sql, Map<String, Object> paramMap);
	
	
}
