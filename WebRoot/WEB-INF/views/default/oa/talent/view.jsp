<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>文章</title>
	<jsp:include page="../../header.jsp"></jsp:include>	

</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>${article.id==null?'新增':'查看'}简历</b> 
				<a href="${ctx}/oa/talent/html?id=${article.id}" > [HTML格式] </a>		
				
				<b class="control-label"> 状态：</b>
					<c:if test="${article.status==1}">正常</c:if>
					<c:if test="${article.status==0 || article.status==null}">解析失败</c:if>
					<a href="${ctx}/oa/talent/parse?id=${article.id}" > [重新解析] </a>		
					
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<h3>${article.fullName}</h3>
			<div class="panel-body">				
				<div class="form-group">
					<label>${article.fullName}</label>			
				</div>
	   		 
					<div class="form-group">
						<label>工作经历:</label>
						<div >
							<!--用来存放works-->
							
							<table id="works" class="table">	
							<thead>
								<tr>
									<th>工作时间</th>
									<th>结束时间</th>
									<th>所属公司</th>
									<th>职位</th>							
																	
								</tr>
							</thead>	
							<c:forEach var="work" items="${works}">
									<tr>
										<td>${work.start_time}</td>
										<td>${work.end_time}</td>							
										<td>${work.company_name}</td>	
										<td>${work.position}</td>					
									</tr>
							</c:forEach>
							</table>
						
						
						</div>
					</div>
					
					<div class="form-group">
						<label>教育经历:</label>
						<div >
							<!--用来存放edus-->							
							<table id="edus" class="table">	
							<thead>
								<tr>
									<th>起始时间</th>
									<th>结束时间</th>
									<th>学校</th>
									<th>专业</th>							
									<th>学历</th>								
								</tr>
							</thead>	
							<c:forEach var="edu" items="${edus}">
									<tr>
										<td>${edu.start_time}</td>
										<td>${edu.end_time}</td>							
										<td>${edu.school}</td>	
										<td>${edu.profession}</td>
										<td>${edu.degree}</td>						
									</tr>
							</c:forEach>
							</table>
						
						
						</div>
					</div>
				   <div class="form-group">
				    	<div name="content" id="editor" style="height:500px;width:90%;">
				    	<pre>
				    	${article.txt_content}
				    	</pre>
				    	</div>  	   		 		 	
				  </div>
        
			</div>
		</section>
	</div>
</section>
 
</body>
</html>
