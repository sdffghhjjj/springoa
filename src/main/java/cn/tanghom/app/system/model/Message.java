package cn.tanghom.app.system.model;

import java.util.Date;

import org.springframework.context.MessageSource;

import cn.tanghom.app.system.aop.ConfigApplication;

public class Message {
	
	public static Message OK = new Message(true,"OK");
	public static Message SUCCESS = new Message(true,"SUCCESS");
	
	
	private static MessageSource messageSource;
	
	private String title = "提示";
	private String message;
	private String from;
	private String to;
	private Date timestamp;
	private Boolean status = true;
	
	
	public static String i18n(String message,Object ...args){
		if(messageSource==null){
			messageSource = (MessageSource)ConfigApplication.getBean("messageSource");
		}
		return messageSource.getMessage(message, args, null);
	}
	
	public Message(){
		
	}
	
	public Message(Boolean status, String message){
		this.status = status;
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
