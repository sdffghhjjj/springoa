package cn.tanghom.app.system.model;

import java.io.Serializable;

/**
 * 系统配置项
 *
 * @author tanghom <tanghom@qq.com> 2016-6-7
 */
public class Config implements Serializable {
    private static final long serialVersionUID = 1741856884390845913L;
    private Long id;
    private String configKey;//唯一标识
    private String name;//名称
    private String value;//值
    private String remark;//备注
    private Integer typeId;//类型
    private String groupKey;//分组标识
    private Integer groupId;//分组
    private Integer sort = 0;//排序
    private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
}
