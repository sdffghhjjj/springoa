<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page import="org.activiti.conf.CommonKey" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="oa" tagdir="/WEB-INF/tags"  %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<%
	String easyuiThemeName="metro";
	Cookie cookies[] =request.getCookies();
	if(cookies!=null&&cookies.length>0){
		for(Cookie cookie : cookies){
			if (cookie.getName().equals("cookiesColor")) {
				easyuiThemeName = cookie.getValue();
				break;
			}
		}
	}
	
	long sysInitTime = CommonKey.SYSY_INIT_TIME;
    //系统启动时间
    request.setAttribute("sysInitTime",sysInitTime);   
    String ctxPath = pageContext.getRequest().getServletContext().getContextPath();	
    request.setAttribute("ctx", ctxPath);
    request.setAttribute("imagePath", ctxPath);   
    request.setAttribute("contextPath", ctxPath);
%>