package cn.tanghom.app.system.model;

import java.io.Serializable;

/**
 * 菜单、资源数据库实体对象
 * @author tanghom <tanghom@qq.com> 2015-11-18
 *
 */
public class Resources implements Serializable {

	private static final long serialVersionUID = 8714326549840591595L;
	private Long id;//ID
	private String name;//名称
	private String resKey;//唯一标识.等价于perm
	private String url;//URL
	private Integer type;//类型:0、模块；1、菜单； 2、按钮；
	private Long parentId;//上级ID
	private String parentName;//上级名称
	private String icon;//图标
	private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常
	private String description;//描述
	private Integer sort;//排序

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResKey() {
		return resKey;
	}

	public void setResKey(String resKey) {
		this.resKey = resKey;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
