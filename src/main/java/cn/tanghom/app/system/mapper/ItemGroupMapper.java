package cn.tanghom.app.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import cn.tanghom.app.system.model.ItemGroup;
import cn.tanghom.app.system.model.ItemInfo;


@Repository
public interface ItemGroupMapper {
	/**
     * 保存
     * @param articleCategory
     */
    void save(ItemGroup articleCategory);
    /**
     * 修改
     * @param articleCategory
     */
    void update(ItemGroup articleCategory);
    /**
     * 删除
     * @param id
     */
    void delete(Long id);
    /**
     * 根据ID查询
     * @param id
     * @return
     */
    ItemGroup findById(Long id);
    
    /**
     * 根据groupKey查询节点Name的节点  
     * @return
     */
    ItemGroup findByGroupKey(@Param(value = "groupKey") String name);
    
    /**
     * 根据groupKey获取其item项目列表
     * @param name
     * @param status
     * @return
     */
    List<ItemInfo> findItemListByGroupKey(@Param(value = "groupKey") String name,@Param(value = "status") Integer status);
    
    /**
     * 根据条件查询
     * @return
     */
    List<ItemGroup> findListByParams(@Param(value = "name") String name, @Param(value = "status") Integer status);
    
    
}
