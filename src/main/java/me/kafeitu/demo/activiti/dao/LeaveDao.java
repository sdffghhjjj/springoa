package me.kafeitu.demo.activiti.dao;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import cn.tanghom.app.oa.model.Leave;

/**
 * 请假实体管理接口
 *
 * @author HenryYan
 */
@Component
public interface LeaveDao extends CrudRepository<Leave, Long> {
}
