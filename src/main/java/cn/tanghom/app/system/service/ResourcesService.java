package cn.tanghom.app.system.service;

import cn.tanghom.app.system.model.Resources;

import java.util.List;

/**
 * 菜单管理逻辑处理层
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
public interface ResourcesService {
    /**
     * 保存
     *
     * @param resources
     */
    String saveOrUpdate(Resources resources);

    /**
     * 修改
     * @param resources
     */
    void update(Resources resources);
    /**
     * 根据iD删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Resources findById(Long id);

    /**
     * 根据名称或key查找菜单
     *
     * @param name name
     * @param key  key
     * @return
     */
    Resources findByNameOrKey(String name, String key);
    /**
     * 查询菜单
     * @param status
     * @return
     */
    List<Resources> findListByParams(Integer status);

    /**
     * 查询用户所有菜单
     * @param userId
     * @return
     */
    List<Resources> findUserResourcess(Long userId);
    /**
     * 查询角色所有菜单
     * @param roleId
     * @return
     */
    List<Resources> findRoleResourcess(Long roleId);

    /**
     *查询所有菜单返回json
     * @param status
     * @return
     */
    String findListToSimpleJson(Integer status);
}
