package cn.tanghom.app.baiding.mapper;

import cn.tanghom.app.baiding.model.ArticleCategory;

import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * ArticleCategoryMapper
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/25
 */
public interface ArticleCategoryMapper {
    /**
     * 保存
     * @param articleCategory
     */
    void save(ArticleCategory articleCategory);
    /**
     * 修改
     * @param articleCategory
     */
    void update(ArticleCategory articleCategory);
    /**
     * 删除
     * @param id
     */
    void delete(Long id);
    /**
     * 根据ID查询
     * @param id
     * @return
     */
    ArticleCategory findById(Long id);
    
    /**
     * 根据父节点下查询节点Name的节点  
     * @return
     */
    ArticleCategory findByName(@Param(value = "name") String name,@Param(value = "parentId") Long parentId);
    
    /**
     * 根据条件查询
     * @return
     */
    List<ArticleCategory> findListByParams(@Param(value = "name") String name, @Param(value = "status") Integer status);
    
    /**
     * 根据父级节点Id查询改父级节点Id下的节点
     *
     * @param parentId
     * @param status
     * @return
     */
    List<ArticleCategory> queryListByParentId(@Param(value = "parentId") Long parentId, @Param(value = "status") Integer status);
}
