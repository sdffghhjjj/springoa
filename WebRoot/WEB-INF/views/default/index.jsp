<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>首页-Baiding管理系统</title>
	<jsp:include page="header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<h1>Baiding管理系统</h1>
		<div class="row">
		<!--
			<div class="col-md-3 col-sm-4 col-xs-6">
				<section class="panel panel-default">
					<div class="panel-body">
						<div class="circle-icon bg-success">
							<i class="fa fa-microphone"></i>
						</div>
						<div>
							<h3 class="no-margin">5468</h3> 用户数
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-6">
				<section class="panel panel-default">
					<div class="panel-body">
						<div class="circle-icon bg-default">
							<i class="fa fa-magnet"></i>
						</div>
						<div>
							<h3 class="no-margin">751</h3> 角色数
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-6">
				<section class="panel panel-default">
					<div class="panel-body">
						<div class="circle-icon bg-danger">
							<i class="fa fa-anchor"></i>
						</div>
						<div>
							<h3 class="no-margin">856</h3> 业态数
						</div>
					</div>
				</section>
			</div>
		-->
			<c:forEach var="dash" items="${dashboards}" varStatus="s">
				<div class="col-md-3 col-sm-4 col-xs-6">
				<section class="panel panel-default">
					<div class="panel-body">
						<div class="circle-icon bg-${dash.type.name()}">
							<i class="fa ${dash.icon}"></i>
						</div>
						<div>
							<h3 class="no-margin">${dash.content}</h3> ${dash.label}
						</div>
					</div>
				</section>
			</div>
			</c:forEach>					
			
		</div>
	</div>
</section>
</body>
</html>
