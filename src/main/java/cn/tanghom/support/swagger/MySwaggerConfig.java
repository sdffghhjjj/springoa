package cn.tanghom.support.swagger;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Created by tanghom on 2016/5/19.
 */
@EnableWebMvc
@EnableSwagger2
@ComponentScan(basePackages = {"cn.tanghom.app","com.monkeyk.os"})
public class MySwaggerConfig {
    private final Logger log = LoggerFactory.getLogger(MySwaggerConfig.class);
    public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*|rest/.*";


    @Bean
    public Docket swaggerSpringfoxDocket() {
        log.debug("Starting Swagger");
        StopWatch watch = new StopWatch();
        watch.start();
        Docket swaggerSpringMvcPlugin = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .genericModelSubstitutes(ResponseEntity.class)
                .select()
                .paths(regex(DEFAULT_INCLUDE_PATTERN))
                .build();
        watch.stop();
        return swaggerSpringMvcPlugin;
    }

    private ApiInfo apiInfo() {
    	Contact contact = new Contact("author","test@test.com","");    
        ApiInfo apiInfo = new ApiInfo("API列表", "", "", "",contact,"", "");
        return apiInfo;
    }
}