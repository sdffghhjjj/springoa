package cn.tanghom.app.system.service.impl;

import cn.tanghom.app.system.mapper.RoleMapper;
import cn.tanghom.app.system.mapper.RoleResourcesMapper;
import cn.tanghom.app.system.mapper.UserRoleMapper;
import cn.tanghom.app.system.model.Role;
import cn.tanghom.app.system.service.RoleService;
import cn.tanghom.support.page.Pager;
import cn.tanghom.support.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 角色管理控制器
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleMapper roleMapper;
    @Resource
    private UserRoleMapper userRoleMapper;
    @Resource
    private RoleResourcesMapper roleResourcesMapper;
    @Override
    public String saveOrUpdate(Role role, Long[] userId) {
        try {
            Role checkRoleKey = roleMapper.findByNameOrKey(null, role.getRoleKey());
            Role checkRoleName = roleMapper.findByNameOrKey(role.getName(), null);
            if (role.getId() == null) {// 添加
                if (checkRoleKey != null) {
                    return CommonUtils.msgCallback(false, "key已存在,请换一个!", "", null);
                }
                if (checkRoleName != null) {
                    return CommonUtils.msgCallback(false, "名称已存在,请换一个!", "", null);
                }
                roleMapper.save(role);

            } else {// 修改
                Role r = roleMapper.findById(role.getId());
                if (checkRoleKey != null && r.getId() != checkRoleKey.getId()) {
                    return CommonUtils.msgCallback(false, "key已存在,请换一个!", "", null);
                }
                if (checkRoleName != null && r.getId() != checkRoleName.getId()) {
                    return CommonUtils.msgCallback(false, "名称已存在,请换一个!", "", null);
                }
                r.setName(role.getName());
                r.setRoleKey(role.getRoleKey());
                r.setDescription(role.getDescription());
                r.setStatus(role.getStatus());
                r.setSort(role.getSort());
                roleMapper.update(r);
                userRoleMapper.delete(null, r.getId());
            }
            if(null!=userId){
                int userIdSize = userId.length;
                for (int i = 0; i < userIdSize; i++) {
                    userRoleMapper.save(role.getId(), userId[i]);
                }
            }
            return CommonUtils.msgCallback(true, "操作成功", "", null);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void save(Role role) {
        roleMapper.save(role);
    }

    @Override
    public void update(Role role) {
        roleMapper.update(role);
    }

    @Override
    public void delete(Long id) {
        roleMapper.delete(id);
    }

    @Override
    public Role findById(Long id) {
        return roleMapper.findById(id);
    }

    @Override
    public Role findByNameOrKey(String name, String key) {
        return roleMapper.findByNameOrKey(name, key);
    }

    @Override
    public List<Role> findList() {
        return roleMapper.findListByParams(null,null);
    }

    @Override
    public Pager<Role> findPageByParams(String name, String key, Long index) {
        Pager<Role> pager = new Pager<Role>();
        pager.setIndex(index);
        pager.setSize(10);
        pager.setItems(roleMapper.findPageByParams(name, key, pager.startRow(), pager.getSize()));
        pager.setTotalRecord(roleMapper.countByParams(name, key));
        return pager;
    }
    /**
     * 根据条件查询关联的组织返回ID集合
     *
     * @param roleId
     * @param resId
     */
    @Override
    public List<Map<String, Object>> findRoleResListByParams(Long roleId, Long resId){
        return roleResourcesMapper.findListByParams(roleId,resId);
    }

    @Override
    public List<Role> findUserRoles(Long userId) {
        return roleMapper.findUserRoles(userId);
    }

    @Override
    public void saveRoleRes(Long roleId, Long[] resIds) {
        roleResourcesMapper.delete(roleId,null);
        if(null!=resIds){
            int resIdsSize = resIds.length;
            for (int i = 0; i < resIdsSize; i++) {
                roleResourcesMapper.save(roleId, resIds[i]);
            }
        }

    }
}
