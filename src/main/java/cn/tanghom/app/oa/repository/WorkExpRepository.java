package cn.tanghom.app.oa.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import cn.tanghom.app.oa.model.Resume;

@Repository
public interface WorkExpRepository  extends MongoRepository<Resume.WorkExp, Long> {

	List<Resume.WorkExp> findByResumeId(Long resume_id);
}
