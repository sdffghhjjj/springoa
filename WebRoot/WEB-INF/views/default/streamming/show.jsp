<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="cn">
<head>
    
    <title>DATA PROCESSING</title>
    <jsp:include page="../header.jsp"></jsp:include>
    <style>
        html {
            padding: 20px;
        }

        .video-link {
            cursor: pointer;
            display: block;
            line-height: 20px;
        }

        .video-link:focus, .video-link:hover {
            text-decoration: underline;
        }

        .video-link:before {
            width: 30px;
            display: inline-block;
        }

        .file {
            color: darkblue;
            font-weight: bold;
            padding-left: 1em;
        }

        .dir {
            font-style: italic;
        }

        @keyframes fadeinout {
            from {
                opacity: 0;
            }
            10% {
                opacity: 1;
            }
            70% {
                opacity: 1;
            }
            to {
                opacity: 0;
            }
        }

        #playerComment {
            position: absolute;
            width: 100%;
            padding-left: 1em;
            padding-right: 1em;
            top: 2em;
            min-height: 1px;
            white-space: nowrap;
            overflow: hidden;
            font-weight: bold;
            color: orange;
            opacity: 0;
        }

        #playerTitle {
            position: absolute;
            width: 100%;
            padding-left: 1em;
            padding-right: 1em;
            top: 0.5em;
            min-height: 1px;
            white-space: nowrap;
            overflow: hidden;
            font-weight: bold;
            color: indigo;
        }

        #progressContainer {
            position: absolute;
            width: 96%;
            left: 2%;
            bottom: 0;
            opacity: 0.5;
        }

        .ani-blank {
            animation: fadeinout 0.7s;
        }

        article {
            width: 1.5em;
            float: left;
            text-align: center;
            padding-left: 0.1em;
            padding-right: 0.1em;
            height: 8em;
        }

        article .key {
            font-weight: bold;
            color: firebrick;
            padding-bottom: 0.5em;
        }

        article .timestamp {
            -ms-transform: rotate(90deg); /* IE 9 */
            -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
            transform: rotate(90deg);
        }
    </style>
</head>
<body style="min-width: 640px;">
<h2>EYE DATA RECORDING ( location: <span id="pathInfo"></span> )</h2>

<div id="fileList" style="height: 100px; overflow-x: hidden; overflow-y: auto;"></div>

<hr/>
<div class="row">
    <div class="col-xs-6">
        <button class="btn btn-default form-control" id="helpLink" data-href="#help"><b>KEY INSTRUCTION</b></button>
    </div>
    <div class="col-xs-6">
        <button class="btn btn-info form-control" id="send-data"><b>SAVE TO CSV</b></button>
    </div>
</div>
<hr/>

<div style="width: 100%;">
    <div style="position: relative; overflow: hidden;">
        <div id="playerContainer" style="position: absolute; width: 100%; top: 0; left: 0;">
            <video id="player" style="width: 100%;" autoplay="autoplay"
                   poster="http://placehold.it/640x360"
            <%--loop="loop"--%> <%--height="400"--%> <%--muted="muted"--%> <%--controls="controls"--%>>
            </video>
        </div>
        <div id="playerPadding" style="position: relative; width: 100%; padding-bottom: 60%;">
            <div id="playerTitle"></div>
            <div id="playerComment"></div>
            <div class="progress" id="progressContainer">
                <div id="playerProgress" class="progress-bar progress-bar-info progress-bar-striped"
                     role="progressbar" <%--aria-valuenow="70"--%>
                     aria-valuemin="0" aria-valuemax="100" style="width:0; transition-duration: 0.1s;"></div>
            </div>
        </div>
    </div>
    <div style="text-align: center;">
        <button id="restart" title="Restart button" class="btn glyphicon glyphicon-repeat"></button>
        &nbsp;&nbsp;&nbsp;
        <button id="rew" title="Rewind button" class="btn glyphicon glyphicon-backward"></button>
        <button id="play" title="Play button" class="btn glyphicon glyphicon-play"></button>
        <button id="fwd" title="Forward button" class="btn glyphicon glyphicon-forward"></button>
        &nbsp;&nbsp;&nbsp;
        <button id="slower" title="Slower playback button"
                class="btn glyphicon glyphicon-fast-backward"></button>
        <button id="normal" title="Reset playback rate button" class="btn glyphicon glyphicon-refresh"></button>
        <button id="faster" title="Faster playback button"
                class="btn glyphicon glyphicon-fast-forward"></button>
        &nbsp;&nbsp;&nbsp;
        <button id="volDn" title="Volume down button" class="btn glyphicon glyphicon-volume-down"></button>
        <button id="volUp" title="Volume up button" class="btn glyphicon glyphicon-volume-up"></button>
        <button id="mute" title="Mute button" class="btn glyphicon glyphicon-volume-off"></button>
    </div>
    <br/>
</div>

<hr/>
<div id="keyLog" class="clearfix" style="width: 100%; overflow-x: auto; min-height: 1px;"></div>
<hr/>

<div id="help" style="display: none; padding: 2em; background: white; width: 30em; border-radius: 5px;">
    <h2>使用方法</h2>
 <pre>  
左方向键：5秒，
右方向键：5秒以后
方向键上：播放速度快10 %
方向键下：播放速度慢10 %
Space bar：视频播放/停止
Backspace：删除日志
+：屏幕比例放大
-：屏幕比例缩小。
=：显示率100%
ctrl +方向键：移动屏幕中心
Q：视线左侧上面
W：视线上面
E：视线左侧上面
A：视线左侧
S：视线中间
D：视线右侧
Z：视线左下方
X：视线下方
C：视线右下方。
 </pre>  
</div>

<div id="scripts">
  
    <script type="text/javascript" src="${ctx}/vendor/plugins/blockui/jquery.blockUI.js" ></script>
    <script type="text/javascript" src="${ctx}/vendor/jquery.leanModal.replace.js"></script>
    
    <script type="text/javascript">
        var currentPath = "";
        var pathInfo = $("#pathInfo");
        var fileList = $("#fileList");
        var playerTitle = $("#playerTitle");
        var playerContainer = $("#playerContainer");
        var playerPadding = $("#playerPadding");
        var playerProgress = $("#playerProgress");
        var player = $("#player");
        var player0 = player[0];
        var helpLink = $("#helpLink");
        var help = $("#help");
        var keyLog = $("#keyLog");
        var comment = $("#playerComment");

        var playerRew = $("#rew");
        var playerFwd = $("#fwd");
        var playerSlower = $("#slower");
        var playerFaster = $("#faster");
        var playerPlay = $("#play");

        function createPlayer() {
            player.bind('progress', function () {
                adjustVideoContainerRatio();
                playerProgress.css({width: ((player0.currentTime / player0.duration) * 100) + "%"})
            });

            if (player0.canPlayType) {
                playerRew.click(function (e) {
                    e.stopPropagation();
                    setTime(-3);
                });
                playerFwd.click(function (e) {
                    e.stopPropagation();
                    setTime(3);
                });
                playerSlower.click(function (e) {
                    e.stopPropagation();
                    player0.playbackRate -= .25;
                    showComment("playbackRate: " + player0.playbackRate);
                });
                playerFaster.click(function (e) {
                    e.stopPropagation();
                    player0.playbackRate += .25;
                    showComment("playbackRate: " + player0.playbackRate);
                });
                playerPlay.click(function (e) {
                    e.stopPropagation();
                    var button = $(e.target);
                    if (player0.paused) {
                        player0.play();
                        button.removeClass('glyphicon-play').addClass('glyphicon-stop');
                    } else {
                        player0.pause();
                        button.removeClass('glyphicon-stop').addClass('glyphicon-play');
                    }
                });

                document.getElementById("restart").addEventListener("click", function () {
                    setTime(0);
                }, false);
                document.getElementById("normal").addEventListener("click", function () {
                    player0.playbackRate = 1;
                    showComment("playbackRate: " + player0.playbackRate);
                }, false);
                document.getElementById("mute").addEventListener("click", function (e) {
                    if (player0.muted) {
                        player0.muted = false;
                        $(e.target).addClass('glyphicon-volume-off').removeClass('glyphicon-bullhorn');
                    } else {
                        player0.muted = true;
                        $(e.target).addClass('glyphicon-bullhorn').removeClass('glyphicon-volume-off');
                    }
                    showComment("sound " + (player0.muted ? "off" : "on"));
                }, false);
                document.getElementById("volDn").addEventListener("click", function () {
                    setVol(-.1); // down by 10%
                }, false);
                document.getElementById("volUp").addEventListener("click", function () {
                    setVol(.1);  // up by 10%
                }, false);

                function setVol(value) {
                    var vol = player0.volume;
                    vol += value;
                    if (vol >= 0 && vol <= 1) {
                        player0.volume = vol;
                    } else {
                        player0.volume = (vol < 0) ? 0 : 1;
                    }
                    showComment("volume: " + player0.volume);
                }

                player0.addEventListener("error", function (err) {
                    showComment("err: " + err);
                }, true);
            }

            function setTime(tValue) {
                try {
                    player0.currentTime = tValue + (tValue == 0 ? 0 : player0.currentTime);
                    showComment(player0.currentTime + "s");
                } catch (err) {
                    showComment("err: " + "Video content might not be loaded");
                }
            }
        }

        function showComment(text) {
            comment.text(text).removeClass('ani-blank');
            comment[0].offsetWidth = comment[0].offsetWidth;
            comment.addClass('ani-blank');
        }

        function ajaxData(url, data) {
            var deferred = $.Deferred();
            console.log(url);
            console.log(data);
            $.ajax({
                url: url,
                method: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    deferred.resolve(data);
                },
                error: function (result) {
                    deferred.reject(result);
                }
            });
            return deferred.promise();
        }

        $("#send-data").click(function () {
            var articleList = keyLog.find("article");
//            if (player.attr("src") == "" || player.attr("src") == null || trList.length <= 0)
//                return;

            var form = $("<form/>", {
                action: "${ctx}/streamming/save",
                method: "post"
            });

            form.append($("<input/>", {
                name: "videoName",
                value: player.attr("src")
            }));

            for (var i = 0; i < articleList.length; i++) {
                form.append($("<input/>", {
                    name: "materialDataList[" + i + "].key",
                    value: $(articleList[i]).find(".key").text()
                })).append($("<input/>", {
                    name: "materialDataList[" + i + "].timestamp",
                    value: $(articleList[i]).find(".timestamp").text()
                }));
            }

            console.log(form);
            form.submit();
        });

        function loadDirFiles(path) {
            if (path == null)
                path = "";
            fileList.block();
            ajaxData("${ctx}/streamming/list/" + path).done(function (data) {
                fileList.empty();
                currentPath = path;
                pathInfo.text(currentPath);
                fileList.append($("<div/>", {
                    'class': 'video-link glyphicon dir glyphicon-folder-open',
                    text: "<<MOVE TO THE PREVIOUS FOLDER>>",
                    click: function () {
                        if (currentPath == "" || currentPath == "/") {
                            loadDirFiles();
                            return;
                        }

                        var lastIndex = currentPath.lastIndexOf("/");
                        if (lastIndex == currentPath.length - 1)
                            lastIndex = currentPath.substr(0, currentPath.length - 1).lastIndexOf("/");
                        console.log(currentPath.substr(0, lastIndex));
                        loadDirFiles(currentPath.substr(0, lastIndex));
                    }
                }));
                for (var i = 0; i < data.length; i++) {
                    fileList.append($("<div/>", {
                        'class': 'video-link glyphicon ' + (data[i].file ? 'file glyphicon-film' : 'dir glyphicon-folder-open'),
                        text: data[i].fileName,
                        'data-file': "" + data[i].file,
                        click: function () {
                            var path = (currentPath + $(this).text()).replace(/[/][/]/gi, "/");                          
                            if ($(this).hasClass('file')) {
                                player.attr('src', '${ctx}/streamming/video-random-accessible/' + path);
                                keyLog.empty();
                                playerContainer.css({left: 0, top: 0});
                                playerTitle.text(path)
                            } else {
                                loadDirFiles(path);
                            }
                        }
                    }));
                }
                if (data.length == 0)
                    fileList.append($("<i/>", {text: "NO MP4 EXISTS"}));
                fileList.unblock();
            }).fail(function (e) {
                fileList.text("error[" + e.status + "]: " + e.statusText);
                alert("FAILED TO CREATE FILE LIST");
                fileList.unblock();
            });
        }

        function addArticle(timestamp, key) {
            keyLog.append(
                    $('<article/>')
                            .append($('<div/>', {'class': 'key', text: key}))
                            .append($('<div/>', {'class': 'timestamp', text: timestamp}))
            );
        }

        function removeArticle() {
            keyLog.find('article:last').remove();
        }

        var videoRatio = 1;

        function adjustVideoContainerRatio() {
            var ratio = player0.videoHeight / player0.videoWidth;
//            console.log("ratio:" + ratio);
            if (!isNaN(ratio))
                playerPadding.css({'padding-bottom': (ratio * 100) + '%'});
        }

        $(window).load(function () {
            createPlayer();
            helpLink.leanModal({closeButton: ".lean-overlay"});
            loadDirFiles();

            $(document).bind('keyup keydown keypress', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
			var keyCodeMap={8:'backspace',37:'arrowleft',39:'arrowright',38:'arrowup',40:'arrowdown',16:'+',189:'-',187:'='};
            $(document).keydown(function (e) {
//                console.log(e);
                var eyeTrackingKeys = ['q', 'w', 'e', 'a', 's', 'd', 'z', 'x', 'c'];
                var arrowKeys = ['arrowleft', 'arrowright', 'arrowup', 'arrowdown'];
                //-var key = e.key.toLowerCase();
                var key = keyCodeMap[e.keyCode];
                if(!key){
                  key = String.fromCharCode(e.keyCode).toLowerCase();
                }
                if (player0.canPlayType) {
                    if (key == " ") {
                        playerPlay.click();
                    } else if (!player0.paused) {
                        if ($.inArray(key, eyeTrackingKeys) >= 0) {
                            addArticle(player0.currentTime, key);
                        } else if ($.inArray(key, arrowKeys) >= 0) {
                            if (e.ctrlKey) {
                                var left = parseInt(playerContainer.css('left'));
                                var top = parseInt(playerContainer.css('top'));
                                var width = parseInt(playerPadding.css('width'));
                                var height = parseInt(playerPadding.css('height'));
                                var videoWidth = parseInt(playerContainer.css('width'));
                                var videoHeight = parseInt(playerContainer.css('height'));
                                var INTERVAL = 5;
                                if (key == "arrowleft") {
                                    var last = left - INTERVAL;
                                    if (last <= 0 && width <= videoWidth + last)
                                        playerContainer.css({left: last + 'px'});
                                } else if (key == "arrowright") {
                                    last = left + INTERVAL;
                                    if (last <= 0 && width <= videoWidth + last)
                                        playerContainer.css({left: last + 'px'});
                                } else if (key == "arrowup") {
                                    last = top - INTERVAL;
                                    if (last <= 0 && height <= videoHeight + last)
                                        playerContainer.css({top: last + 'px'});
                                } else if (key == "arrowdown") {
                                    last = top + INTERVAL;
                                    if (last <= 0 && height <= videoHeight + last)
                                        playerContainer.css({top: last + 'px'});
                                }
                            } else {
                                if (key == "arrowleft") {
                                    playerRew.click();
                                } else if (key == "arrowright") {
                                    playerFwd.click();
                                } else if (key == "arrowup") {
                                    playerFaster.click();
                                } else if (key == "arrowdown") {
                                    playerSlower.click();
                                }
                            }
                        } else if (key == '+') {
                            videoRatio += 0.1;
                            playerContainer.css({width: videoRatio * 100 + '%'});
                        } else if (key == '-') {
                            videoRatio = videoRatio - 0.1 < 1 ? 1 : videoRatio - 0.1;
                            playerContainer.css({width: videoRatio * 100 + '%'});
                        } else if (key == '=') {
                            playerContainer.css({left: 0, top: 0, width: '100%'});
                        } else if (key == "backspace") {
                            removeArticle();
                        }
                    }
                }
            });
        });
    </script>
</div>
</body>
</html>
