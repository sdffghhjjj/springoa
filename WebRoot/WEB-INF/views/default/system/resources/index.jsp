<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>菜单管理</title>
	<jsp:include page="../../header.jsp"></jsp:include>
	<link href="${pageContext.request.contextPath}/vendor/ztree/style/zTreeStyle.css" rel="stylesheet">
	<script src="${pageContext.request.contextPath}/vendor/ztree/jquery.ztree.core-3.5.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/vendor/ztree/jquery.ztree.excheck-3.5.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/vendor/ztree/jquery.ztree.exedit-3.5.js" type="text/javascript"></script>

	<style type="text/css">
	ul.ztree {padding:0;width:220px;height:460px;overflow-x:auto;}
</style>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>菜单资源列表</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<div class="row table-search">
					<div class="col-sm-4">
						<a href="javascript:expandNode();" class="btn btn-default btn-sm">展开/折叠</a>
						<a href="javascript: addNode(0);" class="btn btn-success btn-sm">新增根菜单</a>
						<a href="javascript:addNode(1);" class="btn btn-success btn-sm">新增子菜单</a>
						<a href="javascript:deleteNode();" class="btn btn-danger btn-sm">删除</a>
					</div>
					<div class="col-sm-8 text-right">
					</div>
				</div>
				<div class="clearfix">
					<div class="f_left">
						<ul id="resourcesTree" class="ztree"></ul>
					</div>
					<div class="f_left">
						<form class="form-horizontal" method="post" id="form" style="width: 300px;display: none;">
							<div class="form-group">
								<label class="col-sm-4 control-label">*上级菜单:</label>
								<div class="col-sm-8">
									<input type="text" name="parentName" value="" placeholder="上级菜单" readonly class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">*名称:</label>
								<div class="col-sm-8">
									<input type="text" name="name" value="" placeholder="名称" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">*KEY:</label>
								<div class="col-sm-8">
									<input type="text" name="resKey" value="" placeholder="KEY" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">*URL:</label>
								<div class="col-sm-8">
									<input type="text" name="url" value="" placeholder="URL" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">*类型:</label>
								<div class="col-sm-8">
									<select class="form-control input-sm" name="type" data-placeholder="状态">
										<option value="0">模块</option>
										<option value="1">菜单</option>
										<option value="2">按钮</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">ICON:</label>
								<div class="col-sm-8">
									<input type="text" name="icon" value="" placeholder="ICON" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">描述:</label>
								<div class="col-sm-8">
									<textarea name="description" placeholder="描述" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">*排序:</label>
								<div class="col-sm-8">
									<input type="number" name="sort" value="0" placeholder="排序" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">*状态:</label>
								<div class="col-sm-8">
									<select class="form-control input-sm" name="status" data-placeholder="状态">
										<option value="1">启用</option>
										<option value="0">禁用</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-4">
									<input type="hidden" name="id" value="" />
									<input type="hidden" name="parentId" value="" />
									<button type="button" class="btn btn-primary btn-sm btn-block" id="sb_btn" onclick="saveSub();">保存</button>
								</div>
							</div>
						</form>
					</div>
				</div>
     		 </div>
		</section>
	</div>
</section>

	<script type="text/javascript">
		var setting = {
			edit: {
				edit: {
					enable: true,
					showRemoveBtn: false,
					showRenameBtn: false
				},
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			view: {
				showIcon: false,
				selectedMulti: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeClick: beforeClick,
				onClick: onClick,
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop
			}
		};
		$(document).ready(function(){
			$.ajax({
				type:"get",
				url:"${pageContext.servletContext.contextPath}/resources/json.shtml",
				dataType:"json",
				success:function(data){
					$.fn.zTree.init($("#resourcesTree"), setting, data);
				}
			});

		});
		//展开或折叠
		var expandAll =false;
		function expandNode() {
			var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
			if (expandAll == false) {
				expandAll =true;
				zTree.expandAll(expandAll);
			} else if (expandAll == true) {
				expandAll =false;
				zTree.expandAll(expandAll);
			}
		}
		function beforeClick(treeId, treeNode, clickFlag) {
			return (treeNode.click != false);
		}
		function onClick(event, treeId, treeNode, clickFlag) {
			restForm();
			var parentNode = treeNode.getParentNode();
			var parentName = "无上级";
			if(null!=parentNode){
				parentName = parentNode.name;
			}
			$("[name='parentName']").val(parentName);
			var id = treeNode.id;
			$.ajax({
				type:"get",
				url:"${pageContext.servletContext.contextPath}/resources/edit.shtml",
				data:{id:id},
				dataType:"json",
				success:function(data){
					$("[name='id']").val(data.id);
					$("[name='parentId']").val(data.parentId);
					$("[name='name']").val(data.name);
					$("[name='resKey']").val(data.resKey);
					$("[name='url']").val(data.url);
					$("[name='icon']").val(data.icon);
					$("[name='type']").val(data.type);
					$("[name='sort']").val(data.sort);
					$("[name='description']").val(data.description);
					$("[name='status']").val(data.status);
					$("#form").show();
				}
			});

		}
		//移动前
		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					return false;
				}
			}

			return true;
		}
		//移动后
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			node = treeNodes[0];
			var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
			var parentId = 0;
			var id = node.id;
			if(targetNode){
				node.pId=targetNode.id;
				parentId = targetNode.id;
			}
			$.post('${pageContext.servletContext.contextPath}/resources/update_parent.shtml',{id:id,parentId:parentId},function(result){
				if(result.status==1){
					zTree.updateNode(node);
				}else{
					msg.info("操作失败，请刷新重试");
					return false;
				}
			},'json');
		}
		/**
		 * 新增菜单
		 */
		function addNode(type){
			restForm();
			var parentId = 0;
			var parentName = "无上级";
			$("#sb_btn").text("新增根菜单");
			if(1 == type){
				var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
				var nodes = zTree.getSelectedNodes();
				var treeNode = nodes[0];
				if(nodes.length<=0){
					msg.info("请选择一个菜单")
					return ;
				}else{
					$("#sb_btn").text("新增子菜单");
					var treeNode = nodes[0];
					parentId = treeNode.id;
					parentName = treeNode.name;
				}
			}
			$("[name='parentName']").val(parentName);
			$("[name='parentId']").val(parentId);
			$("#form").show();
		}
		/**
		 * 删除
		 */
		function deleteNode(){
			var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
			var nodes = zTree.getSelectedNodes();
			if(nodes.length<=0){
				msg.info("请选择一个菜单")
			}else{
				var treeNode = nodes[0];
				msg.confirm("是否删除"+treeNode.name+"？",function(){
					var id = treeNode.id;
					$.ajax({
						type:"get",
						url:"${pageContext.servletContext.contextPath}/resources/delete.shtml",
						data:{id:id},
						dataType:"json",
						success:function(data){
							if(data.status == 1){
								msg.info(data.info)
								var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
								var node = zTree.getNodeByParam("id", id, null);
								zTree.removeNode(node);
								restForm();
							}else{
								msg.info(data.info)
							}
						}
					});
				},"是否删除？");
			}

		}
		function saveSub(){
			var obj = $("#form").serialize();
			msg.confirm("确认保存？",function(){
				$.ajax({
					type:"post",
					url:"${pageContext.servletContext.contextPath}/resources/save.shtml",
					data:$("#form").serialize(),
					dataType:"json",
					success:function(result){
						if(result.status == 1){
							msg.info(result.info)
							var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
							var id = $("#form").find("[name='id']").val();//判断是新增还是修改
							var resNode=$.parseJSON(result.data);
							if(""==id){
								var parentId = resNode.parentId;
								var node={id:resNode.id,name:resNode.name};
								if(parentId==0){
									zTree.addNodes(null,node);
								}else{
									var parentZNode = zTree.getNodeByParam("id", parentId, null); //获取父菜单
									zTree.addNodes(parentZNode, node);
								}
							}else {
								var nodes = zTree.getNodeByParam("id", id, null);
								nodes.name=resNode.name;
								zTree.updateNode(nodes);
							}
							restForm();
						}else{
							msg.info(result.info)
						}

					}
				});
			},"是否保存？");
		}
		function restForm(){
			$("#form")[0].reset();
			$("[name='id']").val("");
			$("[name='parentName']").val("");
			$("[name='parentId']").val("");
			$("[name='name']").val("");
			$("[name='resKey']").val("");
			$("[name='url']").val("/");
			$("[name='icon']").val("");
			$("[name='type']").val("0");
			$("[name='sort']").val("0");
			$("[name='description']").val("");
			$("[name='status']").val(1);
			$("#sb_btn").text("保存");
			$("#form").hide();
		}
	</script>
</body>
</html>
