<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>选项分类管理</title>
	<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>${articleCategory.id==null?'新增':'修改'}选项分类</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" action="${pageContext.request.contextPath}/item/save" method="post" id="form">
					<div class="form-group">
	                     <label class="col-sm-4 control-label">*Key:</label>
	                     <div class="col-sm-4">
	                        <input type="text" name="name" value="${articleCategory.groupKey}" placeholder="groupKey" class="form-control input-sm">
	                     </div>
	                 </div>
	   		 		<div class="form-group">
					    <label class="col-sm-4 control-label">*名称:</label>
					    <div class="col-sm-4">
				            <input type="text" name="name" value="${articleCategory.name}" placeholder="名称" class="form-control input-sm">
					    </div>
				  </div>
				   <div class="form-group">
				    <label class="col-sm-4 control-label">注释:</label>
				    <div class="col-sm-4">
  	   		 		 	<textarea name="description" placeholder="注释" class="form-control">${articleCategory.remark}</textarea>
				    </div>
				  </div>
                 
				 <div class="form-group">
					 <label class="col-sm-4 control-label">*状态：</label>
					 <div class="col-sm-4">
						 <select class="form-control input-sm" name="status" data-placeholder="状态">
							 <option value="1"<c:if test="${articleCategory.status==1||articleCategory.status==null}">selected</c:if> >启用</option>
							 <option value="2" <c:if test="${articleCategory.status==0}">selected</c:if> >禁用</option>
						 </select>
					 </div>
				 </div>
					<div class="form-group">
					    <div class="col-sm-offset-4 col-sm-4">
					      <input type="hidden" name="id" value="${articleCategory.id}" />
					      <button type="button" class="btn btn-primary btn-sm btn-block" onclick="sub();">保存</button>
					    </div>
				  </div>
          </form>
	</div>
</section>
		</div>
	</section>
	<script type="text/javascript">
	function sub(){
		var checkName = checkEmpty($('[name="name"]'),'请填名称');
		if(checkName){
			msg.confirm("确认保存？",function(){
			$.ajax({
				type : "POST",
				data : $("#form").serialize(),
				url : $("#form").attr("action"),
				dataType : 'json',
				success:function(data) {
					if (data.status == 1) {
						msg.info(data.info);
						setTimeout(function(){
							window.location.href="${pageContext.request.contextPath}/item/group_list.shtml";
						},1000);
					} else {
						msg.info(data.info);
					}
				}
			});
			},"是否保存？");
		}
	}

	</script>
</body>
</html>
