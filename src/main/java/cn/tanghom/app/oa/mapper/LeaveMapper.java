package cn.tanghom.app.oa.mapper;

import cn.tanghom.app.oa.model.Leave;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 请假mapper
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/25
 */
public interface LeaveMapper {
    /**
     * 保存
     *
     * @param leave
     */
    void save(Leave leave);

    /**
     * 修改
     *
     * @param leave
     */
    void update(Leave leave);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Leave findById(Long id);

    /**
     * 根据条件查询
     *
     * @param userId 用户ID
     * @param status 状态
     * @return
     */
    List<Leave> findPageByParams(@Param(value = "userId") String userId, @Param(value = "status") Integer status, @Param(value = "first") Long first, @Param(value = "size") Integer size);

    /**
     * 查询总数
     *
     * @param userId
     * @param status
     * @return
     */
    Long countByParams(@Param(value = "userId") String userId, @Param(value = "status") Integer status);
}
