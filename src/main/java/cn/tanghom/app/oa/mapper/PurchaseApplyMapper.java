package cn.tanghom.app.oa.mapper;

import cn.tanghom.app.oa.model.PurchaseApply;

public interface PurchaseApplyMapper {
	void save(PurchaseApply apply);
	PurchaseApply get(int id);
	void update(PurchaseApply apply);
}
