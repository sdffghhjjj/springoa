<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>	
	<jsp:include page="../../header.jsp"></jsp:include>	
	<title>请假申请</title>	
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>请假申请</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" action="${ctx}/oa/leave/apply" method="post" id="form">
					<div class="form-group">
						<label class="col-sm-4 control-label">*请假类型:</label>
						<div class="col-sm-4">
							<select class="form-control input-sm" name="leaveType">
								<option value="事假">事假</option>
								<option value="年休">年休</option>
								<option value="病假">病假</option>
								<option value="调休">调休</option>
								<option value="婚假">婚假</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">*开始时间:</label>
						<div class="col-sm-4">
							<input type="text" name="startTime" placeholder="开始时间" class="form-control input-sm">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">*结束时间:</label>
						<div class="col-sm-4">
							<input type="text" name="endTime"  placeholder="结束时间" class="form-control input-sm">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">原因:</label>
						<div class="col-sm-4">
							<textarea name="reason" placeholder="原因" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-4">
							<button type="button" class="btn btn-primary btn-sm btn-block" onclick="sub();">保存并提交</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</section>
<script type="text/javascript">
	function sub(){
		var startTimeC = checkEmpty($('[name="startTime"]'),'请选择开始时间');
		var endTimeC = checkEmpty($('[name="endTime"]'),'请选择结束时间');
		var reasonC = checkEmpty($('[name="reason"]'),'请填写原因');
		if(startTimeC && endTimeC && reasonC){
			msg.confirm("确认保存？",function(){
				$.ajax({
					type : "POST",
					data : $("#form").serialize(),
					url : $("#form").attr("action"),
					dataType : 'json',
					success:function(data) {
						if (data.status == 1) {
							msg.info(data.info);
						} else {
							msg.info(data.info);
						}
					}
				});
			},"是否保存？");
		}
	}
	//验证信息
	//验证空信息
	function checkEmpty(obj,msg){
		var check = false;
		var val = obj.val();
		if(val == ""){
			obj.err(msg);
		}else{
			check = true;
			obj.closeErr();
		}
		return check;
	}
</script>
</body>
</html>
