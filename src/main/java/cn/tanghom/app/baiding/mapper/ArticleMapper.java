package cn.tanghom.app.baiding.mapper;

import cn.tanghom.app.baiding.model.Article;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ArticleMapper
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/25
 */
public interface ArticleMapper {
    /**
     * 保存
     *
     * @param article
     */
    void save(Article article);

    /**
     * 修改
     *
     * @param article
     */
    void update(Article article);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Article findById(Long id);

    /**
     * 根据条件查询
     *
     * @param categoryId 分类ID
     * @param status     状态
     * @return
     */
    List<Article> findListByParams(@Param(value = "categoryId") Long categoryId, @Param(value = "status") Integer status);

    List<Article> findPageByParams(@Param(value = "title") String title, @Param(value = "categoryId") Long categoryId, @Param(value = "status") Integer status, @Param(value = "first") Long first, @Param(value = "size") Integer size);

    Long countByParams(@Param(value = "title") String title, @Param(value = "categoryId") Long categoryId, @Param(value = "status") Integer status);
}
