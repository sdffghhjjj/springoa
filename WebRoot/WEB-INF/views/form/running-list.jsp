<%@page import="cn.tanghom.support.activiti.ProcessDefinitionCache,org.activiti.engine.RepositoryService"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en" >
  <head> 
  	<%@include file="/common/head.jsp"%> 
	<title>运行中流程列表</title>
	
	<script src="${ctx }/js/module/activiti/workflow.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(function() {
		// 跟踪
	    $('.trace').click(graphTrace);
	});
	</script>
</head>

<body>
	<%
	RepositoryService repositoryService = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext()).getBean(org.activiti.engine.RepositoryService.class);
	ProcessDefinitionCache.setRepositoryService(repositoryService);
	%>
	<table>
		<tr>
			<th>执行ID</th>
			<th>流程实例ID</th>
			<th>流程定义ID</th>
			<th>当前节点</th>
			<th>是否挂起</th>
		</tr>

		<c:forEach items="${page.result }" var="p">
		<c:set var="pdid" value="${p.processDefinitionId }" />
		<c:set var="activityId" value="${p.activityId }" />
		<tr>
			<td>${p.id }</td>
			<td>${p.processInstanceId }</td>
			<td>${p.processDefinitionId }</td>
			<td><a class="trace" href='#' pid="${p.id }" pdid="${p.processDefinitionId}" title="点击查看流程图"><%=ProcessDefinitionCache.getActivityName(pageContext.getAttribute("pdid").toString(), ObjectUtils.toString(pageContext.getAttribute("activityId"))) %></a></td>
			<td>${p.suspended }</td>
		</tr>
		</c:forEach>
	</table>
	<tags:pagination page="${page}" paginationSize="${page.pageSize}"/>
	<!-- 办理任务对话框 -->
	<div id="handleTemplate" class="template"></div>

</body>
</html>
