package cn.tanghom.app.system.mapper;

import cn.tanghom.app.system.model.Resources;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 资源资源映射
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Repository
public interface ResourcesMapper {
    /**
     * 保存
     *
     * @param resources
     */
    void save(Resources resources);

    /**
     * 更新
     *
     * @param resources
     * @return
     */
    void update(Resources resources);

    /**
     * 单个删除
     *
     * @param id
     * @return
     */
    void delete(@Param(value = "id") Long id);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Resources findById(@Param(value = "id") Long id);

    /**
     * 根据角色Key或名称查找资源
     *
     * @param key
     * @return
     */
    Resources findByKey(@Param(value = "key") String key);

    /**
     * 查询资源集合
     * @param status
     * @return
     */
    List<Resources> findListByParams(@Param(value = "status") Integer status);

    /**
     * 查询用户所拥有所有资源
     * @param userId
     * @return
     */
    List<Resources> findUserResourcess(@Param(value = "userId") Long userId);

    /**
     * 查询角色所拥有所有资源
     * @param roleId
     * @return
    */
    List<Resources> findRoleResourcess(@Param(value = "roleId") Long roleId);

}
