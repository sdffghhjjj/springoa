package cn.tanghom.app.baiding.controller.api;

import cn.tanghom.app.baiding.model.Article;
import cn.tanghom.app.baiding.model.ArticleCategory;
import cn.tanghom.app.baiding.service.ArticleCategoryService;
import cn.tanghom.app.baiding.service.ArticleService;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文章接口
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/api/article")
@Api(description = "文章相关接口")
public class ArticleApiController {

    @Resource
    private ArticleService articleService;
    @Resource
    private ArticleCategoryService articleCategoryService;

    /**
     * 根据文章分类ID列表查询文章列表
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "根据文章分类ID列表查询文章列表", notes = "根据文章分类ID列表查询文章列表")
    public String list(@ApiParam(name = "categoryId", value = "文章分类ID", required = true) @RequestParam Long categoryId) {
        try {
            List<Article> articleList = articleService.findListByParams(categoryId, 1);
            String returnJson = JsonUtil.toJSONString(articleList);
            return CommonUtils.msgCallback(true, "请求成功", returnJson);
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "请求失败，请重试！", null);
        }
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ApiOperation(value = "根据文章ID查询文章", notes = "返回的是页面，可浏览器打开")
    public ModelAndView detail(@ApiParam(name = "id", value = "文章ID", required = true) @RequestParam Long id) {
        Article article = articleService.findById(id);
        ModelAndView model = new ModelAndView(CommonKey.WEB_PATH + "/api/article_detail");
        model.addObject("article", article);
        return model;
    }

    /**
     * 查询文章分类列表
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/category_list", method = RequestMethod.GET)
    @ApiOperation(value = "查询文章分类列表", notes = "查询文章分类列表")
    public String categoryList() {
        try {
            List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParams(null, 1);
            String returnJson = JsonUtil.toJSONString(articleCategoryList);
            return CommonUtils.msgCallback(true, "请求成功", returnJson);
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "请求失败，请重试！", null);
        }
    }

}
