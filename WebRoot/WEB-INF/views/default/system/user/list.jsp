<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>用户管理</title>
	<jsp:include page="../../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>用户管理</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<div class="row table-search">
					<div class="col-xs-2">
						<a href="${pageContext.servletContext.contextPath}/user/add.shtml">
							<span class="btn btn-success btn-sm">新增</span>
						</a>
					</div>
					<div class="col-xs-10 text-right">
						<form class="form-inline" role="form" method="post">
							<div class="form-group">
								<label class="sr-only">账号</label>
								<input type="text" class="form-control input-sm" value="${param.loginName}" name="loginName" placeholder="账号">
							</div>
							<div class="form-group">
								<label class="sr-only">姓名</label>
								<input type="text" class="form-control input-sm" value="${param.userName}" name="userName" placeholder="姓名">
							</div>
							<div class="form-group">
								<label class="sr-only">*组织机构：</label>
									<select class="form-control input-sm" name="orgId">
										<option value="" <c:if test="${orgId==null}">selected</c:if> >请选择</option>
										<c:forEach var="r" items="${organizations}" varStatus="s">
											<option value="${r.id}"
													<c:if test="${orgId==r.id}">selected</c:if> >${r.name}</option>
										</c:forEach>
									</select>
							</div>
							<div class="form-group">
								<label class="sr-only">*状态：</label>
							<select class="form-control input-sm" name="status" data-placeholder="状态">
								<option value="" <c:if test="${user.status==null}">selected</c:if> >请选择</option>
								<option value="1"<c:if test="${user.status==1}">selected</c:if> >启用</option>
								<option value="0" <c:if test="${user.status==0}">selected</c:if> >禁用</option>
								<option value="2" <c:if test="${user.status==2}">selected</c:if> >待审核</option>
							</select>
								</div>
							<button type="submit" class="btn btn-primary btn-sm">搜索</button>
						</form>
					</div>
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>名称</th>
							<th>账号</th>
							<th>组织机构</th>
							<th>角色</th>
							<th>性别</th>
							<th>电话</th>
							<th>状态</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="user" items="${page.items}" varStatus="s">
						<tr>
							<td>${user.id}</td>
							<td>${user.userName}</td>
							<td>${user.loginName}</td>
							<td>${user.organizationName}</td>
							<td>
								<c:forEach var="role" items="${user.roles}" varStatus="s">
									${role.name};
								</c:forEach>
							</td>
							<td>${user.sex}</td>
							<td>${user.phone}</td>
							<td>
								<c:if test="${user.status==0}">禁用</c:if>
								<c:if test="${user.status==1}">启用</c:if>
								<c:if test="${user.status==2}">待审核</c:if>
							</td>
							<td>
								<a href="${pageContext.servletContext.contextPath}/user/edit.shtml?id=${user.id}" class="label label-success">编辑</a>
							<a href="${pageContext.servletContext.contextPath}/user/delete.shtml?id=${user.id}" data-info="确认删除吗?" class="confirm ajax-get label label-danger">删除</a>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${empty page}">
						<td colspan="8" class="td-center">无相关数据</td>
					</c:if>
					</tbody>
				</table>
				<div class="clearfix">
					<div class="pull-right">
						<mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
					</div>
				</div>
			</div>
		</section>
	</div>
</section>
</body>
</html>