package cn.tanghom.app.system.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * RoleResourcesMapper 关系处理
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Repository
public interface RoleResourcesMapper {

    /**
     * 保存
     *
     * @param roleId
     * @param resId
     */
    void save(@Param(value = "roleId") Long roleId, @Param(value = "resId") Long resId);

    /**
     * 删除
     *
     * @param roleId
     * @param resId
     */
    void delete(@Param(value = "roleId") Long roleId, @Param(value = "resId") Long resId);
    /**
     * 根据条件查询返回集合
     *
     * @param roleId
     * @param resId
     */
    List<Map<String,Object>> findListByParams(@Param(value = "roleId") Long roleId, @Param(value = "resId") Long resId);

}
