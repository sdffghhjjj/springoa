package cn.tanghom.app.oa.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 岗位信息
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Data
public class Position implements Serializable {
    private static final long serialVersionUID = -8155028965166110406L;
    private Long id;//ID   
    private String title;  //职位名称   
    private Long depId; 	//所属部门ID
    private String depName; //所属部门Name   
    private String email;	//邮箱组   
    private String description;//描述
    private String requirement;
    private int salary; 	//月基本岗位薪
    private Date createTime;//新建时间
    private Integer status = 2;//状态:0、禁用；1、正常；2、待审核, 默认：2、待审核
    
    //以下为职位招聘信息
     int anualSalary;  // 年薪
     
     int min_show_annual_salary;
     int max_show_annual_salary;
     
	 int degree_required;
	
	 int gender_required;
	
	 int work_exp_required;
   

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
