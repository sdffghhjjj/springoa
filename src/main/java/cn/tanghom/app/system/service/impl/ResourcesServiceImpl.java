package cn.tanghom.app.system.service.impl;

import cn.tanghom.app.system.mapper.ResourcesMapper;
import cn.tanghom.app.system.model.Resources;
import cn.tanghom.app.system.service.ResourcesService;
import cn.tanghom.support.tree.SimpleJsonTree;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 菜单管理控制器,perm string
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
@Service
public class ResourcesServiceImpl implements ResourcesService {
    @Resource
    private ResourcesMapper resourcesMapper;


    @Override
    public String saveOrUpdate(Resources resources) {
        try {
            Resources checkResKey = resourcesMapper.findByKey(resources.getResKey());
            if (resources.getId() == null) {// 添加
                if (checkResKey != null) {
                    return CommonUtils.msgCallback(false, "key已存在,请换一个!", "", null);
                }
                resourcesMapper.save(resources);
            } else {// 修改
                Resources res  =resourcesMapper.findById(resources.getId());
                res.setName(resources.getName());
                res.setParentId(resources.getParentId());
                res.setResKey(resources.getResKey());
                res.setUrl(resources.getUrl());
                res.setIcon(resources.getIcon());
                res.setDescription(resources.getDescription());
                res.setSort(resources.getSort());
                res.setStatus(resources.getStatus());
                resourcesMapper.update(res);
            }
            return CommonUtils.msgCallback(true, "操作成功", "", JsonUtil.toJSONString(resources));
            } catch (Exception e) {
                throw e;
            }
    }

    @Override
    public void update(Resources resources) {
        resourcesMapper.update(resources);
    }

    @Override
    public void delete(Long id) {
        resourcesMapper.delete(id);
    }

    @Override
    public Resources findById(Long id) {
        return resourcesMapper.findById(id);
    }

    @Override
    public Resources findByNameOrKey(String name, String key) {
        return null;
    }

    @Override
    public List<Resources> findListByParams(Integer status) {
        return resourcesMapper.findListByParams(status);
    }

    @Override
    public List<Resources> findUserResourcess(Long userId) {
        return resourcesMapper.findUserResourcess(userId);
    }

    @Override
    public List<Resources> findRoleResourcess(Long roleId) {
        return resourcesMapper.findRoleResourcess(roleId);
    }

    @Override
    public String findListToSimpleJson(Integer status) {
        List<Resources> resourcesList = resourcesMapper.findListByParams(status);
        List<SimpleJsonTree> simpleJsonTreeList = new ArrayList<SimpleJsonTree>();
        if (null != resourcesList) {
            for (int i = 0; i < resourcesList.size(); i++) {
                Resources res = resourcesList.get(i);
                 Integer id = res.getId()!=null? res.getId().intValue():0;
                 Integer pId = res.getParentId()!=null? res.getParentId().intValue():0;
                SimpleJsonTree simpleJsonTree = new SimpleJsonTree(id, pId, res.getName());
                simpleJsonTreeList.add(simpleJsonTree);
            }
        }
        return JsonUtil.toJSONString(simpleJsonTreeList);
    }
}
