package cn.tanghom.app.oa.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 员工信息，id对应于User id， empid,empname,depid,salary
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Data
public class Employee implements Serializable {
    private static final long serialVersionUID = -8155028965166110406L;
    private Long id;//UID   
    private String empName;//姓名   
    private Long depId; 	//用户所属部门ID
    private String depName; //用户所属部门Name
    private String phone;	//工作电话
    private String email;	//工作邮箱
    private String avator;  //头像地址
    private String description;//描述
    private int salary; //月基本薪
    private Date createTime;//新建时间
    private Date endTime;	//雇佣结束时间
    private Integer status = 2;//状态:0、禁用；1、正常；2、待审核, 默认：2、待审核，4，主动离职，5，解雇
   

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
