<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<% request.setAttribute("ctx", pageContext.getRequest().getServletContext().getContextPath()); %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">  
    <title>Baiding管理系统</title>
    <jsp:include page="header.jsp"></jsp:include>    
    
    <link rel="stylesheet" href="${ctx}/vendor/jquery-easyui/tabs.css">
    <script src="${ctx}/vendor/jquery-easyui/jquery.easyui.min.js"></script>
    <script src="${ctx}/vendor/jquery-easyui/jquery.tabs.js"></script>
    <script src="${ctx}/vendor/websocket/sockjs-0.3.min.js"></script>
    <script src="${ctx}/js/websocket.js"></script>
    <script src="${ctx}/js/off-canvas.js"></script>
    <script src="${ctx}/js/main.js"></script>
  
</head>
<body>
<div class="app">
    <header class="header header-fixed navbar">
        <div class="brand">
            <a href="javascript:;" class="fa fa-bars off-left visible-xs" data-toggle="off-canvas" data-move="ltr"></a>
            <a href="javascript:;" class="navbar-brand text-white">
                <i class="fa fa-dashboard" class="bounceIn animated"></i>
                <span class="heading-font"><b>白丁管理系统</b></span>
            </a>
        </div>
        <ul class="nav navbar-nav" style="font-weight: bold;">
        <li class="active">
            <a href="javascript:;" class="module_tab" data-id="home">
                <i class="fa fa-home"></i>
                <span class="">主页</span>
            </a>
        </li>
        <c:forEach items="${menus}" var="parentMenu">
            <li>
                <a href="javascript:;" class="module_tab" data-id="${parentMenu.resKey}">
                    <i class="fa ${parentMenu.icon} <c:if  test="${parentMenu.icon=='' || parentMenu.icon==null}">fa-reorder</c:if>"></i>
                    <span class="">${parentMenu.name}</span>
                </a>
            </li>
        </c:forEach>
        </ul>
        <ul class="nav navbar-nav navbar-right off-right">
            <li class="notifications dropdown hidden-xs">
                <a href="javascript:;" data-toggle="dropdown">
                    <i class="fa fa-bell"></i>
                    <div class="badge badge-top bg-danger animated flash">2</div>
                </a>
                <div class="dropdown-menu dropdown-menu-right animated slideInRight">
                    <div class="panel bg-white no-border no-margin">
                        <div class="panel-heading no-radius">
                            <small>
                                <b>Notifications</b>
                            </small>
                            <small class="pull-right">
                                <a href="javascript:;" class="mg-r-xs">mark as read</a>•
                                <a href="javascript:;" class="fa fa-cog mg-l-xs"></a>
                            </small>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a href="javascript:;">
                                        <span class="pull-left mg-t-xs mg-r-md">
                                            <img src="${ctx}/images/avatar.jpg" class="avatar avatar-sm img-circle" alt="">
                                        </span>
                                    <div class="m-body show pd-t-xs">
                                        <span>Dean Winchester</span>
                                        <span>Posted on to your wall</span>
                                        <small>2 mins ago</small>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a href="javascript:;">
                                        <span class="pull-left mg-t-xs mg-r-md">
                                        <img src="${ctx}/images/avatar.jpg" class="avatar avatar-sm img-circle" alt="">
                                        </span>
                                    <div class="m-body show pd-t-xs">
                                        <span>Application</span>
                                        <span>Where is my workspace widget</span>
                                        <small>5 days ago</small>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="panel-footer no-border">
                            <a href="javascript:;">See all notifications</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="quickmenu">
                <a href="javascript:;" data-toggle="dropdown">
                    <span class="pull-left mg-r-xs hidden-xs">${userSession.userName}</span>
                    <img src="${ctx}/images/avatar.jpg" class="avatar pull-left img-circle" alt="user" title="user">
                    <i class="caret mg-l-xs hidden-xs no-margin"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right mg-r-xs">
                    <li>
                        <a href="javascript:;">
                            <div class="pd-t-sm">
                                ${userSession.loginName}
                                <small class="text-muted">${userSession.organizationName}</small>
                            </div>
                        </a>
                    </li>
                 <li>
                        <a href="javascript:;">个人设置</a>
                    </li>
                    <li>
                        <a href="javascript:;">帮助 <i class="fa fa-question-circle"></i></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:logout();">退出</a>
                    </li>
                </ul>
            </li>
        </ul>
    </header>

    <section class="layout">
        <aside class="sidebar collapsible canvas-left" style="display: none;">
            <div class="scroll-menu">
                <nav class="main-navigation slimscroll" data-height="auto" data-size="4px" data-color="#ddd" data-distance="0">
                    <c:forEach items="${menus}" var="module">
                        <ul id="${module.resKey}" class="module_menu" style="display: none;">
                            <%--<li>--%>
                                <%--<a href="javascript:;" style="background: #4A5161;">--%>
                                    <%--<i class="fa ${module.icon} <c:if  test="${module.icon=='' || module.icon==null}">fa-reorder</c:if>"></i>--%>
                                    <%--<span>${module.name}</span>--%>
                                <%--</a>--%>
                            <%--</li>--%>
                            <c:forEach items="${module.children}" var="parentMenu">
                                <c:set var="hisChild" value="${! empty parentMenu.children}"></c:set>
                                <li <c:if test="${hisChild}">class="dropdown"</c:if>>
                                    <c:if test="${hisChild}">
                                        <a href="javascript:;" data-toggle="dropdown">
                                            <i class="fa ${parentMenu.icon} <c:if  test="${parentMenu.icon=='' || parentMenu.icon==null}">fa-reorder</c:if>"></i>
                                            <span>${parentMenu.name}</span>
                                            <i class="toggle-accordion"></i>
                                        </a>
                                    </c:if>
                                    <c:if test="${!hisChild}">
                                        <a href="javascript:addTab('${parentMenu.resKey}','${parentMenu.name}','${ctx}${parentMenu.resUrl}');">
                                            <i class="fa ${parentMenu.icon} <c:if  test="${parentMenu.icon=='' || parentMenu.icon==null}">fa-reorder</c:if>"></i>
                                            <span>${parentMenu.name}</span>
                                        </a>
                                    </c:if>
                                    <c:if test="${hisChild}">
                                        <ul class="dropdown-menu">
                                            <c:forEach items="${parentMenu.children}" var="childMenu">
                                                <li>
                                                    <a href="javascript:addTab('${childMenu.resKey}','${childMenu.name}','${ctx}${childMenu.resUrl}');" >
                                                        <span>${childMenu.name}</span>
                                                    </a>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </c:if>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                    <%--<ul>--%>
                        <%--<li>--%>
                            <%--<a href="javascript:addTab('index','主页','index.shtml');">--%>
                                <%--<i class="fa fa-home"></i>--%>
                                <%--<span>主页</span>--%>
                            <%--</a>--%>
                        <%--</li>--%>
                        <%--<c:forEach items="${menus}" var="parentMenu">--%>
                            <%--<c:set var="hisChild" value="${! empty parentMenu.children}"></c:set>--%>
                                <%--<li <c:if test="${hisChild}">class="dropdown"</c:if>>--%>
                                    <%--<c:if test="${hisChild}">--%>
                                        <%--<a href="javascript:;" data-toggle="dropdown">--%>
                                            <%--<i class="fa ${parentMenu.icon} <c:if  test="${parentMenu.icon=='' || parentMenu.icon==null}">fa-reorder</c:if>"></i>--%>
                                            <%--<span>${parentMenu.name}</span>--%>
                                            <%--<i class="toggle-accordion"></i>--%>
                                        <%--</a>--%>
                                    <%--</c:if>--%>
                                    <%--<c:if test="${!hisChild}">--%>
                                        <%--<a href="javascript:addTab('${parentMenu.resKey}','${parentMenu.name}','${ctx}${parentMenu.resUrl}');">--%>
                                            <%--<i class="fa ${parentMenu.icon} <c:if  test="${parentMenu.icon=='' || parentMenu.icon==null}">fa-reorder</c:if>"></i>--%>
                                            <%--<span>${parentMenu.name}</span>--%>
                                        <%--</a>--%>
                                    <%--</c:if>--%>
                                    <%--<c:if test="${hisChild}">--%>
                                        <%--<ul class="dropdown-menu">--%>
                                            <%--<c:forEach items="${parentMenu.children}" var="childMenu">--%>
                                                <%--<li>--%>
                                                    <%--<a href="javascript:addTab('${childMenu.resKey}','${childMenu.name}','${ctx}${childMenu.resUrl}');" >--%>
                                                        <%--<span>${childMenu.name}</span>--%>
                                                    <%--</a>--%>
                                                <%--</li>--%>
                                            <%--</c:forEach>--%>
                                        <%--</ul>--%>
                                    <%--</c:if>--%>
                            <%--</li>--%>
                        <%--</c:forEach>--%>
                    <%--</ul>--%>
                </nav>
            </div>

            <footer>
                <div class="footer-toolbar pull-left">
                    <a href="javascript:;" class="toggle-sidebar pull-right hidden-xs">
                        <i class="fa fa-angle-left"></i>
                    </a>
                </div>
            </footer>
        </aside>
        <section class="main-content">
            <div class="content-wrap no-padding">
                <div class="easyui-tabs" style="width: 100%;height:100%"></div>
            </div>
        </section>
    </section>
</div>
</body>
</html>
