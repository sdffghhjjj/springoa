    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    
	<meta http-equiv="Cache-Control" content="no-store"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="0"/>


    <script src="${ctx}/vendor/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="${ctx}/vendor/jquery-extends.js" type="text/javascript"></script>   
    <script src="${ctx}/vendor/jquery.placeholder.js" type="text/javascript"></script>
    <script src="${ctx}/vendor/jquery.slimscroll.js" type="text/javascript"></script>
    <script src="${ctx}/vendor/modernizr.js" type="text/javascript"></script>    
    
    <script src="${ctx}/vendor/plugins/tools/jquery.cookie.js" type="text/javascript"></script>
    
    <script src="${ctx }/vendor/plugins/validate/jquery.validate.pack.js" type="text/javascript"></script>
	<script src="${ctx }/vendor/plugins/validate/messages_cn.js" type="text/javascript"></script>
	<script src="${ctx }/vendor/plugins/qtip/jquery.qtip.pack.js" type="text/javascript"></script>

	<script src="${ctx }/vendor/plugins/html/jquery.outerhtml.js" type="text/javascript"></script>
	<script src="${ctx }/vendor/plugins/blockui/jquery.blockUI.js" type="text/javascript"></script>
	
    <!--[if lt IE 9]>
    <script src="${ctx}/vendor/html5shiv.js"></script>
    <script src="${ctx}/vendor/respond.min.js"></script>
    <![endif]-->
        
        
<%@ include file="/common/include-jquery-ui-theme.jsp" %>
<%@ include file="/common/include-base-styles.jsp" %>
<%@ include file="/common/include-custom-styles.jsp" %>

<script type="text/javascript">
	var webPath = '${ctx}';
    var imagePath = '${imagePath}';
    var ctx = '<%=request.getContextPath() %>';
</script>