package cn.tanghom.app.system.controller;

import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.StringUtils;
import cn.tanghom.support.websocket.MsgPublisher;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 描述
 *
 * @author tanghom<tanghom@qq.com> on 2016/7/7
 */
@Controller
@RequestMapping(value="msg")
public class MessageController {

    /**
     * notify
     *
     * @return
     */
    @RequestMapping(value = "notify", method = RequestMethod.GET)
    public String notifyMsg() {
        return CommonKey.WEB_PATH + "/system/msg/notify";
    }

    @RequestMapping(value = "/notify", method = RequestMethod.POST)
    @ResponseBody
    public String sendAll(String msg) {
        try {
            if(StringUtils.isNullOrEmpty(msg)){
                return CommonUtils.msgCallback(false, "发送失败,请填写内容", "", null);
            }
            MsgPublisher.getInstance().publish("系统通知", msg);
            return CommonUtils.msgCallback(true, "发送成功", "", null);
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "发送失败", "", null);
        }
    }
}
