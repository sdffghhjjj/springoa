package cn.tanghom.app.streamming.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.tanghom.app.streamming.model.MaterialData;

public interface MaterialDataRepository extends JpaRepository<MaterialData, Long> {
}