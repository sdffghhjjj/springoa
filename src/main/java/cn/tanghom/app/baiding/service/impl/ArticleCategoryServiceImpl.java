package cn.tanghom.app.baiding.service.impl;

import cn.tanghom.app.baiding.mapper.ArticleCategoryMapper;
import cn.tanghom.app.baiding.model.ArticleCategory;
import cn.tanghom.app.baiding.service.ArticleCategoryService;
import cn.tanghom.support.utils.CommonUtils;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
 * 文章分类service实现
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/25
 */
@Service
public class ArticleCategoryServiceImpl implements ArticleCategoryService {
    @Resource
    private ArticleCategoryMapper articleCategoryMapper;

    @Override
    public String saveOrUpdate(ArticleCategory articleCategory) {
        try {
            if (articleCategory.getId() == null) {// 添加
            	if(articleCategory.getParentId()==null){
            		articleCategory.setParentId(0L);
            	}
                articleCategoryMapper.save(articleCategory);

            } else {// 修改
                ArticleCategory ac = articleCategoryMapper.findById(articleCategory.getId());
                ac.setName(articleCategory.getName());
                ac.setDescription(articleCategory.getDescription());
                ac.setSort(articleCategory.getSort());
                ac.setStatus(articleCategory.getStatus());
                articleCategoryMapper.update(articleCategory);
            }
            return CommonUtils.msgCallback(true, "操作成功", "", null);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void save(ArticleCategory articleCategory) {
        articleCategoryMapper.save(articleCategory);
    }

    @Override
    public void update(ArticleCategory articleCategory) {
        articleCategoryMapper.update(articleCategory);
    }

    @Override
    public void delete(Long id) {
        articleCategoryMapper.delete(id);
    }

    @Override
    public ArticleCategory findById(Long id) {
        return articleCategoryMapper.findById(id);
    }

    @Override
    public List<ArticleCategory> findListByParams(String name, Integer status) {
        return articleCategoryMapper.findListByParams(name,status);
    }
    
    public List<ArticleCategory> findListByParentId(Long pid, Integer status){
    	return articleCategoryMapper.queryListByParentId(pid,status);
    }
}
