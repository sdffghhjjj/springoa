package cn.tanghom.app.baiding.service;


import cn.tanghom.app.baiding.model.ArticleCategory;

import java.util.List;

/**
 * 文章分类管理逻辑处理层
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
public interface ArticleCategoryService {
    /**
     * 保存文章分类
     *
     * @param articleCategory
     */
    String saveOrUpdate(ArticleCategory articleCategory);

    /**
     * 保存文章分类
     *
     * @param articleCategory
     */
    void save(ArticleCategory articleCategory);

    /**
     * 更新保存
     *
     * @param articleCategory
     */
    void update(ArticleCategory articleCategory);

    /**
     * 根据iD删除文章分类
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询文章分类
     *
     * @param id
     * @return
     */
    ArticleCategory findById(Long id);

    /**
     * 按条件查询所有文章分类
     *
     * @return
     */
    List<ArticleCategory> findListByParams(String name, Integer status);
    
    List<ArticleCategory> findListByParentId(Long pid, Integer status);

}
