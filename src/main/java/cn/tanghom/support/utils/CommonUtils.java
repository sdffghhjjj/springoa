package cn.tanghom.support.utils;

import cn.tanghom.app.system.model.User;

import org.activiti.conf.CommonKey;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公共
 * 
 * @author tanghom <tanghom@qq.com> 2015-11-18
 */
public class CommonUtils {
	/**
	 * 获取用户session
	 * @return
	 */
	public static User getUserSession(){
		Session session = SecurityUtils.getSubject().getSession();
		User user = (User) session.getAttribute(CommonKey.USER_SESSION);
		return user;
	}

	/**
	 * 判断是否POST提交，是POST返回true，不是POST提交返回false
	 * 
	 * @return 是POST返回true，不是POST提交返回false
	 */
	public static Boolean isPost() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		if ("POST".equals(request.getMethod())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * AJAX提交返回信息
	 * 
	 * @param status
	 *            是否成功，true成功，false 异常
	 * @param info
	 *            返回信息提示
	 * @param url
	 *            返回信息提示
	 * @param data
	 *            数据json
	 * @return 返回信息
	 */
	public static String msgCallback(Boolean status, String info, String url, String data) {
		Map<String, Object> callback = new HashMap<String, Object>();
		callback.put("status", status);
		callback.put("info", info);
		callback.put("url", url);
		callback.put("data", data);
		return JsonUtil.toJSONString(callback);
	}
	public static String msgCallback(Boolean status, String info, String data) {
		Map<String, Object> callback = new HashMap<String, Object>();
		callback.put("status", status);
		callback.put("info", info);
		callback.put("data", data);
		return JsonUtil.toJSONString(callback);
	}
	

	/**
	 * 保留两个小数
	 * 
	 * @return
	 */
	public static String formatDouble(Double b) {
		BigDecimal bg = new BigDecimal(b);
		return bg.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
	}


	/**
	 * 用来去掉List中空值和相同项的。
	 * 
	 * @param list
	 * @return
	 */
	public static List<String> removeSameItem(List<String> list) {
		List<String> difList = new ArrayList<String>();
		for (String t : list) {
			if (t != null && !difList.contains(t)) {
				difList.add(t);
			}
		}
		return difList;
	}

	/**
	 * 返回用户的IP地址
	 * 
	 * @param request
	 * @return
	 */
	public static String toIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
}
