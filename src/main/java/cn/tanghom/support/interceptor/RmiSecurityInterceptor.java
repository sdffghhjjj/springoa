package cn.tanghom.support.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.rmi.server.RemoteServer;
import java.util.Set;

/**
 * 描述
 *
 * @author tanghom<tanghom@qq.com> 2016/4/20
 */
public class RmiSecurityInterceptor implements MethodInterceptor {
    private Set allowed;

    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        String clientHost = RemoteServer.getClientHost();
        if (allowed != null && allowed.contains(clientHost)) {
            return methodInvocation.proceed();
        } else {
            throw new SecurityException("非法访问。");
        }
    }

    public void setAllowed(Set allowed) {
        this.allowed = allowed;
    }
}
