package org.activiti.conf;

import java.util.Date;


/**
 * 公共
 * 
 * @author tanghom <tanghom@qq.com> 2015-11-18
 */
public class CommonKey {
	// 页面路径 相对于WEB-INF/views,实际上就是theme的路径
	public static String WEB_PATH = "default";
	
	// UserSession
	public static final String USER_SESSION = "userSession";
	// webscoketUserSession
	public static final String WEBSOCKET_USER_SESSION = "webscoketUserSession";
	// 菜单session
	public static final String MENU_SESSION = "menuSession";
	
	//数据字典cache
	public static final String CACHE_KEY_DICTIONARY = "cacheKeyDictionary";
	
		
	/***************** session key *****************/
    public static final String CURRENT_USER = "user";
    public static final String SESSION_FORCE_LOGOUT_KEY = "session.force.logout";
    
	/***************** default password (123456) *****************/
	public static final String DEFAULT_PASSWORD = "14e1b600b1fd579f47433b88e8d85291";
	
	public static long SYSY_INIT_TIME = new Date().getTime();
	
	
	public static PropertiesLoader application = new PropertiesLoader("application.properties","application.local.properties");
	
	public static PropertiesLoader jdbc = new PropertiesLoader("jdbc.properties","jdbc.local.properties");

}
