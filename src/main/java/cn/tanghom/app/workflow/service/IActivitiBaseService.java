package cn.tanghom.app.workflow.service;

import cn.tanghom.app.system.model.User;



public interface IActivitiBaseService {

	/**
     * 删除用户和组的关系
     */
	public void deleteAllUser() throws Exception;
	
	/**
     * 删除用户和组的关系
     */
    public void deleteAllRole() throws Exception;
    
    /**
     * 删除用户和组的关系
     */
    public void deleteAllMemerShip() throws Exception;
    
    /**
     * 更新用户和组的关系
     */
    public void updateMembership(String userId, String groupId) throws Exception;
    
    /**
     * 添加一个用户到Activiti {@link org.activiti.engine.identity.User}
     * @param user  用户对象, {@link User}
     */
    void saveActivitiUser(User user);
       
    
}
