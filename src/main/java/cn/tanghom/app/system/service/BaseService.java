/**
 * Project Name:SpringOA
 * File Name:IBaseService.java
 * Package Name:com.zml.oa.service
 * Date:2014-11-9下午5:37:16
 *
 */
package cn.tanghom.app.system.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import cn.tanghom.support.page.Pager;


/**
 * @ClassName: IBaseService
 * @Description:IBaseService
 * @author: zml
 * @date: 2014-11-9 下午5:37:16
 *
 */
public interface BaseService<T,PK extends Serializable> {

	 public List<T> getAllList(String tableSimpleName) throws Exception;
	 
	 public T getUnique(String tableSimpleName,String[] columns,String[] values) throws Exception;
	 
	 public List<T> findByWhere(String tableSimpleName,String[] columns,String[] values) throws Exception;
	 
	 public long getCount(String tableSimpleName) throws Exception;
	 
	 public int insert(T bean) throws Exception;
	 
	 public int saveOrUpdate(T bean) throws Exception;

	 public int delete(Map<String, Object> param) throws Exception;
	 
	 public int update(T bean) throws Exception;
	 
	 public T getBean(final Class<T> obj,final Serializable id) throws Exception;
	 
	 public List<T> findByPage(String tableSimpleName,String[] columns,String[] values) throws Exception;
	 
	 public List<T> getListPage(String tableSimpleName,String[] columns,String[] values, Pager<T> page) throws Exception;
	
}
