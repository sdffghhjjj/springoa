package cn.tanghom.app.system.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 角色数据库实体对象
 * @author tanghom <tanghom@qq.com> 2015-11-18
 *
 */
public class Role implements Serializable{
	private static final long serialVersionUID = -290677296363477756L;
	
	private Long id;
	private String name;//名称
	private String roleKey;//唯一标识
	private String type; // Role type == activiti group type
	private String description;
	private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常
	private Integer sort = 0;//排序
	private List<User> users = new ArrayList<User>();
	private List<Resources> resources = new ArrayList<Resources>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoleKey() {
		return roleKey;
	}

	public void setRoleKey(String roleKey) {
		this.roleKey = roleKey;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Resources> getResources() {
		return resources;
	}

	public void setResources(List<Resources> resources) {
		this.resources = resources;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
