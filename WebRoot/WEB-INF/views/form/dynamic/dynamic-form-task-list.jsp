<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en" >
  <head> 
  	<%@include file="/common/head.jsp"%>
	<title>待办任务列表</title>
	
	
	<script src="${ctx }/js/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        var processType = '${empty processType ? param.processType : processType}';
    </script>
	<script src="${ctx }/js/activiti/workflow.js" type="text/javascript"></script>
	<script src="${ctx }/js/form/dynamic/dynamic-form-handler.js" type="text/javascript"></script>
</head>

<body>
	<c:if test="${not empty message}">
		<div id="message" class="alert alert-success">${message}</div>
		<!-- 自动隐藏提示信息 -->
		<script type="text/javascript">
		setTimeout(function() {
			$('#message').hide('slow');
		}, 5000);
		</script>
	</c:if>
	<table>
		<tr>
			<th>任务ID</th>
			<th>任务Key</th>
			<th>任务名称</th>
			<th>流程定义ID</th>
			<th>流程实例ID</th>
			<th>优先级</th>
			<th>任务创建日期</th>
			<th>任务逾期日</th>
			<th>任务描述</th>
			<th>任务所属人</th>
			<th>操作</th>
		</tr>

		<c:forEach items="${tasks }" var="task">
		<tr>
			<td>${task.id }</td>
			<td>${task.taskDefinitionKey }</td>
			<td>${task.name }</td>
			<td>${task.processDefinitionId }</td>
			<td>${task.processInstanceId }</td>
			<td>${task.priority }</td>
			<td>${task.createTime }</td>
			<td>${task.dueDate }</td>
			<td>${task.description }</td>
			<td>${task.owner }</td>
			<td>
				<c:if test="${empty task.assignee }">
					<a class="claim" href="${ctx }/form/dynamic/task/claim/${task.id}?processType=${param.processType}">签收</a>
				</c:if>
				<c:if test="${not empty task.assignee }">
					<%-- 此处用tkey记录当前节点的名称 --%>
					<a class="handle" tkey='${task.taskDefinitionKey }' tname='${task.name }' tid='${task.id }' href="#">办理</a>
				</c:if>
			</td>
		</tr>
		</c:forEach>
	</table>

	<!-- 办理任务对话框 -->
	<div id="handleTemplate" class="template"></div>

</body>
</html>
