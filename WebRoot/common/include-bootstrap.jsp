<link rel="stylesheet" type="text/css" href="${ctx}/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="${ctx}/vendor/bootstrap/css/bootstrap-datetimepicker.min.css" />

<script type="text/javascript" src="${ctx}/vendor/bootstrap/js/bootstrap.js"  ></script>
<script type="text/javascript" src="${ctx}/vendor/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="${ctx}/vendor/bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>

