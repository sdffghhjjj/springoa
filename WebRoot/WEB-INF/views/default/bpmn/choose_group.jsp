﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
  <head> 
  	<%@include file="/common/head.jsp"%>  
 
	<title>选择候选用户组</title>
</head>

<body>
	<input type="hidden" id="taskDefKey" value="${taskDefKey }"/>
	<!-- 此处的choose_group.js应该卸载bpmn.js -->
	<table id="group_datagrid" title="候选组列表"></table>
    
</body>

</html>
