package cn.tanghom.app.system.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 附件
 *
 * @author 汤宏
 */
public class Accessory implements Serializable {

    public static final long serialVersionUID = 5563600397870237225L;
    /**
     * 图片
     */
    public static final int TYPE_IMG = 1;
    /**
     * 文件
     */
    public static final int TYPE_FILE = 2;

    private Long id;

    /**
     * 文件名称
     */
    private String title;

    /**
     * 上级Id
     */
    private Long parentId;


    /**
     * 地址
     */
    private String path;

    /**
     * 类型 1.图片，2.文件
     */
    private Integer type;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 上传时间
     */
    private Date uploadTime;
    /**
     * 上传人Id
     */
    private Long userId;

    /**
     * 上传人名称
     */
    private String userName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUser(User user) {
        this.userId = user.getId();
        this.userName = user.getUserName();
    }
}
