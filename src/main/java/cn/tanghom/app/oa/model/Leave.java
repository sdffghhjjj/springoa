package cn.tanghom.app.oa.model;

import cn.tanghom.app.system.model.User;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 请假
 *
 * @author tanghom<tanghom@qq.com> on 2016/6/30
 */
@Entity
@Table(name = "t_oa_leave")
public class Leave implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true)	
    private Long id;
	
	@Column(name = "USER_ID")
    private String userId; //eq loginName
	
	@Column(name = "USER_NAME")
    private String userName;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")   
    @Column(name = "create_time")
    private Date createTime;
	
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")   
    @Column(name = "start_time")
    private Date startTime;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "end_time")
    private Date endTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date realityStartTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date realityEndTime;
    
    @Column(name = "apply_time")
    private Date applyTime;
    
    @Column(name = "leave_type")
    private String leaveType;
    
    @Column(name = "reason")
    private String reason;
    
    @Column(name = "status")
    private Integer status;//状态，1正常，0取消
    
    @Column(name = "process_instance_id")
    private String processInstanceId; //运行中的流程实例ID

    //-- 临时属性 --//
    @Transient 
    private User user;
    // 流程任务
    @Transient 
    private Task task;
    @Transient 
    private Map<String, Object> variables;
    // 运行中的流程实例
    @Transient 
    private ProcessInstance processInstance;
    // 历史的流程实例
    @Transient 
    private HistoricProcessInstance historicProcessInstance;
    // 流程定义
    @Transient 
    private ProcessDefinition processDefinition;

    public ProcessInstance getProcessInstance() {
        return processInstance;
    }

    public void setProcessInstance(ProcessInstance processInstance) {
        this.processInstance = processInstance;
    }
    @Column
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REALITY_START_TIME")
    public Date getRealityStartTime() {
        return realityStartTime;
    }

    public void setRealityStartTime(Date realityStartTime) {
        this.realityStartTime = realityStartTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REALITY_END_TIME")
    public Date getRealityEndTime() {
        return realityEndTime;
    }

    public void setRealityEndTime(Date realityEndTime) {
        this.realityEndTime = realityEndTime;
    }
    

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    public HistoricProcessInstance getHistoricProcessInstance() {
        return historicProcessInstance;
    }

    public void setHistoricProcessInstance(HistoricProcessInstance historicProcessInstance) {
        this.historicProcessInstance = historicProcessInstance;
    }

    public ProcessDefinition getProcessDefinition() {
        return processDefinition;
    }

    public void setProcessDefinition(ProcessDefinition processDefinition) {
        this.processDefinition = processDefinition;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.userId = user.getLoginName();
        this.userName = user.getUserName();
        this.user = user;
    }

}
