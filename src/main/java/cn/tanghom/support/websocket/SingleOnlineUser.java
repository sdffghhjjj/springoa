package cn.tanghom.support.websocket;

import cn.tanghom.support.websocket.SystemWebSocketHandler;

import org.activiti.conf.CommonKey;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 添加用户到在线列表
 * @author liulei
 *
 */
public class SingleOnlineUser {
    /**
     * 给数组赋值
     * @return mapOnline
     */
    public static Map<String,Long> getMapOnline() {
        ArrayList<WebSocketSession> sessions=new SystemWebSocketHandler().session();
        Map<String, Long> mapOnline = new HashMap<String, Long>();
        for(WebSocketSession u:sessions){
            String LoginName=u.getPrincipal().getName();
            Long userId=(Long) u.getAttributes().get(CommonKey.WEBSOCKET_USER_SESSION);
            mapOnline.put(LoginName,userId);
        }
        return mapOnline;
    }
    /**
     * 给数组赋值
     * @return mapOnline
     */
    public static String getMapOnlineStr() {
        ArrayList<WebSocketSession> sessions=new SystemWebSocketHandler().session();
        String userIds = "";
        for(WebSocketSession u:sessions){
            Long userId=(Long) u.getAttributes().get(CommonKey.WEBSOCKET_USER_SESSION);
            userIds += userId + ",";
        }
        if(userIds != "")
            userIds = userIds.substring(0,userIds.length() - 1);
        return userIds;
    }

}
