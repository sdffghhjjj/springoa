<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>简历解析</title>
	<jsp:include page="../../header.jsp"></jsp:include>	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/vendor/kindeditor/plugins/file_manager/file_manager.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/vendor/webuploader/webuploader.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/vendor/webuploader/webuploader.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/vendor/webuploader/myuploader.js"></script>

</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>简历解析</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<form class="form-inlinle" action="${pageContext.request.contextPath}/oa/talent/parse" method="post" id="form">
				<div class="form-group">
					<label>分类(设置简历类别):</label>
					<select class="-form-control input-sm" name="categoryId" style="width: 200px;">
						<option value="">请选择</option>
						<c:forEach var="ac" items="${articleCategoryList}" varStatus="s">
							<option value="${ac.id}" ${article.categoryId==ac.id?'selected':''}>${ac.name}</option>
						</c:forEach>
					</select>
				</div>
	   		 	<div class="form-group">
				    	<label>*标题:</label>
			            <input type="text" name="fullName" value="${article.fullName}" placeholder="标题" class="-form-control input-sm" style="width: 400px;">
			            <label>简历ID:</label>
			            <input type="text" name="resume_id" value="${article.id}" placeholder="要解析的CV_ID，不为空的话，读取内部的简历ID的简历文本。" class="-form-control input-sm" style="width: 120px;">
				  </div>
				  <%--
					<div class="form-group" style="display:none" >
						<label>附件文档:</label>
						<div id="uploader-demo">
							<!--用来存放item-->
							<div id="fileList" class="uploader-list">
							<c:forEach var="access" items="${article.accessorys}">
								<div id="WU_FILE_${access.id}" class="file-item thumbnail upload-state-done">
									<img src="${imagePath}/${access.path}">
									<div class="info">${access.title}</div>
									<a href="javascript:;" class="file-cancel" onclick="delFile('WU_FILE_${access.id}')">×</a>
									<input type="hidden" name="fileId[]" value="${access.id}">
								</div>
							</c:forEach>
							</div>
							<div id="filePicker">选择图片</div>
						</div>
					</div>
					--%>
				   <div class="form-group">
				    	<label>*内容(粘贴简历文本，支持纯文本和HTML，推荐HTML):</label>
  	   		 		 	<textarea name="txt_content" id="editor" style="height:700px;width:100%;">
  	   		 		 	
  	   		 		 	${article.txt_content}
  	   		 		 	
  	   		 		 	</textarea>
				  </div>
				 <div class="form-group">
					 <label class="control-label">*格式：</label>
					 <select class="form-control input-sm" name="status" data-placeholder="状态" style="width: 100px;">
						 <option value="1" <c:if test="${article.status==1}">selected</c:if> >HTML</option>
						 <option value="0" <c:if test="${article.status==0 || article.status==null}">selected</c:if> >纯文本</option>
					 </select>
				 </div>
					<div class="form-group">
					      <input type="hidden" name="id" value="${article.id}"/>
					      <button type="button" class="btn btn-primary" onclick="sub();">提交保存</button>
				  </div>
          </form>
			</div>
		</section>
	</div>
</section>
  <script type="text/javascript" >
	
	
	function sub(){		
		var checkId =$('[name="resume_id"]').val();
		var checkContent =$('[name="txt_content"]').val();
			if(""==checkContent && checkId==""){
				msg.info("请填写内容")
				return;
			}
		if(checkId || checkContent){		
			$.ajax({
				type : "POST",
				data : $("#form").serialize(),
				url : $("#form").attr("action"),
				dataType : 'json',
				success:function(data) {
					if (data.status == 1) {
						msg.info(data.info);
						setTimeout(function(){
							window.location.href="${pageContext.request.contextPath}"+data.url;
						},3000);
					} else {
						msg.info(data.info);
					}
				}
			});			
		}
	}
	</script>
</body>
</html>
