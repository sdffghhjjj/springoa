package cn.tanghom.support.db;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;


public class MultipleDataSource extends AbstractRoutingDataSource {
    @Override
    public Object determineCurrentLookupKey() {
         return DynamicDataSourceHolder.getRouteKey();
    }
}
