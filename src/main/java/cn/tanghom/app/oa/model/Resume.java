package cn.tanghom.app.oa.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import cn.tanghom.support.shiro.Functions;

import com.unbiz.common.lang.BaseObject;

import org.springframework.data.annotation.Id;

@Data
@Document(collection="hd_talent_resume")
public class Resume extends BaseObject<Long>{	
	
	private static final long serialVersionUID = 1L;

	public static String collection = "hd_talent_resume";	
	
	public String name;
	public String fullName;
	
	public Integer cityId;
	public String companyName;
	public String school;
	public Integer anualSalary = 0;	
	
	public String expect_city;
	
	public Integer expect_salary = 0;
	
	public String expect_industry;
	
	//title ids use expect_function
	public String expect_position_titles;
	
	public int start_work_year = 0;
	
	public int degreeId = 0;
		
	public Integer hunter = 0;
	
	public Integer talentId; //人才Id
	
	public String txt_content;
	
	public String htm_content;
	
	Date createTime = new Date();
	
	int status = 0;
	
	public Long categoryId;//分类ID
	
	public Long getId(){
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
	@Data	
	@Document(collection="hd_talent_work_exp_new")
	public static class WorkExp extends BaseObject<Long> {
			
		public String start_time;
		public String end_time;
		
		public String company_name;
		public String position;	
		public String description;
		
		@Indexed
		@Field("resume_id")
		public Long resumeId;
		
		int step = 0;
		
		@Id
		public Long getId(){
			return id;
		}
		
		WorkExp(){
			
		}
		
		public WorkExp(String start){
			start_time = start;
			if(start!=null){
				step++;
			}
		}
		
		public WorkExp(String start,String end){
			start_time = start;
			end_time = end;
			if(start!=null){
				step++;
			}
		}	
	};
	
	@Data	
	@Document(collection="hd_talent_edu_exp_new")
	public static class EduExp extends BaseObject<Long> {
			
		String start_time;
		String end_time;

		String school;
		String profession;	
		String degree;
		
		int step = 0;	
		
		@Indexed
		@Field("resume_id")
		public Long resumeId;	
		
		@Id
		public Long getId(){
			return id;
		}		

		EduExp(){			
		}
		
		public EduExp(String start){
			start_time = start;
			if(start!=null){
				step++;
			}
		}
		
		public EduExp(String start,String end){
			start_time = start;
			end_time = end;
			if(start!=null){
				step++;
			}
		}
		
		
	}
	
	public List<WorkExp> works;
	
	public List<EduExp> edus;	
	
	public Resume(long id) {		
		this(id, ""+id);
	}
	
	
	public Resume(long id, String name) {		
		this.setKey(id);
		this.fullName = name;
	}
	
	Resume() {
		
	}
	
	
	public String getCategoryName() {
        return Functions.category(categoryId).getName();
    }
}
