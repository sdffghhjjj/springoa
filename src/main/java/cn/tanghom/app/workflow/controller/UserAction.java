/**
 * Project Name:SpringOA
 * File Name:UserAction.java
 * Package Name:com.zml.oa.action
 * Date:2014-11-9上午12:35:50
 *
 */
package cn.tanghom.app.workflow.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import org.activiti.conf.CommonKey;
import org.activiti.engine.IdentityService;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import cn.tanghom.app.system.model.Message;
import cn.tanghom.app.system.model.Organization;
import cn.tanghom.app.system.model.Role;
import cn.tanghom.app.system.model.User;
import cn.tanghom.app.system.service.OrganizationService;
import cn.tanghom.app.system.service.RoleService;
import cn.tanghom.app.system.service.UserService;
import cn.tanghom.app.workflow.service.IActivitiBaseService;
import cn.tanghom.support.page.Pager;
import cn.tanghom.support.page.Pagination;
import cn.tanghom.support.page.PaginationThreadUtils;



/**
 * @ClassName: UserAction
 * @Description:用户Controller
 * @author: zml
 * @date: 2014-11-9 上午12:35:50
 *
 */

@Controller
@RequestMapping("/userAction")
public class UserAction {
	private static final Logger logger = Logger.getLogger(UserAction.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private OrganizationService groupService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
    private SessionDAO sessionDAO;
	
	@Autowired
    protected IActivitiBaseService activitiBaseService;
	
	@Autowired
	protected IdentityService identityService;
	
	
	@RequiresPermissions("admin:session:forceLogout")
    @RequestMapping("/forceLogout/{sessionId}")
	@ResponseBody
    public Message forceLogout(@PathVariable("sessionId") String sessionId) {
		Message message = new Message();
        try {
            Session session = sessionDAO.readSession(sessionId);
            if(session != null) {
                session.setAttribute(CommonKey.SESSION_FORCE_LOGOUT_KEY, Boolean.TRUE);
            }
            message.setStatus(Boolean.TRUE);
            message.setMessage("强制退出成功！");
        } catch (Exception e) {
        	message.setStatus(Boolean.FALSE);
            message.setMessage("强制退出失败！");
        }
        return message;
    }
	
	/**
	 * 同步所有用户到activiti表
	 * @param redirectAttributes
	 * @return
	 * @throws Exception
	 */
	@RequiresPermissions("admin:user:syncUser")
	@RequestMapping("/syncUserToActiviti")
	@ResponseBody
	public Message syncUserToActiviti() throws Exception {
		Message message = new Message();
		try {
			this.synAllUserAndRoleToActiviti();
			message.setStatus(Boolean.TRUE);
            message.setMessage("同步用户信息成功！");
		} catch (Exception e) {
			message.setStatus(Boolean.FALSE);
            message.setMessage("同步用户信息失败！");
		}
		return message;
	}
	
	//如果执行删除，工作流审批中的代办任务和待签收任务将无法使用。（在act_ru_identitylink将查不到act_id_user、act_id_group和act_id_membership的信息）
	@RequiresPermissions("admin:user:delAllIdentifyData")
	@RequestMapping("/delAllIdentifyData")
	public String delAllIdentifyData(RedirectAttributes redirectAttributes) throws Exception {
		this.deleteAllActivitiIdentifyData();
		redirectAttributes.addFlashAttribute("msg", "成功删除工作流引擎Activiti的用户、角色以及关系！");
		return "redirect:/userAction/toList_page";
	}  
	
	
	/**
	 * 跳转选择候选人页面-easyui
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/toChooseUser")
	public ModelAndView toChooseUser(@RequestParam("multiSelect") boolean multiSelect, @RequestParam("taskDefKey") String taskDefKey) throws Exception{
		ModelAndView mv = new ModelAndView("bpmn/choose_user");
		List<Organization> groupList = this.groupService.findList();
		mv.addObject("taskDefKey", taskDefKey);
		mv.addObject("multiSelect", multiSelect);
		mv.addObject("groupList", groupList);
		return mv;
	}
	
	/**
	 * 跳转选择任务委派人页面-easyui
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/toChooseDelegateUser")
	public ModelAndView toChooseDelegateUser() throws Exception{
		ModelAndView mv = new ModelAndView("task/delegate_user");
		List<Organization> groupList = this.groupService.findList();
		mv.addObject("groupList", groupList);
		return mv;
	}
	
	/**
	 * 在tabs中根据groupId显示用户列表
	 * @return
	 */
	@RequestMapping(value = "/toShowUser")
	public ModelAndView toShowUser(
			@RequestParam("groupId") String groupId,
			@RequestParam("multiSelect") boolean multiSelect, 
			@RequestParam("taskDefKey") String taskDefKey){
		ModelAndView mv = new ModelAndView("bpmn/show_user");
		mv.addObject("groupId", groupId);
		mv.addObject("multiSelect", multiSelect);
		mv.addObject("taskDefKey", taskDefKey);
		return mv;
	}
	
	/**
	 * 在tabs中根据groupId显示用户列表
	 * @return
	 */
	@RequestMapping(value = "/toShowDelegateUser")
	public ModelAndView toShowDelegateUser(
			@RequestParam("groupId") String groupId){
		ModelAndView mv = new ModelAndView("task/show_user");
		mv.addObject("groupId", groupId);
		return mv;
	}
	
	/**
	 * 获取候选人列表 - easyui
	 * @param page
	 * @param rows
	 * @param groupId
	 * @param flag
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chooseUser")
	@ResponseBody
	public ModelAndView chooseUser(
			@RequestParam(value = "page", required = false) Integer page, 
			@RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "groupId", required = false) Long groupId) throws Exception{
		
		Pager<User> p = new Pager<User>(page, rows);
		
		List<User> userList = new ArrayList<>();
		if(groupId != null){
			userList = this.userService.getUserByGroupId(null,p);
		}else{
			userList = this.userService.getUserByGroupId(groupId,p);
		}
		List<Object> jsonList=new ArrayList<Object>(); 
		
		for(User user: p.getItems()){
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("id", user.getId());
			map.put("name", user.getUserName());
			map.put("group", user.getOrganizationName());
			map.put("registerDate", user.getCreateTime());
			jsonList.add(map);
		}
		
		Pagination pagination = PaginationThreadUtils.get();
		List<Organization> groupList = this.groupService.findList();
		ModelAndView model = new ModelAndView("bpmn/choose_user_page");
		model.addObject("userList", userList);
		model.addObject("groupList", groupList);
		model.addObject("groupId", groupId);
		
		return model;
	}
	
	
	 /**
     * 同步用户、角色数据到工作流
     * @throws Exception
     */   
	public void synAllUserAndRoleToActiviti() throws Exception {
		// 清空工作流用户、角色以及关系
        deleteAllActivitiIdentifyData();
        
        // 复制角色数据
        synRoleToActiviti();
        
        // 复制用户以及关系数据
        synUserWithRoleToActiviti();
		
	}
 
    /**
     * 删除工作流引擎Activiti的用户、角色以及关系
     * @throws Exception
     */   
	
	public void deleteAllActivitiIdentifyData() throws Exception {
		this.activitiBaseService.deleteAllMemerShip();
		this.activitiBaseService.deleteAllRole();
		this.activitiBaseService.deleteAllUser();
	}
	

	/**
     * 同步所有角色数据到{@link Group}
	 * @throws Exception 
     */
    private void synRoleToActiviti() throws Exception {
        List<Role> allGroup = this.roleService.findList();
        for (Role group : allGroup) {
            String groupId = group.getRoleKey();
            org.activiti.engine.identity.Group identity_group = identityService.newGroup(groupId);
            identity_group.setName(group.getName());
            identity_group.setType(group.getRoleKey());
            identityService.saveGroup(identity_group);
        }
    }
    
    /**
     * 复制用户以及关系数据
     * @throws Exception 
     */
    private void synUserWithRoleToActiviti() throws Exception {
        List<User> allUser = userService.findList();
        for (User user : allUser) {
            String userId = user.getId().toString();
            List<Role> roles = user.getRoles();
            // 添加一个用户到Activiti
            activitiBaseService.saveActivitiUser(user);
 
            // 角色和用户的关系
            for(Role role: roles){
            	 identityService.createMembership(userId, role.getRoleKey());
            }
        }
    }
    
    /**
     * 更新用户时同事更新membership
     * @param user
     * @throws Exception
     */
    private void updateMembership(User user) throws Exception{
    	for(Role role: user.getRoles()){
    		 this.activitiBaseService.updateMembership(user.getId().toString(), role.getRoleKey());
        }    	
    }
	
}
