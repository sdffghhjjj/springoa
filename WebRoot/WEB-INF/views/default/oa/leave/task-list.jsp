<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>请假待办任务列表</title>
    <jsp:include page="../../header.jsp"></jsp:include>
    <script src="${ctx}/js/activiti/workflow.js" type="text/javascript"></script>
    <script>
        $(function () {
            $('.trace').click(function(){
                var pid= $(this).attr('pid');
                var pdid= $(this).attr('pdid');
                var url = "/diagram-viewer/index.html?processDefinitionId="+pdid+"&processInstanceId="+pid;
                dialog({
                    fixed:true,
                    quickClose:true,
                    title : "流程跟踪",
                    width:$(".main-content").width()-300,
                    height:500,
                    content : "<iframe scrolling=\"auto\" frameborder=\"0\"  src=\""+url+"\" style=\"width:100%;height:100%;\"></iframe>"
                }).show();
            });
        });
    </script>
</head>
<body>
<section class="main-content">
    <div class="content-wrap">
        <section class="panel panel-default">
            <div class="panel-heading">
                <b>请假待办任务列表</b>
                <small class="pull-right">
                    <a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
                </small>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>假种</th>
                            <th>申请人</th>
                            <th>申请时间</th>
                            <th>开始时间 -- 结束时间</th>
                            <th>当前节点</th>
                            <th>流程开始时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${page.items}" var="leave">
                            <c:set var="task" value="${leave.task }"/>
                            <c:set var="pi" value="${leave.processInstance }"/>
                            <tr>
                                <td>${leave.leaveType}</td>
                                <td>${leave.userName}</td>
                                <td><fmt:formatDate pattern="YYYY/MM/dd HH:mm"  value="${leave.applyTime}" /></td>
                                <td><fmt:formatDate pattern="YYYY/MM/dd HH:mm"  value="${leave.startTime}" /> -- <fmt:formatDate pattern="YYYY/MM/dd HH:mm"  value="${leave.endTime}" /></td>
                                <td>
                                    <a href="javascript:;" class="trace"  pid="${pi.id }" pdid="${pi.processDefinitionId}" title="点击查看流程图">${task.name }</a>
                                </td>
                                <td><fmt:formatDate pattern="YYYY/MM/dd HH:mm"  value="${task.createTime}" /></td>
                                <td>
                                    <c:if test="${empty task.assignee }">
                                        <a href="javascript:;" url="${ctx }/oa/leave/task/claim/${task.id}" data-info="签收为我办理?" class="ajax-get label label-info">签收</a>
                                    </c:if>
                                    <c:if test="${not empty task.assignee }">
                                        <a href="javascript:;" url="${ctx }/oa/leave/task/complete/${task.id}" data-info="确认办理?" class="ajax-get label label-success">办理</a>

                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="pull-right">
                        <mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
                    </div>
                </div>
            </div>
            </section>
        </div>
    </section>
</body>
</html>