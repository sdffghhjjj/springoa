/**
 * main页js
 *
 * @author tanghom<tanghom@qq.com> 2016/4/28
 */
$(function () {
    /*查询未读消息数*/
    // unread();
    /*添加首页Tab*/
    addTab('index', '主页', 'index');

    /**
     * 模块菜单切换
     */
    $(".module_tab").click(function () {
        var _id = $(this).attr("data-id");
        $(this).parent('li').parent('ul').find('li').removeClass("active");
        $(this).parent('li').addClass("active");
        if('home'==_id){
            $('.sidebar').hide();
        }else{
            $(".main-navigation .module_menu").hide();
            $(".main-navigation").find("#" + _id).show();
            $('.sidebar').show();
        }
    });
});

/*添加Tab*/
function addTab(id, title, url) {
    if ($('.easyui-tabs').tabs('exists', title)) {
        $('.easyui-tabs').tabs('select', title);
    } else {
        var isClosable = true;
        if (id == 'index') {
            isClosable = false;
        }
        var $iframe_width = $(".main-content").width() + 'px';
        var $iframe_height = $(".main-content").height() - 30 + 'px';
        var content = '<iframe scrolling="auto" frameborder="0" id="' + id + '" src="' + url + '" style="width:' + $iframe_width + ';height:' + $iframe_height + ';"></iframe>';
        $('.easyui-tabs').tabs('add', {
            title: title,
            content: content,
            closable: isClosable
        });
    }
}
//更新tab,若存在刷新，不存在添加
function updateTab(id, title, url) {
    if ($('.easyui-tabs').tabs('exists', title)) {
        $('.easyui-tabs').tabs('select', title);
        var tab = $('.easyui-tabs').tabs('getSelected');
        var $iframe_width = $(".main-content").width() + 'px';
        var $iframe_height = $(".main-content").height() - 30 + 'px';
        var content = '<iframe scrolling="auto" frameborder="0" id="' + id + '" src="' + url + '" style="width:' + $iframe_width + ';height:' + $iframe_height + ';"></iframe>';
        $('.easyui-tabs').tabs('update', {
            tab: tab,
            options: {
                title: title,
                content: content
            }
        });
    } else {
        addTab(id, title, url);
    }
}


/*窗口大小改变监听*/
$(window).resize(function () {
    changeTabSize();
});
/*窗口tab内容改变*/
function changeTabSize() {
    var $height = $(".main-content").height() + 'px';
    var $width = $(".main-content").width() + 'px';
    $('.easyui-tabs').height($height);
    $('.easyui-tabs').width($width);
    $('.easyui-tabs .tabs-header').width($width);
    $('.easyui-tabs .tabs-header .tabs-wrap').width($width);
    $('.easyui-tabs .tabs-panels').width($width);
    $('.easyui-tabs .tabs-panels .panel').width($width);
    $('.easyui-tabs .tabs-panels .panel .panel-body').width($width);
    var $iframe_height = $(".main-content").height() - 30 + 'px';
    $('.easyui-tabs').find("iframe").css("height", $iframe_height);
    $('.easyui-tabs').find("iframe").css("width", $width);
}

function logout() {
    msg.confirm("确认要退出吗?", function () {
        window.location.href = "logout";
    }, "退出提醒");
}

var options = {"storageName": "cameo", "menuStateStorage": false};
var app = {
    initialize: function () {
        if (Modernizr.localstorage && $(".app").data("sidebar") !== "locked") {
            app.menuState();
        }
    }, menuState: function () {
        if (window.localStorage.getItem(options.storageName + "_navigation") !== null && options.menuStateStorage === true) {
            if (window.localStorage.getItem(options.storageName + "_navigation") === "0") {
                app.openMenuState();
            } else {
                app.closeMenuState();
            }
        }
    }, closeMenuState: function () {
        $(".app").addClass("small-sidebar");
        $(".toggle-sidebar  i").addClass("fa-angle-right");
    }, openMenuState: function () {
        $(".app").removeClass("small-sidebar");
        $(".toggle-sidebar i").removeClass("fa-angle-right");
    }
};
$(function () {
    $(".no-touch .slimscroll").each(function () {
        var data = $(this).data();
        $(this).slimScroll(data);
    });
    $('input, textarea').placeholder();
    $(document).on("click", ".toggle-sidebar ", function (e) {
        e.preventDefault();
        if ($(".app").hasClass("small-sidebar")) {
            app.openMenuState();
            window.localStorage.setItem(options.storageName + "_navigation", "0");
        } else {
            app.closeMenuState();
            window.localStorage.setItem(options.storageName + "_navigation", "1");
        }
        changeTabSize();
    });
    $(document).on("click touchstart", ".collapsible .main-navigation > ul> li > a", function (e) {
        var subMenu = $(this).next(), parent = $(this).closest("li");
        if (!subMenu.is("ul")) {
            return;
        }
        if ($(".app").hasClass("small-menu") && $(window).width() > 767) {
            return;
        }
        $(".sidebar li").removeClass("collapse-open");
        parent.addClass("collapse-open");
        if ((subMenu.is("ul")) && (subMenu.is(":visible")) && (!$(".app").hasClass("small-sidebar"))) {
            parent.removeClass("collapse-open");
            subMenu.slideUp();
        }
        if ((subMenu.is("ul")) && (!subMenu.is(":visible")) && (!$(".app").hasClass("small-sidebar"))) {
            $(".sidebar ul ul:visible").slideUp();
            subMenu.slideDown();
            parent.addClass("collapse-open");
        }
        if (subMenu.is("ul")) {
            return false;
        }
        e.stopPropagation();
        return true;
    });
    $(".main-navigation > ul > li.collapse-open").each(function () {
        $(this).children(".dropdown-menu").hide().show();
    });
    app.initialize();
});