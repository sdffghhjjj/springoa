package cn.tanghom.app.baiding.controller.web;

import cn.tanghom.app.baiding.model.Article;
import cn.tanghom.app.baiding.model.ArticleCategory;
import cn.tanghom.app.baiding.service.ArticleCategoryService;
import cn.tanghom.app.baiding.service.ArticleService;
import cn.tanghom.support.utils.CommonUtils;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文章分类列表管理控制层
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/article_category")
public class ArticleCategoryController {

    @Resource
    private ArticleCategoryService articleCategoryService;
    @Resource
    private ArticleService articleService;

    /**
     * 列表
     *
     * @param name
     * @param status
     * @param model
     * @return
     */
    @RequestMapping(value = "/list", method = {RequestMethod.POST, RequestMethod.GET})
    public String list(String name, Integer status, Model model) {
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParams(name, status);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/article_category/list";
    }

    /**
     * 新增文章分类页面
     *
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Long parentId,Model model) {
    	if(parentId!=null){
    		List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParentId(parentId, null);
    		model.addAttribute("articleCategoryList", articleCategoryList);
    	}
        return CommonKey.WEB_PATH + "/article_category/edit";
    }

    /**
     * 编辑文章分类页面
     *
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(Long id, Long parentId,Model model) {
    	if(parentId!=null){
    		List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParentId(parentId, null);
    		model.addAttribute("articleCategoryList", articleCategoryList);
    	}
        ArticleCategory articleCategory = articleCategoryService.findById(id);
        model.addAttribute("articleCategory", articleCategory);
        return CommonKey.WEB_PATH + "/article_category/edit";
    }

    /**
     * 保存文章分类
     *
     * @param articleCategory 文章分类对象
     * @return
     */
    @RequestMapping(value = "/save", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @ResponseBody
    public String add(ArticleCategory articleCategory) {
        try {
            String result = articleCategoryService.saveOrUpdate(articleCategory);
            return result;
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "操作失败，请重试！", "", null);
        }
    }

    /**
     * 删除文章分类
     *
     * @param id 文章分类ID
     * @return 结果
     */
    @ResponseBody
    @RequestMapping(value = "/delete", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    public String delete(Long id) {
        try {
            List<Article> articles = articleService.findListByParams(id,null);
            if(null!=articles){
                return CommonUtils.msgCallback(false, "分类下有文章，不能删除", "", null);
            }
            articleCategoryService.delete(id);
            return CommonUtils.msgCallback(true, "删除文章分类成功", "", null);
        } catch (Exception ex) {
            return CommonUtils.msgCallback(false, "删除文章分类失败", "", null);

        }
    }
}
