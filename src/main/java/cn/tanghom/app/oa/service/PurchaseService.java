package cn.tanghom.app.oa.service;

import java.util.Map;
import org.activiti.engine.runtime.ProcessInstance;

import cn.tanghom.app.oa.model.PurchaseApply;

public interface PurchaseService {
	ProcessInstance startWorkflow(PurchaseApply apply,String userid,Map<String,Object> variables);
	PurchaseApply getPurchase(int id);
	void updatePurchase(PurchaseApply a);
}
