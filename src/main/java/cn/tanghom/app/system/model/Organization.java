package cn.tanghom.app.system.model;

import java.io.Serializable;

/**
 * 组织架构数据库实体对象
 * @author tanghom <tanghom@qq.com> 2015-11-18
 *
 */
public class Organization implements Serializable{

    private static final long serialVersionUID = -5088563773118268950L;
	private Long id;//ID
	private String name;//名称
    private Long parentId;//上级ID
    private String parentName;//上级名称
	private String description;//描述
    private Integer sort;//排序
    private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
