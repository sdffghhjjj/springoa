
document.write('<script type="text/javascript" src="'+webPath+'/vendor/jquery-art-dialog/src/dialog-config.js"></script>');
document.write('<script type="text/javascript" src="'+webPath+'/vendor/jquery-art-dialog/src/popup.js"></script>');
document.write('<script type="text/javascript" src="'+webPath+'/vendor/jquery-art-dialog/src/dialog.js"></script>');
document.write('<script type="text/javascript" src="'+webPath+'/vendor/jquery-art-dialog/src/dialog-plus.js"></script>');
document.write('<script type="text/javascript" src="'+webPath+'/vendor/jquery-art-dialog/src/drag.js"></script>');

/**
 * 公共提示信息接口
 * alert:没按钮的提示框
 * confirm:带按钮的提示框
 * info:没有标题和按钮的提示框，2秒自动关闭 
 */
var msg = {
	alert : function(content,title) {
		var element = $('[i="dialog"]');
		if(element.length <=0){
			top.dialog({
				fixed:true,
				padding : 10,
				title : title != null && title != "" ? title : '提示信息',
				content : content,
				width : 200
			}).show();
		}
	},
	confirm : function(content,callback,title) {
		top.dialog({
			fixed:true,
			padding : 10,
			title : title != null && title != "" ? title : '提示信息',
			content : content,
			width : 250,
			okValue: '确定',
		    ok: callback,
		    cancelValue: '取消',
		    cancel: function () {}
		}).showModal();
	},
	info:function(content){
		var d = dialog({
			padding : 20,
			width:200,
		    content: "<span style='color: red;'>"+content+"</span>"
		});
		d.show();
		setTimeout(function () {
		    d.close().remove();
		}, 2000);
	},
	dialog : function(content,callback1,callback2,title){
		top.dialog({
			fixed : true,
			padding : 10,
			title : title != null && title != "" ? title : '提示信息',
			content : content,
			width : 250,
			button: [
		        {
		            value: '同意',
		            callback: callback1,
		            autofocus: true
		        },
		        {
		            value: '不同意',
		            callback: callback2
		        }
		    ]
		}).showModal();
	}
};


var model = {
		info : function(title,url) {
			var element = $('[i="dialog"]');
			if(element.length <=0){
				$.ajax({
					type:"get",
					url:url,
					dataType:"html",
					success:function(html){
						dialog({
							fixed:true,
							quickClose: true,
							title : title != null && title != "" ? title : '信息弹出框',
							content : html
						}).show();
					}
				});
			}
		}
};

/*function tip(obj,content,align){
	var d = dialog({
		padding : 0,
		align: align != null && align != "" ? align : 'top',
		content: content,
		quickClose: true
	});
	d.show(obj);
};*/

/**
 * 提示错误信息
 */
$.fn.err = function(title){
	$(this).parent('div').addClass('has-error');
	$(this).tooltip({
		template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow" style="border-top-color:#d9534f"></div><div class="tooltip-inner" style="background-color:#d9534f"></div></div>',
		title:title,trigger:'manual'}).tooltip('show');
	$(this).focus(function(){
		$(this).parent('div').removeClass('has-error');
		$(this).tooltip('destroy');
	});
};

/**
 * 关闭错误信息
 */
$.fn.closeErr = function(){
	$(this).parent('div').removeClass('has-error');
	$(this).tooltip('destroy');
};