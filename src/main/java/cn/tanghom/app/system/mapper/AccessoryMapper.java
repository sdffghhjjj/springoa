package cn.tanghom.app.system.mapper;

import cn.tanghom.app.system.model.Accessory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文件管理
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/26
 */
@Repository
public interface AccessoryMapper {
    /**
     * 保存文件信息
     * @param accessory 文件信息
     */
    void save(Accessory accessory);
    /**
     * 查询单个文件信息
     * @param id 文件Id
     * @return Accessory
     */
    Accessory getById(Long id);
    List<Accessory> findByIdStr(@Param(value = "idStr")String idStr);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

}
