<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>流程管理</title>
	<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>流程管理</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<div class="row table-search">
					<div class="col-xs-10 col-lg-offset-2 text-right">
						<form class="form-inline" action="${ctx }/workflow/deploy.shtml" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label>支持格式：zip、bar、bpmn、bpmn20.xml</label>
								<input type="file" class="form-control input-sm" name="file">
							</div>
							<input type="submit" class="btn btn-primary btn-sm" value="部署新流程" />
						</form>
					</div>
				</div>
				<table class="table">
					<tr>
						<th>ProcessDefinitionId</th>
						<th>DeploymentId</th>
						<th>名称</th>
						<th>KEY</th>
						<th>版本号</th>
						<th>XML</th>
						<th>图片</th>
						<th>部署时间</th>
						<th>是否挂起</th>
						<th>操作</th>
					</tr>
					<c:forEach items="${page.items }" var="object">
						<c:set var="process" value="${object[0] }" />
						<c:set var="deployment" value="${object[1] }" />
						<tr>
							<td>${process.id }</td>
							<td>${process.deploymentId }</td>
							<td>${process.name }</td>
							<td>${process.key }</td>
							<td>${process.version }</td>
							<td><a target="_blank" href='${ctx }/workflow/resource/read?processDefinitionId=${process.id}&resourceType=xml'>${process.resourceName }</a></td>
							<td><a target="_blank" href='${ctx }/workflow/resource/read?processDefinitionId=${process.id}&resourceType=image'>${process.diagramResourceName }</a></td>
							<td><fmt:formatDate pattern="YYYY/MM/dd HH:mm:ss"  value="${deployment.deploymentTime }" /></td>
							<td>${process.suspended} |
								<c:if test="${process.suspended }">
									<a href="processdefinition/update/active/${process.id}">激活</a>
								</c:if>
								<c:if test="${!process.suspended }">
									<a href="processdefinition/update/suspend/${process.id}">挂起</a>
								</c:if>
							</td>
							<td>
								<a href='${ctx }/workflow/process/delete?deploymentId=${process.deploymentId}'>删除</a>
								<a href='${ctx }/workflow/process/convert-to-model/${process.id}'>转换为Model</a>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${empty page}">
						<td colspan="8" class="td-center">无相关数据</td>
					</c:if>
				</table>
				<div class="clearfix">
					<div class="pull-right">
						<mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
					</div>
				</div>
			</div>
		</section>
	</div>
</section>
</body>
</html>