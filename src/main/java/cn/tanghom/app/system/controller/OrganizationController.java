package cn.tanghom.app.system.controller;

import cn.tanghom.app.system.model.Organization;
import cn.tanghom.app.system.service.OrganizationService;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 组织管理控制层
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/organization")
public class OrganizationController {

    @Resource
    private OrganizationService organizationService;


    /**
     * 组织机构管理首页
     * @return
     */
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String list() {
        return CommonKey.WEB_PATH + "/system/organization/index";
    }

    /**
     * 查询组织机构返回
     * @return
     */
    @RequestMapping(value = "json", method = RequestMethod.GET)
    @ResponseBody
    public String json() {
        String json = organizationService.findListToSimpleJson(null);
        return json;
    }

    /**
     * 编辑
     * @param id
     * @return
     */
    @RequestMapping(value = "edit", method = RequestMethod.GET)
    @ResponseBody
    public String editJson(Long id) {
        Organization organization = organizationService.findById(id);
        String json = JsonUtil.toJSONString(organization);
        return json;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    @ResponseBody
    public String addJson(Long id) {
        try {
            organizationService.delete(id);
            return CommonUtils.msgCallback(true, "操作成功", "", null);
        }catch (Exception e){
            return CommonUtils.msgCallback(false, "操作失败，" + e, "", null);
        }
    }

    /**
     * 保存
     * @param organization
     * @return
     */
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public String save(Organization organization) {
        try {
            String result = organizationService.saveOrUpdate(organization);
            return result;
        }catch (Exception e){
            return CommonUtils.msgCallback(false, "操作失败，请重试！" , "", null);
        }
    }
    /**
     * 修改ParentId
     * @param id
     * @param parentId
     * @return
     */
    @RequestMapping(value = "update_parent", method = RequestMethod.POST)
    @ResponseBody
    public String save(Long id,Long parentId) {
        try {
            Organization organization =organizationService.findById(id);
            organization.setParentId(parentId);
            organizationService.update(organization);
            return CommonUtils.msgCallback(true, "操作成功", "", null);

        }catch (Exception e){
            return CommonUtils.msgCallback(false, "操作失败，请重试！" , "", null);
        }
    }
}
