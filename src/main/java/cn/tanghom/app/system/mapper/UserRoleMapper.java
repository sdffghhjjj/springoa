package cn.tanghom.app.system.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * UserRoleMapper关系处理
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Repository
public interface UserRoleMapper {

    /**
     * 保存
     *
     * @param userId
     * @param roleId
     */
    void save(@Param(value = "userId") Long userId, @Param(value = "roleId") Long roleId);

    /**
     * 删除
     *
     * @param userId
     * @param roleId
     */
    void delete(@Param(value = "userId") Long userId, @Param(value = "roleId") Long roleId);


}
