<?php
	$htmlData = '';
	if (!empty($_POST['content1'])) {
		if (get_magic_quotes_gpc()) {
			$htmlData = stripslashes($_POST['content1']);
		} else {
			$htmlData = $_POST['content1'];
		}
	}
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<title>KindEditor PHP</title>
	<link rel="stylesheet" href="../plugins/file_manager/file_manager.css" />
	<script charset="utf-8" src="../kindeditor.js" type="text/javascript" ></script>
	<script type="text/javascript" >
		KE.show({
			id : 'content1',
			imageUploadJson : '../../php/upload_json.php',
			fileManagerJson : '../../php/file_manager_json.php',
			allowFileManager : true,
			afterCreate : function(id) {
				KE.event.ctrl(document, 13, function() {
					KE.util.setData(id);
					document.forms['example'].submit();
				});
				KE.event.ctrl(KE.g[id].iframeDoc, 13, function() {
					KE.util.setData(id);
					document.forms['example'].submit();
				});
			}
		});
	</script>
	
	

</head>
<body class="ke-content">
	
	<form name="example" method="post" action="demo.php">
		<textarea id="content1" name="content1" cols="100" rows="8" style="width:800px;height:500px;visibility:hidden;"><?php echo htmlspecialchars($htmlData); ?></textarea>
		<br />		
		<input type="button" value="提交" onclick="Ok();" style="width:100px" />&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" value="放弃" onclick="Cancel();" style="width:80px" />
	</form>
	
	<script type="text/javascript">

	// #### URLParams: holds all URL passed parameters (like ?Param1=Value1&Param2=Value2)
	var FCKURLParams = new Object() ;
	
	var aParams = document.location.search.substr(1).split('&') ;
	for ( var i = 0 ; i < aParams.length ; i++ )
	{
		var aParam = aParams[i].split('=') ;
		var sParamName  = aParam[0] ;
		var sParamValue = aParam[1] ;
	
		FCKURLParams[ sParamName ] = sParamValue ;
	}
	
	// It is preferable to have the oFCKeditor object defined in the opener window,
	// so all the configurations will be there. In this way the popup doesn't need
	// to take care of the configurations "clonning".
	
	
	try{
		oFCKeditorValue	= window.opener.document.getElementById( FCKURLParams[ 'el' ] ).value;
		//alert(oFCKeditorValue);
		//KE.html('content1',oFCKeditorValue);
		document.getElementById('content1').value=oFCKeditorValue;
	}
	catch(e)
	{}
	
	function Ok()
	{	
	try{
		window.opener.document.getElementById( FCKURLParams[ 'el' ] ).value = KE.html('content1');		// "true" means you want it formatted.
	}
	catch(e)
	{}
		window.opener.focus() ;
		window.close() ;
	}
	
	function Cancel()
	{	
		if (true)
		{
			if ( !confirm( 'Are you sure you want to cancel? Your changes will be lost.' ) )
				return ;
		}
	
		window.close() ;
	}


</script>
</body>
</html>

