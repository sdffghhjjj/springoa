package cn.tanghom.app.system.controller;

import cn.tanghom.app.system.model.Organization;
import cn.tanghom.app.system.model.Role;
import cn.tanghom.app.system.model.User;
import cn.tanghom.app.system.service.OrganizationService;
import cn.tanghom.app.system.service.RoleService;
import cn.tanghom.app.system.service.UserService;
import cn.tanghom.support.page.Pager;
import cn.tanghom.support.tree.TreeObject;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.websocket.SingleOnlineUser;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 用户管理控制层
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;
    @Resource
    private OrganizationService organizationService;
    @Resource
    private RoleService roleService;

    /**
     * @param model
     * @param userName
     * @param loginName
     * @param orgId
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "/list", method = {RequestMethod.POST, RequestMethod.GET})
    public String list(Model model, String userName, String loginName, Long orgId, Integer status, Long pageNo) {
        Pager<User> page = userService.findPageByParams(userName, loginName, orgId,status, pageNo);
        model.addAttribute("page", page);
        List<TreeObject> orgTree = organizationService.findToSelectTree();
        model.addAttribute("organizations", orgTree);
        return CommonKey.WEB_PATH + "/system/user/list";
    }
    /**
     * 新增用户页面
     *
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {
        List<TreeObject> orgTree = organizationService.findToSelectTree();
        model.addAttribute("organizations", orgTree);
        List<Role> roles = roleService.findList();
        model.addAttribute("roles", roles);
        return CommonKey.WEB_PATH + "/system/user/edit";
    }

    /**
     * 编辑用户页面
     *
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(Long id, Model model) {
        User user = userService.findById(id);
        model.addAttribute("user", user);
        List<TreeObject> orgTree = organizationService.findToSelectTree();
        model.addAttribute("organizations", orgTree);
        List<Role> roles = roleService.findList();
        model.addAttribute("roles", roles);
        return CommonKey.WEB_PATH + "/system/user/edit";
    }

    /**
     * 保存用户
     *
     * @param user   用户对象
     * @param roleId 角色ID数组
     * @return
     */
    @RequestMapping(value = "/save", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @ResponseBody
    public String add(User user, Long roleId[]) {
        try {
            String result = userService.saveOrUpdate(user, roleId);
            return result;
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "操作失败，" + e, "", null);
        }
    }

    /**
     * 删除用户
     *
     * @param id 用户ID
     * @return 结果
     */
    @ResponseBody
    @RequestMapping(value = "/delete", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    public String delete(Long id) {
        try {
            userService.delete(id);
            return CommonUtils.msgCallback(true, "删除用户成功", "", null);
        } catch (Exception ex) {
            return CommonUtils.msgCallback(false, "删除用户失败", "", null);

        }
    }
    /**
     * 查询在线用户
     * @return
     */
    @RequestMapping("online")
    public String singleOnlineUser(Model model, String name, String loginName, Long orgId, Long pageNo){
        List<TreeObject> orgTree = organizationService.findToSelectTree();
        model.addAttribute("organizations", orgTree);
        String ids="";
        @SuppressWarnings("static-access")
        Map<String, Long> mapOnline=new SingleOnlineUser().getMapOnline();
        for (String key : mapOnline.keySet()) {
            ids+=mapOnline.get(key)+",";
        }

        if(ids.length()>0){
            ids=ids.substring(0,ids.length()-1);
            Pager<User> pageUsers = userService.findPageByIds(ids,name,loginName,orgId,pageNo);
            model.addAttribute("page",pageUsers);
        }
        return CommonKey.WEB_PATH + "/system/user/online";
    }
}
