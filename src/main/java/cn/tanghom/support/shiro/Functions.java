package cn.tanghom.support.shiro;

import javax.annotation.Resource;

import org.activiti.conf.CommonKey;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.util.CollectionUtils;

import cn.tanghom.app.baiding.model.ArticleCategory;
import cn.tanghom.app.baiding.service.ArticleCategoryService;
import cn.tanghom.app.system.aop.ConfigApplication;

/**
 * 自定义标签
 * @author zml
 *
 */
public class Functions {
	
    private static ArticleCategoryService articleCategoryService;	

    public static boolean in(Iterable iterable, Object element) {
        if(iterable == null) {
            return false;
        }
        return CollectionUtils.contains(iterable.iterator(), element);
    }

    public static String principal(Session session) {
        PrincipalCollection principalCollection =
                (PrincipalCollection) session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);

        return (String)principalCollection.getPrimaryPrincipal();
    }
    public static boolean isForceLogout(Session session) {
        return session.getAttribute(CommonKey.SESSION_FORCE_LOGOUT_KEY) != null;
    }
    
    public static ArticleCategory category(Long categoryId) {
    	if(articleCategoryService==null){
    		articleCategoryService = (ArticleCategoryService)ConfigApplication.getBean(ArticleCategoryService.class);
    	}
    	if(articleCategoryService!=null){
    		ArticleCategory c = articleCategoryService.findById(categoryId);
    		if(c!=null){
    			return c;
    		}
    	}
        return null;
    }
}

