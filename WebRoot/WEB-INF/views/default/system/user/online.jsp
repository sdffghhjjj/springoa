<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>在线用户</title>
	<jsp:include page="../../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>在线用户</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<div class="row table-search">
					<div class="col-xs-2">
					</div>
					<div class="col-xs-10 text-right">
						<form class="form-inline" role="form" method="post">
							<div class="form-group">
								<label class="sr-only">账号</label>
								<input type="text" class="form-control input-sm" value="${param.loginName}" name="loginName" placeholder="账号">
							</div>
							<div class="form-group">
								<label class="sr-only">姓名</label>
								<input type="text" class="form-control input-sm" value="${param.userName}" name="userName" placeholder="姓名">
							</div>
							<div class="form-group">
								<label class="sr-only">*组织机构：</label>
									<select class="form-control input-sm" name="orgId">
										<option value="" <c:if test="${param.orgId==null}">selected</c:if> >请选择</option>
										<c:forEach var="r" items="${organizations}" varStatus="s">
											<option value="${r.id}"
													<c:if test="${param.orgId==r.id}">selected</c:if> >${r.name}</option>
										</c:forEach>
									</select>
							</div>
							<button type="submit" class="btn btn-primary btn-sm">搜索</button>
						</form>
					</div>
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>名称</th>
							<th>账号</th>
							<th>组织机构</th>
							<th>角色</th>
							<th>性别</th>
							<th>电话</th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="user" items="${page.items}" varStatus="s">
						<tr>
							<td>${user.id}</td>
							<td>${user.userName}</td>
							<td>${user.loginName}</td>
							<td>${user.organizationName}</td>
							<td>
								<c:forEach var="role" items="${user.roles}" varStatus="s">
									${role.name};
								</c:forEach>
							</td>
							<td>${user.sex}</td>
							<td>${user.phone}</td>
						</tr>
					</c:forEach>
					<c:if test="${empty page}">
						<td colspan="7" class="td-center">无相关数据</td>
					</c:if>
					</tbody>
				</table>
				<div class="clearfix">
					<div class="pull-right">
						<mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
					</div>
				</div>
			</div>
		</section>
	</div>
</section>
</body>
</html>