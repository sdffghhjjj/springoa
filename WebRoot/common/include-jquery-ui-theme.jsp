<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
//jquery.ui主题
String defaultTheme = "redmond";
String themeVersion = "1.9.2";

session.setAttribute("themeName", defaultTheme);
session.setAttribute("themeVersion", themeVersion);
%>

<link href="${ctx}/vendor/jquery-ui/themes/${themeName }/jquery-ui-${themeVersion}.custom.css" type="text/css" rel="stylesheet" />
<script src="${ctx }/vendor/jquery-ui/jquery-ui-${themeVersion}.min.js" type="text/javascript"></script>
<script src="${ctx }/vendor/jquery-ui/extends/themeswitcher/jquery.ui.switcher.js" type="text/javascript"></script>


    <link href="${ctx }/vendor/jquery-ui/extends/timepicker/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <link href="${ctx }/vendor/plugins/qtip/jquery.qtip.min.css" type="text/css" rel="stylesheet" />

  
    <script src="${ctx }/vendor/jquery-ui/extends/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
	<script src="${ctx }/vendor/jquery-ui/extends/i18n/jquery-ui-date_time-picker-zh-CN.js" type="text/javascript"></script>
	<script src="${ctx }/vendor/plugins/qtip/jquery.qtip.pack.js" type="text/javascript"></script>
	<script src="${ctx }/vendor/plugins/html/jquery.outerhtml.js" type="text/javascript"></script>
	