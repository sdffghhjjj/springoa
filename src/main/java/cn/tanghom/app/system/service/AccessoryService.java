package cn.tanghom.app.system.service;

import cn.tanghom.app.system.model.Accessory;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


public interface AccessoryService {
	
	/**
	 * 根据Id查询附件信息
	 * @param id 附件Id
	 * @return Accessory
	 */
	 Accessory getById(Long id);
	/**
	 * 删除文件
	 * @param id
	 * @return
	 */
	 void delete(Long id,HttpServletRequest request);
	/**
	 * 单个文件上传
	 * @param accessory 文件信息
	 */
	Map<String, Object> upload(Accessory accessory, MultipartFile file, HttpServletRequest request);
	List<Accessory> findByIdStr(String idStr);
}
