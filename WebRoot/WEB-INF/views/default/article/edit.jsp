<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>文章管理</title>
	<jsp:include page="../header.jsp"></jsp:include>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/vendor/kindeditor/kindeditor-min.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/vendor/kindeditor/plugins/file_manager/file_manager.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/vendor/webuploader/webuploader.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/vendor/webuploader/webuploader.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/vendor/webuploader/myuploader.js"></script>

</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>${article.id==null?'新增':'修改'}文章</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<form class="form-inlinle" action="${pageContext.request.contextPath}/article/save.shtml" method="post" id="form">
				<div class="form-group">
					<label>分类:</label>
					<select class="form-control input-sm" name="categoryId" style="width: 200px;">
						<option value="">请选择</option>
						<c:forEach var="ac" items="${articleCategoryList}" varStatus="s">
							<option value="${ac.id}" ${article.categoryId==ac.id?'selected':''}>${ac.name}</option>
						</c:forEach>
					</select>
				</div>
	   		 	<div class="form-group">
				    	<label>*标题:</label>
			            <input type="text" name="title" value="${article.title}" placeholder="标题" class="form-control input-sm" style="width: 400px;">
				  </div>
					<div class="form-group">
						<label>图片:</label>
						<div id="uploader-demo">
							<!--用来存放item-->
							<div id="fileList" class="uploader-list">
							<c:forEach var="access" items="${article.accessorys}">
								<div id="WU_FILE_${access.id}" class="file-item thumbnail upload-state-done">
									<img src="${pageContext.request.contextPath}/${access.path}">
									<div class="info">${access.title}</div>
									<a href="javascript:;" class="file-cancel" onclick="delFile('WU_FILE_${access.id}')">×</a>
									<input type="hidden" name="fileId[]" value="${access.id}">
								</div>
							</c:forEach>
							</div>
							<div id="filePicker">选择图片</div>
						</div>
					</div>
				   <div class="form-group">
				    	<label>*内容:</label>
  	   		 		 	<textarea name="content" id="editor" style="height:500px;width:100%;">${article.content}</textarea>
				  </div>
				 <div class="form-group">
					 <label class="control-label">*状态：</label>
					 <select class="form-control input-sm" name="status" data-placeholder="状态" style="width: 100px;">
						 <option value="1"<c:if test="${article.status==1||article.status==null}">selected</c:if> >正常</option>
						 <option value="2" <c:if test="${article.status==0}">selected</c:if> >禁用</option>
					 </select>
				 </div>
					<div class="form-group">
					      <input type="hidden" name="id" value="${article.id}"/>
					      <button type="button" class="btn btn-primary" onclick="sub();">提交保存</button>
				  </div>
          </form>
			</div>
		</section>
	</div>
</section>
  <script type="text/javascript" >
		KE.show({
			id : 'editor',
			imageUploadJson : '/uploadJson/file_upload',
			fileManagerJson : '/uploadJson/file_manager',
			allowFileManager : true,
			afterCreate : function(id) {
				KE.event.ctrl(document, 13, function() {
					KE.util.setData(id);
					document.forms['form'].submit();
				});
				KE.event.ctrl(KE.g[id].iframeDoc, 13, function() {
					KE.util.setData(id);
					document.forms['form'].submit();
				});
			}
		});
	
	
	function sub(){
		var checkTitle = checkEmpty($('[name="title"]'),'请填标题');
		var checkCategoryId = checkEmpty($('[name="categoryId"]'),'请选择所属分类');
		var checkContent =$('[name="content"]').val();
			if(""==checkContent){
				msg.info("请填写内容")
				return;
			}
		if(checkTitle && checkCategoryId){
			msg.confirm("确认保存？",function(){
			$.ajax({
				type : "POST",
				data : $("#form").serialize(),
				url : $("#form").attr("action"),
				dataType : 'json',
				success:function(data) {
					if (data.status == 1) {
						msg.info(data.info);
						setTimeout(function(){
							window.location.href="${pageContext.request.contextPath}/article/list.shtml";
						},1000);
					} else {
						msg.info(data.info);
					}
				}
			});
			},"是否保存？");
		}
	}
	</script>
</body>
</html>
