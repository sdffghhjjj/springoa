package cn.tanghom.app.baiding.model;

import java.io.Serializable;

/**
 * 文章分类
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/25
 */
public class ArticleCategory implements Serializable {
    private static final long serialVersionUID = -7884446182751936164L;
    private Long id;//ID
    private String name;//名称
    private String description;//描述
    private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常
    private Integer sort = 0;//排序
    private Long parentId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
