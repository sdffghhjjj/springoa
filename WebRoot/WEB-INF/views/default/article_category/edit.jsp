<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>知识分类管理</title>
	<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>${articleCategory.id==null?'新增':'修改'}知识分类</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" action="${pageContext.request.contextPath}/article_category/save" method="post" id="form">
					<div class="form-group">
						<label class="col-sm-4 control-label">*上级:</label>
						<div class="col-sm-4">
							<input type="text" name="parentId" value="${articleCategory!=null? articleCategory.parentId: param.parentId}" placeholder="上级分类" readonly class="form-control input-sm">
						</div>
					</div>
	   		 		<div class="form-group">
				    <label class="col-sm-4 control-label">*名称:</label>
				    <div class="col-sm-4">
			            <input type="text" name="name" value="${articleCategory.name}" placeholder="名称" class="form-control input-sm">
				    </div>
				  </div>
				   <div class="form-group">
				    <label class="col-sm-4 control-label">描述:</label>
				    <div class="col-sm-4">
  	   		 		 	<textarea name="description" placeholder="描述" class="form-control">${articleCategory.description}</textarea>
				    </div>
				  </div>
                 <div class="form-group">
                     <label class="col-sm-4 control-label">*排序:</label>
                     <div class="col-sm-4">
                         <input type="number" name="sort" value="${articleCategory.sort==null?'0':articleCategory.sort}" placeholder="排序" class="form-control input-sm">
                     </div>
                 </div>
				 <div class="form-group">
					 <label class="col-sm-4 control-label">*状态：</label>
					 <div class="col-sm-4">
						 <select class="form-control input-sm" name="status" data-placeholder="状态">
							 <option value="1"<c:if test="${articleCategory.status==1||articleCategory.status==null}">selected</c:if> >启用</option>
							 <option value="2" <c:if test="${articleCategory.status==0}">selected</c:if> >禁用</option>
						 </select>
					 </div>
				 </div>
					<div class="form-group">
					    <div class="col-sm-offset-4 col-sm-4">
					      <input type="hidden" name="id" value="${articleCategory.id}" />
					      <button type="button" class="btn btn-primary btn-sm btn-block" onclick="sub();">保存</button>
					    </div>
				  </div>
          </form>
	</div>
		<!--子分类列表： -->		
			<div class="panel-heading">
				<b>子分类列表</b>
			</div>
				<table class="table">
					<thead>
					<tr>
						<th>Id</th>						
						<th>名称</th>						
						<th>描述</th>
						<th>状态</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="ac" items="${articleCategoryList}">
						<tr>
							<td>${ac.id}</td>							
							<td>${ac.name}</td>							
							<td>${ac.description}</td>
							<td>${ac.status==1?'启用':'禁用'}</td>
							<td>
								<a href="${pageContext.servletContext.contextPath}/article_category/edit.shtml?id=${ac.id}" class="label label-success">编辑</a>
								<a href="javascript:;"  url="${pageContext.servletContext.contextPath}/article_category/delete.shtml?id=${ac.id}" data-info="确认删除吗?" class="ajax-get label label-danger">删除</a>
								<a href="${pageContext.servletContext.contextPath}/article_category/add?parentId=${ac.id}" class="label label-success">新增子类</a>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${empty articleCategoryList}">
						<td colspan="4" class="td-center">无相关数据</td>
					</c:if>
					</tbody>
				</table>
</section>
		</div>
	</section>
	<script type="text/javascript">
	function sub(){
		var checkName = checkEmpty($('[name="name"]'),'请填名称');
		if(checkName){
			msg.confirm("确认保存？",function(){
			$.ajax({
				type : "POST",
				data : $("#form").serialize(),
				url : $("#form").attr("action"),
				dataType : 'json',
				success:function(data) {
					if (data.status == 1) {
						msg.info(data.info);
						setTimeout(function(){
							window.location.href="${pageContext.request.contextPath}/article_category/list.shtml";
						},1000);
					} else {
						msg.info(data.info);
					}
				}
			});
			},"是否保存？");
		}
	}

	</script>
</body>
</html>
