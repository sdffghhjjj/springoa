package cn.tanghom.app.system.service;

import cn.tanghom.app.system.model.Role;
import cn.tanghom.app.system.model.User;
import cn.tanghom.support.page.Pager;

import java.util.List;

/**
 * 用户管理逻辑处理层
 *
 * @author tanghom<tanghom@qq.com> 2016/5/10
 */
public interface UserService {

    /**
     * 保存用户
     *
     * @param user
     * @param roleId
     */
    String saveOrUpdate(User user, Long roleId[]);

    /**
     * 用户注册
     *
     * @param user
     */
    void reg(User user);

    /**
     * 保存用户
     *
     * @param user
     */
    void save(User user);

    /**
     * 更新保存
     *
     * @param user
     */
    void update(User user);

    /**
     * 根据iD删除用户
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询用户
     *
     * @param id
     * @return
     */
    User findById(Long id);

    /**
     * 根据登录名查找用户
     *
     * @param loginName 登录名
     * @return
     */
    User findByLoginName(String loginName);

    /**
     * 查询所有用户
     *
     * @return
     */
    List<User> findList();

    /**
     * 分页查询
     *
     * @param userName  用户名称
     * @param loginName 登录帐号
     * @param orgId     部门Id
     * @param index     当前页
     * @return
     */
    Pager<User> findPageByParams(String userName, String loginName, Long orgId, Integer status, Long index);

    /**
     * @param ids       用户ID
     * @param userName  用户名称
     * @param loginName 登录帐号
     * @param orgId     部门Id
     * @param index     当前页
     * @return
     */
    Pager<User> findPageByIds(String ids, String userName, String loginName, Long orgId, Long index);

    List<User> getUserByGroupId(Long groupId, Pager<User> page);
    
    boolean hasRole(String userId, String rolekey);
    
    
    boolean addRole(String userId, String rolekey);
    
}
