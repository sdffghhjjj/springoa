package cn.tanghom.support.page;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;

/**
 * page from 1 begin diff PageRequest which is o begin
 * @author Hunteron-cp
 *
 */
public class Pagination implements Pageable {   
    
	protected String pageStr = "";
	protected Integer totalSum = 0;  	//共多少记录
	protected Integer currentPage = 1;	
	
	protected int pageNumDefault = 10;
	protected Integer pageSize; //size,limit	
	
	protected String sortStr;
	protected String orderColumn;
	
	private Sort sort;
	
	public Pagination() {
		this(1, 100);
	}
	
	public Pagination(int page, int size) {
		this(page, size, null);
	}
	
	public Pagination(int page, int size, Sort sort) {
		currentPage = page; pageSize = size; this.sort = sort;		
	}
	
	public String getSortStr() {
		return sortStr;
	}
	public void setSortStr(String orderStr) {
		this.sortStr = orderStr;
		
	}
	public String getOrderBy() {
		return orderColumn;
	}
	//must first set sortStr
	public void setOrderBy(String orderColumn) {
		this.orderColumn = orderColumn;	
	}
	
	public Integer getMaxResult() {
		return this.getPageSize() > this.getTotalSum() ? this.getTotalSum() : this.getPageSize();
	}
	
	public Integer getPageNumDefault() {
		return pageNumDefault;
	}
	public void setPageNumDefault(int pageNumDefault) {
		this.pageNumDefault = pageNumDefault;
	}
	
	
	
	@Override
	public int getPageSize() {		
		pageSize = pageSize==null || pageSize < 1 || pageSize > 1000 ? pageNumDefault : pageSize;
		return pageSize;
	}
	
	public void setPageSize(int pageNum) {
		this.pageSize = pageNum;
	}
	public String getPageStr() {
		return pageStr;
	}
	public void setPageStr(String pageStr) {
		this.pageStr = pageStr;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalPage() {
		int totalPage = (this.getTotalSum() / this.getPageSize()) + (this.getTotalSum() % this.getPageSize() != 0 ? 1 : 0);
		return totalPage;
	}
	
	public Integer getPrePage() {
		return this.getCurrentPage() - 1 > 1 ? this.getCurrentPage() - 1 : 1;
	}
	
	public Integer getNextPage() {
		return this.getCurrentPage() + 1 < this.getTotalPage() ? this.getCurrentPage() + 1 : this.getTotalPage();
	}
	
	public Integer getTotalSum() {
		return totalSum;
	}
	public void setTotalSum(Integer totalSum) {
		this.totalSum = totalSum;
	}
	//处理分页
	public void processTotalPage()
	{			
		StringBuffer str = new StringBuffer();
		str.append("<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" class=\"box6\">");
		str.append("<tr><td width=\"100%\" height=\"24\"  class=\"bt\">");
		str.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" background=\"\">");
		str.append("<input type=\"hidden\" name=\"current\" value=\"\">");
		str.append("<tr>");
		
		str.append("<td align=\"left\" valign=\"center\" class=\"bt\" width=\"30%\">共" + this.getTotalPage() + "页&nbsp;");
		str.append("第" + this.getCurrentPage() + "页&nbsp;");
		str.append("每页<input id=\"pageNum\" name=\"limit\" type=\"text\" class=\"bt\" value=\""  + this.getPageSize() + "\" onChange=\"doSearch(1);\"  maxlength=\"3\" size=\"2\">条记录</td>");
		str.append("<td align=\"right\" valign=\"center\" class=\"bt\" width=\"70%\">");
		
		if(this.getTotalPage() > 1) {
			str.append("<a href=\"javascript:doSearch(1);\" class=\"bt\" onClick=\"\">首页</a>&nbsp;&nbsp;");
			str.append("<a href=\"javascript:doSearch(" + this.getPrePage() + ");\" class=\"bt\" onClick=\"\" style=\"cursor:hand\">上一页</a>&nbsp;&nbsp;");
			str.append("<a href=\"javascript:doSearch(" + this.getNextPage() + ");\" class=\"bt\" onClick=\"\" style=\"cursor:hand\">下一页</a>&nbsp;");
			str.append("<a href=\"javascript:doSearch(" + this.getTotalPage() + ");\" class=\"bt\" onClick=\"\" style=\"cursor:hand\"> 末页 </a>&nbsp;");
		} else {
			str.append("首页&nbsp;&nbsp;");
			str.append("上一页&nbsp;&nbsp;");
			str.append("下一页&nbsp;&nbsp;");
			str.append("末页&nbsp;&nbsp;");
		}
		str.append("共" + this.getTotalSum() + "条记录<input type=\"hidden\" id=\"currentPage\" name=\"page\" value=\"" + this.getCurrentPage() + "\">");
		str.append("</td>");
		str.append("</tr>");
		str.append("</table>");
		str.append("</td></tr>");
		str.append("</table>");
		
        this.setPageStr(str.toString());
	}
	
	/* 
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;

		result = prime * result + currentPage;
		result = prime * result + pageSize;

		return result;
	}


	@Override
	public int getPageNumber() {	
		return this.currentPage;
	}

	@Override
	public Sort getSort() {	
		if(sort==null){
			Sort.Direction dir = null;
			if(sortStr!=null){		
				dir = Sort.Direction.fromStringOrNull(sortStr);
			}
			if(dir!=null){
				sort = JpaSort.unsafe(dir,orderColumn.split(","));
			}
			else{
				sort = JpaSort.unsafe(orderColumn.split(","));
			}
		}
		return sort;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#next()
	 */
	public Pagination next() {
		return new Pagination(getPageNumber() + 1, getPageSize(), getSort());
	}

	/* 
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.AbstractPageRequest#previous()
	 */
	public Pagination previous() {
		return getPageNumber() <= 1 ? this : new Pagination(getPageNumber() - 1, getPageSize(), getSort());
	}


	/* 
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#getOffset()
	 */
	public int getOffset() {
		if(this.getCurrentPage() <= 1) return 0;
		return (currentPage-1) * pageSize;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#hasPrevious()
	 */
	public boolean hasPrevious() {
		return currentPage > 1;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#previousOrFirst()
	 */
	public Pageable previousOrFirst() {
		return hasPrevious() ? previous() : first();
	}

	@Override
	public Pagination first() {
		// TODO Auto-generated method stub
		return new Pagination(1, getPageSize(), getSort());
	}
	
}
