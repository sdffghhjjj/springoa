package cn.tanghom.app.workflow.service;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.IdentityService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tanghom.app.system.model.User;
import cn.tanghom.support.db.ActiveRecordDao;




@Service
public class ActivitiIdentifyServiceImpl implements IActivitiBaseService {

	private static final Logger logger = Logger.getLogger(ActivitiIdentifyServiceImpl.class);

	@Autowired
	protected ActiveRecordDao jdbcDao;
	
	@Autowired
	protected IdentityService identityService;
	
	@Override
	public void deleteAllUser() {
		String sql = "delete from ACT_ID_USER";
		this.jdbcDao.delete(sql, null);
		logger.debug("deleted from activiti user.");
	}

	@Override
	public void deleteAllRole() {
		String sql = "delete from ACT_ID_GROUP";
		this.jdbcDao.delete(sql, null);
		logger.debug("deleted from activiti group.");
	}

	@Override
	public void deleteAllMemerShip() {
		String sql = "delete from ACT_ID_MEMBERSHIP";
		this.jdbcDao.delete(sql, null);
		logger.debug("deleted from activiti membership.");
	}

	@Override
	public void updateMembership(String userId, String groupId) throws Exception {
		String sql = "update ACT_ID_MEMBERSHIP set GROUP_ID_=:groupId where USER_ID_=:userId ";
		Map<String, Object> paramMap = new HashMap<String, Object>();  
	    paramMap.put("groupId", groupId);  
	    paramMap.put("userId", userId);  
		this.jdbcDao.saveOrUpdate(sql, paramMap);
		logger.debug("update ACT_ID_MEMBERSHIP.");
		
	}
	
	 /**
     * 添加一个用户到Activiti {@link org.activiti.engine.identity.User}
     * @param user  用户对象, {@link User}
     */
    public void saveActivitiUser(User user) {
        String userId = user.getId().toString();
        org.activiti.engine.identity.User activitiUser = identityService.newUser(userId);
        cloneAndSaveActivitiUser(user, activitiUser);
        logger.info("add activiti user: {}"+activitiUser);
    }
    
    

    /**
     * 使用系统用户对象属性设置到Activiti User对象中
     * @param user          系统用户对象
     * @param activitiUser  Activiti User
     */
    private void cloneAndSaveActivitiUser(User user, org.activiti.engine.identity.User activitiUser) {
        activitiUser.setFirstName(user.getUserName());
        activitiUser.setLastName(user.getLoginName());
        activitiUser.setPassword(user.getLoginPwd());
        activitiUser.setEmail(user.getEmail());
        identityService.saveUser(activitiUser);
    }
    

}
