package cn.tanghom.app.system.service.impl;

import cn.tanghom.app.system.mapper.AccessoryMapper;
import cn.tanghom.app.system.model.Accessory;
import cn.tanghom.app.system.model.User;
import cn.tanghom.app.system.service.AccessoryService;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.DateUtil;
import cn.tanghom.support.utils.JsonUtil;
import cn.tanghom.support.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class AccessoryServiceImpl implements AccessoryService {

    @Resource
    private AccessoryMapper accessoryMapper;

    @Override
    public  Map<String, Object> upload(Accessory accessory, MultipartFile file, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            String allowSuffix = "jpg,png,gif,jpeg";
            String fileDir = "upload/image/" + DateUtil.fromDateP("yyyyMMdd");
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1).toLowerCase();
            String realPath = request.getServletContext().getRealPath("/");
            File savePath = new File(realPath + fileDir);
            if (!savePath.exists()) {
                savePath.mkdirs();
            }
            String newFielName = StringUtils.getRandomString(10) + "." + suffix;
            File f = new File(savePath.getAbsoluteFile() + "/" + newFielName);
            file.transferTo(f);
            f.createNewFile();
            String resultPath = fileDir + "/" + newFielName;
            // 保存文件信息
            accessory.setTitle(file.getOriginalFilename());
            accessory.setPath(resultPath);
            accessory.setSize(file.getSize());
            if (allowSuffix.indexOf(suffix) == -1) {
                accessory.setType(Accessory.TYPE_FILE);
            } else {
                accessory.setType(Accessory.TYPE_IMG);
            }
            accessory.setUploadTime(new Date());
            User loger = CommonUtils.getUserSession();
            accessory.setUser(loger);
            accessoryMapper.save(accessory);
            map.put("id", accessory.getId());
            map.put("path", resultPath);
            map.put("status", "1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    @Override
    public List<Accessory> findByIdStr(String idStr) {
        return accessoryMapper.findByIdStr(idStr);
    }

    @Override
    public Accessory getById(Long id) {
        return accessoryMapper.getById(id);
    }

    @Override
    public void delete(Long id,HttpServletRequest request) {
        Accessory accessory = this.getById(id);
        accessoryMapper.delete(accessory.getId());
        String realPath = request.getServletContext().getRealPath("/");
        File f = new File(realPath+"/"+accessory.getPath()); // 输入要删除的文件位置
        if(f.exists()){
            f.delete();
        }
    }
}
