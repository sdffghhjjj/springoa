<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	
	<jsp:include page="header.jsp"></jsp:include>
	
	<title>Baiding管理系统</title>
	<meta name="description" content="Baiding管理系统">
	
	<link rel="stylesheet" href="${ctx}/css/animate.min.css">	
	<link rel="stylesheet" href="${ctx}/css/main.css">

	<script src="${ctx}/vendor/jquery.backstretch.min.js"></script>
	<script>
		/**
		 * 背景图片
		 */
		var imgs=["images/login-bg/1.jpg","images/login-bg/2.jpg","images/login-bg/3.jpg","images/login-bg/4.jpg","images/login-bg/5.jpg","images/login-bg/6.jpg"];
		$(function() {
			$.backstretch(imgs, {
				fade: 750,
				duration: 3000
			});
		});
	</script>
	<style>
		.bg-color {	background-color: #535a6c;}
	</style>
	<![endif]-->
	<script type="text/javascript">
		if (top.location != self.location)top.location=self.location;
	</script>
</head>
<body class="bg-color center-wrapper">
<div class="center-content">
	<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
		<section class="panel panel-info">
			<header class="panel-heading"><b>登录白丁管理系统</b></header>
			<div class="bg-white user pd-md">
				<h6 class="shake animated">
					<b>你好.</b>
					<span style="color: #ff6417"><c:if test="${not empty param.kickout}">其他地方登录了此账号，你被踢出下线，</c:if>${error==null?'请输入帐号和密码登录!':error}</span>
				</h6>
				<form role="form" action="login" method="post">
					<div class="input-group mg-b-sm">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" class="form-control mg-b-sm" name="loginName" placeholder="请输入帐号" autofocus>
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" class="form-control" name="loginPwd" placeholder="请输入密码">
					</div>
					<div class="checkbox pull-left">
						<%--<label>
                            <input type="checkbox" value="remember-me"> 记住密码
                        </label>--%>
					</div>
					<div class="text-right mg-b-sm mg-t-sm">
						<%--<a href="javascript:;">忘记密码?</a>--%>
					</div>
					<button class="btn btn-success btn-block" type="submit"><b>登&nbsp;&nbsp;&nbsp;录</b></button>
				</form>
				<p class="center-block mg-t text-center animated bounceIn">
					<a href="http://jingyan.baidu.com/article/d169e186a3dd27436611d829.html" target="_blank" style="color: #ff6417">请使用360浏览器急速模式或谷歌CHROME浏览器访问！</a>
				</p>
			</div>
		</section>
	</div>
</div>
</body>
</html>
