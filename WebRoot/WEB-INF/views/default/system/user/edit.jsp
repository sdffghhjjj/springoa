<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>用户管理</title>
    <jsp:include page="../../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
    <div class="content-wrap">
        <section class="panel panel-default">
            <div class="panel-heading">
                <b>${user.id==null?'新增':'修改'}用户</b>
                <small class="pull-right">
                    <a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
                    <a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
                </small>
            </div>
            <div class="panel-body">
            <form class="form-horizontal" action="${pageContext.request.contextPath}/user/save.shtml" method="post" id="form">
                <div class="form-group">
                    <label class="col-sm-4 control-label">*姓名：</label>
                    <div class="col-sm-4">
                        <input type="text" name="userName" value="${user.userName}"  placeholder="名称" class="form-control input-sm">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">*账号：</label>
                    <div class="col-sm-4">
                        <input type="text" name="loginName" value="${user.loginName}" placeholder="登录名" class="form-control input-sm">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">*密码：</label>
                    <div class="col-sm-4">
                        <input type="text" name="loginPwd" value="" placeholder="密码" class="form-control input-sm">
                        <p class="help-block">${user.id==null?'密码不填写则默认123456':'密码不填写不修改'}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">电话：</label>
                    <div class="col-sm-4">
                        <input type="text" name="phone" value="${user.phone}" placeholder="电话" class="form-control input-sm">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">*性别：</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" name="sex" data-placeholder="性别">
                            <option value="" <c:if test="${user.sex==null}">selected</c:if> >请选择</option>
                            <option value="男"<c:if test="${user.sex=='男'}">selected</c:if> >男</option>
                            <option value="女" <c:if test="${user.sex=='女'}">selected</c:if> >女</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">*状态：</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" name="status" data-placeholder="状态">
                            <option value="1" <c:if test="${user.status==null}">selected</c:if> >请选择</option>
                            <option value="1"<c:if test="${user.status==1}">selected</c:if> >启用</option>
                            <option value="2" <c:if test="${user.status==0}">selected</c:if> >禁用</option>
                            <option value="2" <c:if test="${user.status==2}">selected</c:if> >待审核</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-4 control-label">*组织机构：</label>
                <div class="col-sm-4">
                    <select class="form-control input-sm" name="organizationId">
                        <option value="" <c:if test="${user.organizationId==null}">selected</c:if> >请选择</option>
                        <c:forEach var="r" items="${organizations}" varStatus="s">
                            <option value="${r.id}"
                                    <c:if test="${user.organizationId==r.id}">selected</c:if> >${r.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">*选择角色：</label>
                    <div class="col-sm-4">
                        <c:forEach var="r" items="${roles}" varStatus="s">
                            <input type="checkbox" name="roleId" value="${r.id}">${r.name}
                        </c:forEach>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4">
                        <input type="hidden" name="id" value="${user.id}"/>
                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="sub();">保存</button>
                    </div>
                </div>
            </form>
         </div>
        </section>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        <c:forEach items="${user.roles}" var="k">
        $("input[name='roleId']:checkbox[value='${k.id}']").prop('checked',
                'true');
        </c:forEach>
    });
    function sub() {
        msg.confirm("确认保存？",function(){
        $.ajax({
            type: "POST",
            data: $("#form").serialize(),
            url: $("#form").attr("action"),
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) {
                    msg.info(data.info);
                    setTimeout(function(){
                        window.location.href="${pageContext.request.contextPath}/user/list.shtml";
                    },1000);
                } else {
                    msg.info(data.info);
                }
            }
        });
        },"是否保存？");
    }
</script>
</body>
</html>
