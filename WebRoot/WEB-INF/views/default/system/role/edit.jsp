<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>角色管理</title>
	<jsp:include page="../../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>${role.id==null?'新增':'修改'}角色</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" action="${pageContext.request.contextPath}/role/save.shtml" method="post" id="form">
	   		 	<div class="form-group">
				    <label class="col-sm-4 control-label">*名称:</label>
				    <div class="col-sm-4">
			            <input type="text" name="name" value="${role.name}" placeholder="名称" class="form-control input-sm">
				    </div>
				  </div>
                 <div class="form-group">
                        <label class="col-sm-4 control-label">*key:</label>
                        <div class="col-sm-4">
                            <input type="text" name="roleKey" value="${role.roleKey}" placeholder="key" class="form-control input-sm">
                        </div>
				  </div>
				   <div class="form-group">
				    <label class="col-sm-4 control-label">描述:</label>
				    <div class="col-sm-4">
  	   		 		 	<textarea name="description" placeholder="描述" class="form-control">${role.description}</textarea>
				    </div>
				  </div>
                 <div class="form-group">
                     <label class="col-sm-4 control-label">*排序:</label>
                     <div class="col-sm-4">
                         <input type="number" name="sort" value="${role.sort==null?'0':role.sort}" placeholder="排序" class="form-control input-sm">
                     </div>
                 </div>
				 <div class="form-group">
					 <label class="col-sm-4 control-label">*状态：</label>
					 <div class="col-sm-4">
						 <select class="form-control input-sm" name="status" data-placeholder="状态">
							 <option value="1"<c:if test="${role.status==1||role.status==null}">selected</c:if> >启用</option>
							 <option value="2" <c:if test="${role.status==0}">selected</c:if> >禁用</option>
						 </select>
					 </div>
				 </div>
					<div class="form-group">
					    <div class="col-sm-offset-4 col-sm-4">
					      <input type="hidden" name="id" value="${role.id}" />
					      <button type="button" class="btn btn-primary btn-sm btn-block" onclick="sub();">保存</button>
					    </div>
				  </div>
          </form>
	</div>
</section>
		</div>
	</section>
	<script type="text/javascript">
	function sub(){
		var checkName = checkEmpty($('[name="name"]'),'请填名称');
		var checkKey = checkEmpty($('[name="key"]'),'请填写key');
		if(checkName && checkKey){
			msg.confirm("确认保存？",function(){
			$.ajax({
				type : "POST",
				data : $("#form").serialize(),
				url : $("#form").attr("action"),
				dataType : 'json',
				success:function(data) {
					if (data.status == 1) {
						msg.info(data.info);
						setTimeout(function(){
							history.go(-1);
						},1000);
					} else {
						msg.info(data.info);
					}
				}
			});
			},"是否保存？");
		}
	}
	//验证信息
	//验证空信息
	function checkEmpty(obj,msg){
		var check = false;
		var val = obj.val();
		if(val == ""){
			obj.err(msg);
		}else{
			check = true;
			obj.closeErr();
		}
		return check;
	}
	</script>
</body>
</html>
