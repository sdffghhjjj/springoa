package cn.tanghom.app.oa.controller;

import java.util.List;

import javax.annotation.Resource;

import org.activiti.conf.CommonKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mongodb.BasicDBObject;
import com.unbiz.common.StringUtil;

import cn.tanghom.app.baiding.model.Article;
import cn.tanghom.app.baiding.model.ArticleCategory;
import cn.tanghom.app.baiding.service.ArticleCategoryService;
import cn.tanghom.app.oa.model.Resume;
import cn.tanghom.app.oa.repository.EduExpRepository;
import cn.tanghom.app.oa.repository.ResumeRepository;
import cn.tanghom.app.oa.repository.WorkExpRepository;
import cn.tanghom.app.system.model.Accessory;
import cn.tanghom.support.id.IDWorker;
import cn.tanghom.support.page.Pager;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.StringUtils;

@Controller
@RequestMapping("/oa/talent")
public class TalentController {
	
	final Long talentCategoryId = 8L;
	String collection = "hd_talent_resume_receive";
	
	IDWorker resumeIdSeq = new IDWorker(1,talentCategoryId);;
	
	@Autowired
	private MongoOperations destination;
	
	@Resource
    private ArticleCategoryService articleCategoryService;
	
	@Autowired
	ResumeRepository talentRepository;
	
	@Autowired
	WorkExpRepository workExpRepository;
	
	
	@Autowired
	EduExpRepository eduExpRepository;

    /**
     * 列表
     *
     * @param categoryId
     * @param status
     * @param model
     * @return
     */
    @RequestMapping(value = "/list", method = {RequestMethod.POST, RequestMethod.GET})
    public String list(String title, Long categoryId, Integer status, Long pageNo, Model model) {
        status = status == null || status < 0 ? null : status;
        categoryId = categoryId == null || categoryId < 0 ? null : categoryId;
        int pageIndex = pageNo!=null? pageNo.intValue():1;
        PageRequest pager = new PageRequest(pageIndex,20);
        Page<Resume> page;
        
    	if(title!=null && title.length()>0){
    		page = talentRepository.queryFirst10ByName(title, pager);
    	}
    	else if(status!=null){
        	page = talentRepository.findByStatus(status, pager);
        }
    	else{       
    		page = talentRepository.findAll(pager);
    	}
        model.addAttribute("page", page);
        model.addAttribute("pageIndex",pageIndex);
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParentId(talentCategoryId, 1);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/oa/talent/list";
    }
    

    /**
     * 显示文章页面
     *
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String view(Long id, Model model) {
    	Resume article = talentRepository.findById(id);
    	if(article==null){
    		throw new RuntimeException("找不到简历，可能是简历还没有解析完成。");
    	}
       
        model.addAttribute("article", article);
        model.addAttribute("talent", article);
        
        List<Resume.WorkExp> works = workExpRepository.findByResumeId(article.getId());
        model.addAttribute("works", works);
        
        
        List<Resume.EduExp> edus = eduExpRepository.findByResumeId(article.getId());
        model.addAttribute("edus", edus);
        
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParentId(talentCategoryId, 1);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/oa/talent/view";
    }

    /**
     * 显示文章页面
     *
     * @return
     */
    @RequestMapping(value = "/html", method = RequestMethod.GET)
    @ResponseBody
    public String html(Long id, Model model) {
    	Resume article = talentRepository.findById(id);
    	String html = article.htm_content;
    	if(html==null || html.isEmpty()){
    		html ="<pre>\n"+ article.txt_content +"\n</pre>";
    	}        
        return html;
    }
    
    /**
     * 解析简历
     *
     * @param categoryId
     * @param status
     * @param model
     * @return
     */
    @RequestMapping(value = "/parse", method = {RequestMethod.GET})
    public String parseReq(Long id,Model model) {
        model.addAttribute("resumeId", id);   
        Resume article = talentRepository.findById(id);
        model.addAttribute("article", article);
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParentId(talentCategoryId, 1);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/oa/talent/input";
    }
    
    /**
     * 解析简历
     *
     * @param categoryId
     * @param status
     * @param model
     * @return
     */
    @RequestMapping(value = "/parse", method = {RequestMethod.POST})
    @ResponseBody
    public String parseReq(Long resume_id,Long categoryId, Resume model) {
    	resume_id = resume_id == null || resume_id < 0 ? null : resume_id;
        categoryId = categoryId == null || categoryId < 0 ? null : categoryId;
      
        Resume article = null;
        if(resume_id==null){
        	resume_id = resumeIdSeq.nextId();
        }
        else{
        	article = talentRepository.findById(resume_id);
        	if(article!=null){
        		
        	} 
        }
    	   
    	BasicDBObject doc = new BasicDBObject();
    	doc.append("resume_id", resume_id);
    	doc.append("status", 0);
    	
    	try{
    		String content = model.txt_content;
    		if(StringUtil.isEmpty(content) || content.length()<256){
    			if(article!=null){
    				content = article.txt_content;
    				doc.append("txt_content", article.txt_content);
    				doc.append("htm_content", article.htm_content);
    				doc.append("fullName", article.fullName);
    				doc.append("categoryId", article.categoryId);
    				if(model.getStatus()==1){
    					doc.append("format", "htm");
    				}
    				else{
    					doc.append("format", "txt");
    				}
    			}
    			else{
    				return CommonUtils.msgCallback(false, "操作失败，resumeId和文本至少填写一个！", "", null);
    			}
    		}
    		else{
    			article = model;
    			content = content.trim();
    			if(content.startsWith("<html") || content.startsWith("<body") || content.startsWith("<!DOCTYPE")){
    				doc.append("htm_content", content);
    				doc.append("format", "htm");
    			}
    			else{
    				doc.append("txt_content", article.txt_content);
    				doc.append("format", "txt");
    			}
				
				doc.append("fullName", article.fullName);
				doc.append("categoryId", article.categoryId);
    		}
    		destination.save(doc,collection);
    		
    		return CommonUtils.msgCallback(true, "解析请求发送成功,请等待结果。", "/oa/talent/view?id="+resume_id, null);
    	}
    	catch(Exception e){
    		return CommonUtils.msgCallback(false, "操作失败，请重试！"+e.getMessage(), "", null);
    	}
    	
        //return "redirect:/oa/talent/view?id="+resume_id;
    }
}
