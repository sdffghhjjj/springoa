package cn.tanghom.app.oa.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import cn.tanghom.app.oa.model.Resume;

@Repository
public interface EduExpRepository  extends MongoRepository<Resume.EduExp, Long> {

	List<Resume.EduExp> findByResumeId(Long resume_id);
}
