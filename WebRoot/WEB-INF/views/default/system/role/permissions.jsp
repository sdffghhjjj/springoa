<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>角色授权</title>
    <jsp:include page="../../header.jsp"></jsp:include>
    <link href="${pageContext.request.contextPath}/vendor/ztree/style/zTreeStyle.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/vendor/ztree/jquery.ztree.core-3.5.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/vendor/ztree/jquery.ztree.excheck-3.5.js" type="text/javascript"></script>
    <style type="text/css">
        .ztree{padding: 0px;}
    </style>
</head>
<body>
<a href="javascript:expandNode();" class="btn btn-default btn-sm">展开/折叠</a>
<a href="javascript: save(0);" class="btn btn-success btn-sm">保存</a>
<ul id="resourcesTree" class="ztree"></ul>
<script type="text/javascript">
    var setting = {
        view: {
            showIcon: false
        },
        check: {
            enable: true
         },
        data: {
            simpleData: {
                enable: true
            }
        }
    };
    $(document).ready(function(){
        $.ajax({
            type:"get",
            url:"${pageContext.servletContext.contextPath}/resources/role_res_json?roleId=${roleId}",
            dataType:"json",
            success:function(data){
                var treeDate = JSON.parse(data.res);
                $.fn.zTree.init($("#resourcesTree"), setting,treeDate);
                var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
                expandNode();
                var roleRes = data.roleRes;
                $.each(roleRes,function(i){
                    var rr = roleRes[i];
                    var node = zTree.getNodesByParam("id", rr.id, null);
                    zTree.checkNode(node[0], true, false);
                });
            }
        });
    });

function save(){
    var roleId="${roleId}";
    var resIds = "";
    var treeObj = $.fn.zTree.getZTreeObj("resourcesTree");
    var nodes = treeObj.getChangeCheckedNodes();
    $.each(nodes,function(i){
        resIds += (i==0)?nodes[i].id:"," + nodes[i].id;
    });
    $.ajax({
        type:"post",
        url:"${pageContext.servletContext.contextPath}/role/save_role_res",
        data:{
            resIds:resIds,
            roleId:roleId
        },
        dataType:"json",
        success:function(data){
            if (data.status == 1) {
                msg.info(data.info);
                setTimeout(function(){
                    window.location.reload();
                },1000);
            } else {
                msg.info(data.info);
            }
        }
    });

}
    //展开或折叠
    var expandAll =false;
    function expandNode() {
        var zTree = $.fn.zTree.getZTreeObj("resourcesTree");
        if (expandAll == false) {
            expandAll =true;
            zTree.expandAll(expandAll);
        } else if (expandAll == true) {
            expandAll =false;
            zTree.expandAll(expandAll);
        }
    }
</script>
</body>
</html>
