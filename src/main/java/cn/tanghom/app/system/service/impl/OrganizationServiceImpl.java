package cn.tanghom.app.system.service.impl;

import cn.tanghom.app.system.mapper.OrganizationMapper;
import cn.tanghom.app.system.model.Organization;
import cn.tanghom.app.system.service.OrganizationService;
import cn.tanghom.support.tree.SelectTreeUtil;
import cn.tanghom.support.tree.SimpleJsonTree;
import cn.tanghom.support.tree.TreeObject;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 部门管理逻辑处理层
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
@Service
public class OrganizationServiceImpl implements OrganizationService {

    @Resource
    private OrganizationMapper organizationMapper;

    @Override
    public String saveOrUpdate(Organization organization) {
        try {
            Organization parentOrg = new Organization();
            if(organization.getParentId()==0){
                parentOrg.setId(0L);
                parentOrg.setParentName("无上级");
            }else{
                parentOrg = organizationMapper.findById(organization.getParentId());
                organization.setParentId(parentOrg.getId());
                organization.setParentName(parentOrg.getParentName());
            }
            if (organization.getId() == null) {// 添加
                organizationMapper.save(organization);
            } else {// 修改
                Organization org = organizationMapper.findById(organization.getId());
                org.setName(organization.getName());
                org.setDescription(organization.getDescription());
                org.setParentId(parentOrg.getId());
                org.setParentName(parentOrg.getParentName());
                org.setStatus(organization.getStatus());
                org.setSort(organization.getSort());
                organizationMapper.update(org);
            }
            return CommonUtils.msgCallback(true, "操作成功", "", JsonUtil.toJSONString(organization));
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void save(Organization organization) {
        organizationMapper.save(organization);
    }

    @Override
    public void update(Organization organization) {
        organizationMapper.update(organization);
    }

    @Override
    public void delete(Long id) {
        organizationMapper.delete(id);
    }

    @Override
    public Organization findById(Long id) {
        return organizationMapper.findById(id);
    }

    @Override
    public List<Organization> findList() {
        return organizationMapper.findListByParams(null);
    }

    @Override
    public List<Organization> queryListByParentId(Long parentId) {
        return organizationMapper.queryListByParentId(parentId,1);
    }

    @Override
    public List<TreeObject> findToSelectTree() {
        List<Organization> organizations = organizationMapper.findListByParams(null);
        List<TreeObject> list = new ArrayList<TreeObject>();
        for (Organization org : organizations) {
            TreeObject ts = new TreeObject();
            ts.setId(org.getId().intValue());
            ts.setName(org.getName());
            ts.setParentId(org.getParentId().intValue());
            list.add(ts);
        }
        SelectTreeUtil selectTreeUtil = new SelectTreeUtil();
        List<TreeObject> orgTree = selectTreeUtil.listToTree(list, 0);
        return orgTree;
    }

    @Override
    public String findListToSimpleJson(Integer status) {
        List<Organization> organizations = organizationMapper.findListByParams(null);
        List<SimpleJsonTree> simpleJsonTreeList = new ArrayList<SimpleJsonTree>();
        if (null != organizations) {
            for (int i = 0; i < organizations.size(); i++) {
                Organization org = organizations.get(i);
                Integer id = org.getId()!=null? org.getId().intValue():0;
                Integer pId = org.getParentId()!=null? org.getParentId().intValue():0;
                SimpleJsonTree simpleJsonTree = new SimpleJsonTree(id, pId, org.getName());
                simpleJsonTreeList.add(simpleJsonTree);
            }
        }
        return JsonUtil.toJSONString(simpleJsonTreeList);
    }

}
