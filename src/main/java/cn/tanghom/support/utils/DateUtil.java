package cn.tanghom.support.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期Util
 *
 * @author tanghom <tanghom@qq.com> 2013-11-18
 * 
 */
public class DateUtil {
	public static void main(String[] args) throws ParseException {
		System.out.println(getNextMonday(2));
		System.out.println(getNextSunday(2));
	}

	/**
	 * 计算当期时间加上N天之后的日期
	 * 
	 * @param date
	 *            当期日期
	 * @param day
	 *            N天
	 * @return N天后的日期
	 * @throws ParseException
	 *             异常
	 */
	public static String dateFomartJs(Date date, int day) throws ParseException {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String dd = fmt.format(date);
		Date df = fmt.parse(dd);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(df);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		String T = fmt.format(calendar.getTime());
		System.out.println(T);
		return T;
	}

	/**
	 * 计算传入参数时间加上N个月之后的日期
	 * 
	 * @param date
	 *            当前时间
	 * @param moth
	 *            月数
	 * @return 结果
	 */
	public static String dataAddMoth(Date date, int moth) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, moth);
		System.out.println(fmt.format(calendar.getTime()));
		String T = fmt.format(calendar.getTime());
		return T;
	}

	/**
	 * 计算当期时间减去N天之后的日期
	 * 
	 * @param date
	 *            当期日期
	 * @param day
	 *            N天
	 * @return N天后的日期
	 * @throws ParseException
	 *             异常
	 */
	public static String dateDelDay(Date date, int day) throws ParseException {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String dd = fmt.format(date);
		Date df = fmt.parse(dd);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(df);
		calendar.add(Calendar.DAY_OF_MONTH, 0 - day);
		String T = fmt.format(calendar.getTime());
		System.out.println(T);
		return T;
	}

	/**
	 * 计算两个日期之间相差的天数
	 * 
	 * @param smdate
	 *            较小的时间
	 * @param bdate
	 *            较大的时间
	 * @return 相差天数
	 * @throws ParseException
	 */
	public static int daysBetween(Date smdate, Date bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		smdate = sdf.parse(sdf.format(smdate));
		bdate = sdf.parse(sdf.format(bdate));
		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}
	/**
	 * 计算两个时间之间相差的分钟数
	 *
	 * @param smdate
	 *            较小的时间
	 * @param bdate
	 *            较大的时间
	 * @return 相差分钟数
	 * @throws ParseException
	 */
	public static int minutesBetween(Date smdate, Date bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		smdate = sdf.parse(sdf.format(smdate));
		bdate = sdf.parse(sdf.format(bdate));
		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long minutes = (time2 - time1) / (1000 * 3600 * 24 * 60);
		return Integer.parseInt(String.valueOf(minutes));
	}

	/**
	 * 字符串的日期格式的计算
	 */
	public static int daysBetweenString(String smdate, String bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(smdate));
		long time1 = cal.getTimeInMillis();
		cal.setTime(sdf.parse(bdate));
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 获取当前日期下周一的日期
	 * @return 下周一日期
	 */
	public static String getNextMonday(Integer num) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.WEEK_OF_YEAR, num);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String monday = df.format(cal.getTime());
		return monday;
	}

	/**
	 * 获取当前日期下周日的日期
	 * @return 下周日期
	 */
	public static String getNextSunday(Integer num) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.WEEK_OF_YEAR, num+1);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String sunday = df.format(cal.getTime());
		return sunday;
	}
	
	/**
	 * 日期转换为字符串
	 * @param date 日期
	 * @param format 格式
	 * @return
	 */
	public static String dateToString(Date date,String format){
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}
	
	/**
	 * 字符串转换为日期
	 * @param dateStr 日期
	 * @param format 格式
	 * @return
	 */
	public static Date stringToDate(String dateStr,String format){
		Date date = new Date();
		try {
			SimpleDateFormat df = new SimpleDateFormat(format);
			date = df.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	/**
	 * 返回当前时间　格式：yyyy-MM-dd hh:mm:ss
	 * 
	 * @return String
	 */
	public static String fromDateH() {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return format1.format(new Date());
	}

	
	/**
	 * 返回当前时间　格式：yyyy-MM-dd
	 * 
	 * @return String
	 */
	public static String fromDateY() {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		return format1.format(new Date());
	}

	public static String fromDateP(String pattern) {
		DateFormat format1 = new SimpleDateFormat(pattern);
		return format1.format(new Date());
	}
}
