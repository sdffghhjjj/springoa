<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>流程模型管理</title>
	<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>流程管理</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>KEY</th>
						<th>Name</th>
						<th>Version</th>
						<th>创建时间</th>
						<th>最后更新时间</th>
						<th>元数据</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${page.items }" var="model">
						<tr>
							<td>${model.id }</td>
							<td>${model.key }</td>
							<td>${model.name}</td>
							<td>${model.version}</td>
							<td><fmt:formatDate pattern="YYYY/MM/dd HH:mm:ss"  value="${model.createTime}" /></td>
							<td><fmt:formatDate pattern="YYYY/MM/dd HH:mm:ss"  value="${model.lastUpdateTime}" /></td>
							<td>${model.metaInfo}</td>
							<td>
								<a href="${ctx}/modeler.html?modelId=${model.id}" target="_blank" class="label label-success">编辑</a>
								<a href="javascript:;"  url="${ctx}/workflow/model/deploy/${model.id}" data-info="确认部署吗?" class="ajax-get label label-info">部署</a>
								<a href="javascript:;"  url="${ctx}/workflow/model/delete/${model.id}" data-info="确认删除吗?" class="ajax-get label label-danger">删除</a>
								导出(<a href="${ctx}/workflow/model/export/${model.id}/bpmn" target="_blank">BPMN</a>
								|&nbsp;<a href="${ctx}/workflow/model/export/${model.id}/json" target="_blank">JSON</a>)
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="clearfix">
				<div class="pull-right">
					<mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
				</div>
			</div>
        <form id="modelForm" action="${ctx}/workflow/model/create.shtml" target="_blank" method="post" class="form-horizontal">
		<table>
			<tr>
				<td>名称：</td>
				<td>
					<input id="name" name="name" type="text" class="form-control" />
				</td>
			</tr>
			<tr>
				<td>KEY：</td>
				<td>
					<input id="key" name="key" type="text" class="form-control" />
				</td>
			</tr>
			<tr>
				<td>描述：</td>
				<td>
					<textarea id="description" name="description" style="width:300px;height: 50px;" class="form-control" ></textarea>
				</td>
			</tr>			
		</table>
		<br/>
		<input type="submit" class="btn" value="创建模型" />
        </form>
	</div>
</body>
</html>