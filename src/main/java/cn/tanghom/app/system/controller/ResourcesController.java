package cn.tanghom.app.system.controller;

import cn.tanghom.app.system.model.Resources;
import cn.tanghom.app.system.service.ResourcesService;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 资源管理控制层
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/resources")
public class ResourcesController {

    @Resource
    private ResourcesService resourcesService;

    /**
     * 菜单管理首页
     * @return
     */
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String list() {
        return CommonKey.WEB_PATH + "/system/resources/index";
    }

    /**
     * 查询菜单返回
     * @return
     */
    @RequestMapping(value = "json", method = RequestMethod.GET)
    @ResponseBody
    public String json() {
        String json = resourcesService.findListToSimpleJson(null);
        return json;
    }

    /**
     * 编辑
     * @param id
     * @return
     */
    @RequestMapping(value = "edit", method = RequestMethod.GET)
    @ResponseBody
    public String editJson(Long id) {
        Resources resources = resourcesService.findById(id);
        String json = JsonUtil.toJSONString(resources);
        return json;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    @ResponseBody
    public String addJson(Long id) {
        try {
           resourcesService.delete(id);
            return CommonUtils.msgCallback(true, "操作成功", "", null);
        }catch (Exception e){
            return CommonUtils.msgCallback(false, "操作失败，" + e, "", null);
        }
    }

    /**
     * 保存
     * @param resources
     * @return
     */
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public String save(Resources resources) {
        try {
            String result = resourcesService.saveOrUpdate(resources);
            return result;
        }catch (Exception e){
            return CommonUtils.msgCallback(false, "操作失败，请重试！" , "", null);
        }
    }
    /**
     * 修改ParentId
     * @param id
     * @param parentId
     * @return
     */
    @RequestMapping(value = "update_parent", method = RequestMethod.POST)
    @ResponseBody
    public String save(Long id,Long parentId) {
        try {
            Resources resources =resourcesService.findById(id);
            resources.setParentId(parentId);
            resourcesService.update(resources);
            return CommonUtils.msgCallback(true, "操作成功", "", null);

        }catch (Exception e){
            return CommonUtils.msgCallback(false, "操作失败，请重试！" , "", null);
        }
    }

    /**
     * 角色所有资源
     *
     * @return JSON
     */
    @RequestMapping(value = "/role_res_json", method = RequestMethod.GET)
    @ResponseBody
    public String roleResJson(Long roleId) {
        List<Resources> roleResoucess = resourcesService.findRoleResourcess(roleId);
        String resJson = resourcesService.findListToSimpleJson(1);
        Map<String,Object> maps = new HashMap<String,Object>();
        maps.put("roleRes",roleResoucess);
        maps.put("res",resJson);
        return JsonUtil.toJSONString(maps);
    }
}
