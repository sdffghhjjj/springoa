package cn.tanghom.app.baiding.model;

import cn.tanghom.app.system.model.Accessory;
import cn.tanghom.app.system.model.Role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 文章
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/25
 */
public class Article implements Serializable {
    private static final long serialVersionUID = 3234713207604217904L;
    private Long id;
    private String title;//标题
    private String content;//内容
    private Long categoryId;//分类ID
    private String categoryName;//分类
    private String fileIds;//图片ID
    private List<Accessory> Accessorys = new ArrayList<Accessory>();//  关联图片
    private Long createrId;//发起人
    private String createrName;//发起人
    private Date createTime; //新建时间
    private Integer status = 1;//状态:0、禁用；1、正常； 默认：1、正常
    private Integer sort = 0;//排序
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public String getCreaterName() {
        return createrName;
    }

    public void setCreaterName(String createrName) {
        this.createrName = createrName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getFileIds() {
        return fileIds;
    }

    public void setFileIds(String fileIds) {
        this.fileIds = fileIds;
    }

    public List<Accessory> getAccessorys() {
        return Accessorys;
    }

    public void setAccessorys(List<Accessory> accessorys) {
        Accessorys = accessorys;
    }
}
