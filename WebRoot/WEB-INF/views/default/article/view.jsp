<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>文章</title>
	<jsp:include page="../header.jsp"></jsp:include>	

</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>${article.id==null?'新增':'修改'}文章</b>
				<small class="pull-right">
					<a href="javascript:javascript:history.go(-1);" class="pd-r-xs"><i class="fa fa-reply"></i> 返回</a>
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<h3>${article.title}"</h3>
			<div class="panel-body">				
				<div class="form-group">
					<label>分类:${article.categoryName}</label>					
				</div>
	   		 
					<div class="form-group">
						<label>图片:</label>
						<div id="uploader-demo">
							<!--用来存放item-->
							<div id="fileList" class="uploader-list">
							<c:forEach var="access" items="${article.accessorys}">
								<div id="WU_FILE_${access.id}" class="file-item thumbnail upload-state-done">
									<img src="${pageContext.request.contextPath}/${access.path}">
									<div class="info">${access.title}</div>									
								</div>
							</c:forEach>
							</div>
						
						</div>
					</div>
				   <div class="form-group">
				    	<div name="content" id="editor" style="height:500px;width:100%;">${article.content}</div>  	   		 		 	
				  </div>
        
			</div>
		</section>
	</div>
</section>
 
</body>
</html>
