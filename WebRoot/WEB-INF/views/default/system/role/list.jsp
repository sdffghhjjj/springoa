<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>角色管理</title>
	<jsp:include page="../../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">

		<section class="panel panel-default">
			<div class="panel-heading">
				<b>角色列表</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<div class="row table-search">
					<div class="col-xs-2">
						<a href="${pageContext.servletContext.contextPath}/role/add.shtml">
							<span class="btn btn-success btn-sm">新增</span>
						</a>
					</div>
					<div class="col-xs-10 text-right">
						<form action="" method="post" class="form-inline">
							<div class="form-group">
								<span>名称:</span>
								<input type="text" class="form-control input-sm" value="${param.name}" name="name" placeholder="名称">
							</div>
							<div class="form-group">
								<span>KEY:</span>
								<input type="text" class="form-control input-sm" value="${param.key}" name="key"placeholder="唯一KEY">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-sm">搜索</button>
							</div>
						</form>
					</div>
				</div>
				<table class="table">
					<thead>
					<tr>
						<th>名称</th>
						<th>KEY</th>
						<th>描述</th>
						<th>状态</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="role" items="${page.items}" varStatus="s">
						<tr>
							<td>${role.name}</td>
							<td>${role.roleKey}</td>
							<td>${role.description}</td>
							<td>${role.status==1?'启用':'禁用'}</td>
							<td>
								<a href="${pageContext.servletContext.contextPath}/role/edit.shtml?id=${role.id}" class="label label-success">编辑</a>
								<a href="javascript:;"  url="${pageContext.servletContext.contextPath}/role/delete.shtml?id=${role.id}" data-info="确认删除吗?" class="ajax-get label label-danger">删除</a>
								<a href="javascript:setPermissions('${role.id}','${role.name}');" class="label label-warning">授权</a>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${empty page}">
						<td colspan="5" class="td-center">无相关数据</td>
					</c:if>
					</tbody>
				</table>
				<div class="clearfix">
					<div class="pull-right">
						<mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
					</div>
				</div>
			</div>
		</section>
	</div>
</section>
<script>
	function setPermissions(roleId,roleName){
		var content = '<iframe scrolling="auto" frameborder="0" src="${pageContext.request.contextPath}/role/permissions.shtml?roleId='+roleId+'" style="width:400px;height:400px;"></iframe>';
		var d = dialog({
			height:400,
			width:400,
			fixed:true,
			quickClose: true,
			title:'设置权限('+roleName+')',
			content: content
		});
		d.show();

	}
</script>
</body>
</html>