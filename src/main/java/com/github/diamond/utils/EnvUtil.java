package com.github.diamond.utils;

import java.util.Properties;

import org.activiti.conf.PropertiesLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.diamond.Constants;

/**
 * 
 * @author libinsong1204@gmail.com
 *
 */
public abstract class EnvUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnvUtil.class);
	
	private static String profile;
	private static String projectName;
	private static String projectBaseDir;
	private static String buildVersion;
	private static String buildTime;
	
	static {
		try {
			PropertiesLoader config = new PropertiesLoader("META-INF/res/env.properties");
			
			LOGGER.info("加载env.properties");
			
			profile = config.getProperty("spring.profiles.active");
			projectName = config.getProperty("project.name");
			projectBaseDir = config.getProperty("project.basedir");
			buildVersion = config.getProperty("build.version");
			buildTime = config.getProperty("build.time");
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
	
	public static String getSpringProfile() {
		return profile;
	}
	
	public static boolean isDevelopment() {
		if(profile != null  && !profile.contains(Constants.PROFILE_TEST) && 
				!profile.contains(Constants.PROFILE_PRODUCTION))
			return true;
		else
			return false;
	}
	
	public static boolean isTest() {
		if(profile != null  && profile.contains(Constants.PROFILE_TEST))
			return true;
		else
			return false;
	}
	
	public static boolean isProduction() {
		if(profile != null  && profile.contains(Constants.PROFILE_PRODUCTION))
			return true;
		else
			return false;
	}

	public static String getProjectName() {
		return projectName;
	}

	public static String getProjectBaseDir() {
		return projectBaseDir;
	}

	public static String getBuildVersion() {
		return buildVersion;
	}

	public static String getBuildTime() {
		return buildTime;
	}
	
}
