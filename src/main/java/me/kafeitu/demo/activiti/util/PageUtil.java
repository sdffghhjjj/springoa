package me.kafeitu.demo.activiti.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 分页工具
 *
 * @author henryyan
 */
public class PageUtil {

    public static int PAGE_SIZE = 20;

    public static int[] init(Page<?> page, HttpServletRequest request) {
        int pageNumber = Integer.parseInt(StringUtils.defaultIfBlank(request.getParameter("page"), "1"));
        page.setPageNo(pageNumber);
        int pageSize = Integer.parseInt(StringUtils.defaultIfBlank(request.getParameter("limit"), String.valueOf(PAGE_SIZE)));
        page.setPageSize(pageSize);
        int firstResult = page.getFirst() - 1;
        int maxResults = page.getPageSize();
        return new int[]{firstResult, maxResults};
    }
    
    public static int pageCount(long recordCount, int limit) {
	    int pc = (int)Math.ceil(recordCount * 1d / limit);
	    return (pc==0)? 1 : pc;
	}
	
	public static int getOffset(int page, int limit) {
		if(page < 1)
			page = 1;
		return (page - 1) * limit;
	}

}
