package cn.tanghom.app.system.mapper;

import cn.tanghom.app.system.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * UserMapper
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Repository
public interface UserMapper {

    /**
     * 保存
     *
     * @param user
     */
    void save(User user);

    /**
     * 更新
     *
     * @param user
     * @return
     */
    void update(User user);

    /**
     * 单个删除
     *
     * @param id
     * @return
     */
    void delete(@Param(value = "id") Long id);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    User findById(@Param(value = "id") Long id);

    /**
     * 根据用户名查找用户
     *
     * @param loginName
     * @return
     */
    User findByLoginName(@Param(value = "loginName") String loginName);

    /**
     * 查询所有
     *
     * @return
     */
    List<User> findListByParams();

    /**
     * 查询分页
     *
     * @param first     数据第几条查询
     * @param size      查询数量大小
     * @param userName  用户名称
     * @param loginName 登录帐号
     * @param orgId     部门Id
     * @return
     */
    List<User> findPageByParams(@Param(value = "userName") String userName, @Param(value = "loginName") String loginName, @Param(value = "orgId") Long orgId, @Param(value = "status") Integer status, @Param(value = "ids") String ids, @Param(value = "first") Long first, @Param(value = "size") Integer size);

    /**
     * 分页返回条数
     *
     * @param userName  用户名称
     * @param loginName 登录帐号
     * @param orgId     部门Id
     * @return
     */
    Long countByParams(@Param(value = "userName") String userName, @Param(value = "loginName") String loginName, @Param(value = "orgId") Long orgId, @Param(value = "status") Integer status, @Param(value = "ids") String ids);
    
    
    List<Map> executeSql(String sql);
}
