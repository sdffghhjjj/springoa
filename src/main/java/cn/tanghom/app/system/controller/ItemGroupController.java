package cn.tanghom.app.system.controller;


import cn.tanghom.app.system.mapper.AccessoryMapper;
import cn.tanghom.app.system.mapper.ItemGroupMapper;
import cn.tanghom.app.system.model.ItemGroup;
import cn.tanghom.app.system.model.ItemInfo;
import cn.tanghom.support.db.BaseCrudDao;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;

import org.activiti.conf.CommonKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

import java.util.List;

/**
 * 列表管理控制层
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/item")
public class ItemGroupController {

   
	//@Autowired 
    private BaseCrudDao<ItemInfo,Long> articleService = new BaseCrudDao<ItemInfo,Long>();   
    
    @Resource
    private ItemGroupMapper articleCategoryService;

    /**
     * 列表
     *
     * @param name
     * @param status
     * @param model
     * @return
     */
    @RequestMapping(value = "/group_list", method = {RequestMethod.POST, RequestMethod.GET})
    public String list(String name, Integer status, Model model) {
        List<ItemGroup> articleCategoryList = articleCategoryService.findListByParams(name,status);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/item_group/list";
    }
    
    @RequestMapping(value = "/item_list", method = {RequestMethod.POST, RequestMethod.GET})
    public String itemList(String groupKey, Integer status, Model model) {
        List<ItemInfo> articleCategoryList = articleCategoryService.findItemListByGroupKey(groupKey,status);
        model.addAttribute("itemList", articleCategoryList);
        return CommonKey.WEB_PATH + "/item_group/item_list";
    }

    /**
     * 新增文章分类页面
     *
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add() {
        return CommonKey.WEB_PATH + "/item_group/edit";
    }

    /**
     * 编辑文章分类页面
     *
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(Long id, Model model) {
        ItemGroup articleCategory = articleCategoryService.findById(id);
        model.addAttribute("articleCategory", articleCategory);
        return CommonKey.WEB_PATH + "/item_group/edit";
    }

    /**
     * 保存文章分类
     *
     * @param articleCategory 文章分类对象
     * @return
     */
    @RequestMapping(value = "/save", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @ResponseBody
    public String add(ItemGroup articleCategory) {
        try {
        	articleCategoryService.save(articleCategory);
            String result =  CommonUtils.msgCallback(true, "操作成功", "", JsonUtil.toJSONString(articleCategory));
            return result;
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "操作失败，请重试！", "", null);
        }
    }

    

    /**
     * 新增项目页面
     *
     * @return
     */
    @RequestMapping(value = "/add_item", method = RequestMethod.GET)
    public String addItem() {
        return CommonKey.WEB_PATH + "/item_group/edit_item";
    }
    
    /**
     * 删除项目
     *
     * @param id 文章分类ID
     * @return 结果
     */
    @ResponseBody
    @RequestMapping(value = "/delete_item", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    public String deleteItem(Long id) {
        try {           
            articleService.delete(id);
            return CommonUtils.msgCallback(true, "删除项目成功", "", null);
        } catch (Exception ex) {
            return CommonUtils.msgCallback(false, "删除项目失败", "", null);

        }
    }
}
