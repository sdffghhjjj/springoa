package cn.tanghom.support.shiro;

import cn.tanghom.app.system.model.Resources;
import cn.tanghom.app.system.service.ResourcesService;
import cn.tanghom.support.utils.LoggerUtils;
import org.apache.shiro.config.Ini;
import org.springframework.beans.factory.FactoryBean;

import javax.annotation.Resource;
import java.util.List;

/**
 * 产生责任链，确定每个url的访问权限
 * 
 * @author tanghom <tanghom@qq.com> 2015-11-18
 */
public class ChainDefinitionSectionMetaSource implements FactoryBean<Ini.Section> {

	@Resource
	private ResourcesService resourcesService;
	
	// 静态资源访问权限
	private String filterChainDefinitions = null;
	public Ini.Section getObject() throws Exception {
		LoggerUtils.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>加载资源");
		Ini ini = new Ini();
		// 加载默认的url
		ini.load(filterChainDefinitions);
		Ini.Section section = ini.getSection(Ini.DEFAULT_SECTION_NAME);
		// 循环Resource的url,逐个添加到section中。section就是filterChainDefinitionMap,
		// 里面的键就是链接URL,值就是存在什么条件才能访问该链接
		List<Resources> resources = resourcesService.findListByParams(1);
		for (Resources resource : resources) {
			// 构成permission字符串
			String permission = "perms[" + resource.getResKey() + "]";
			section.put(resource.getUrl() + "", permission);
		}
		return section;
	}

	/**
	 * 通过filterChainDefinitions对默认的url过滤定义
	 * 
	 * @param filterChainDefinitions
	 *            默认的url过滤定义
	 */
	public void setFilterChainDefinitions(String filterChainDefinitions) {
		this.filterChainDefinitions = filterChainDefinitions;
	}

	public Class<?> getObjectType() {
		return this.getClass();
	}

	public boolean isSingleton() {
		return false;
	}
}
