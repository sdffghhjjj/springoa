package cn.tanghom.app.system.service;

import cn.tanghom.app.system.model.Role;
import cn.tanghom.support.page.Pager;

import java.util.List;
import java.util.Map;

/**
 * 角色管理逻辑处理层
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
public interface RoleService {
    /**
     * 保存角色
     *
     * @param role
     * @param userId
     */
    String saveOrUpdate(Role role, Long userId[]);

    /**
     * 保存角色
     *
     * @param role
     */
    void save(Role role);

    /**
     * 更新保存
     *
     * @param role
     */
    void update(Role role);

    /**
     * 根据iD删除角色
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询角色
     *
     * @param id
     * @return
     */
    Role findById(Long id);

    /**
     * 根据名称或key查找角色
     *
     * @param name name
     * @param key  key
     * @return
     */
    Role findByNameOrKey(String name, String key);

    /**
     * 查询所有角色
     *
     * @return
     */
    List<Role> findList();

    /**
     * 分页查询
     *
     * @param name  角色名称
     * @param key   key
     * @param index 当前页
     * @return
     */
    Pager<Role> findPageByParams(String name, String key, Long index);

    /**
     * 查询角色和资源集合
     * @param roleId
     * @param resId
     * @return
     */
    List<Map<String, Object>> findRoleResListByParams(Long roleId, Long resId);

    /**
     * 根据用户ID查询用户角色
     * @param userId
     * @return
     */
    List<Role> findUserRoles(Long userId);

    /**
     * 保存角色绑定的资源
     * @param roleId
     * @param resIds
     */
    void saveRoleRes(Long roleId,Long resIds[]);
}
