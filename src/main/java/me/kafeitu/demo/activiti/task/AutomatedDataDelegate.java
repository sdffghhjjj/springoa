package me.kafeitu.demo.activiti.task;

import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * <serviceTask id="automatedIntro" name="Generic and Automated Data Entry" activiti:class="me.kafeitu.demo.activiti.task.AutomatedDataDelegate"></serviceTask>
 * 
 * @author Hunteron-cp
 *
 */
public class AutomatedDataDelegate implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    Date now = new Date();
    execution.setVariable("autoWelcomeTime", now);
    System.out.println("Faux call to backend for ["  + execution.getVariable("fullName") + "]");
  }

}
 