package cn.tanghom.app.baiding.controller.web;

import cn.tanghom.app.baiding.model.Article;
import cn.tanghom.app.baiding.model.ArticleCategory;
import cn.tanghom.app.baiding.service.ArticleCategoryService;
import cn.tanghom.app.baiding.service.ArticleService;
import cn.tanghom.app.system.model.Accessory;
import cn.tanghom.app.system.service.AccessoryService;
import cn.tanghom.support.page.Pager;
import cn.tanghom.support.utils.CommonUtils;
import cn.tanghom.support.utils.JsonUtil;
import cn.tanghom.support.utils.StringUtils;

import org.activiti.conf.CommonKey;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文章列表管理控制层
 *
 * @author tanghom
 */
@Controller
@RequestMapping("/article")
public class ArticleController {

    @Resource
    private ArticleService articleService;
    @Resource
    private ArticleCategoryService articleCategoryService;
    @Resource
    private AccessoryService accessoryService;

    /**
     * 列表
     *
     * @param categoryId
     * @param status
     * @param model
     * @return
     */
    @RequestMapping(value = "/list", method = {RequestMethod.POST, RequestMethod.GET})
    public String list(String title, Long categoryId, Integer status, Long pageNo, Model model) {
        status = status == null || status < 0 ? null : status;
        categoryId = categoryId == null || categoryId < 0 ? null : categoryId;
        Pager<Article> page = articleService.findPageByParams(title, categoryId, status, pageNo);
        model.addAttribute("page", page);
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParams(null, null);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/article/list";
    }
    

    /**
     * 显示文章页面
     *
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String view(Long id, Model model) {
        Article article = articleService.findById(id);
        if(!StringUtils.isNullOrEmpty(article.getFileIds())){
            List<Accessory> accessories = accessoryService.findByIdStr(article.getFileIds());
            article.setAccessorys(accessories);
        }
        model.addAttribute("article", article);
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParams(null, 1);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/article/view";
    }

    /**
     * 新增文章页面
     *
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParams(null, 1);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/article/edit";
    }

    /**
     * 编辑文章页面
     *
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(Long id, Model model) {
        Article article = articleService.findById(id);
        if(!StringUtils.isNullOrEmpty(article.getFileIds())){
            List<Accessory> accessories = accessoryService.findByIdStr(article.getFileIds());
            article.setAccessorys(accessories);
        }
        model.addAttribute("article", article);
        List<ArticleCategory> articleCategoryList = articleCategoryService.findListByParams(null, 1);
        model.addAttribute("articleCategoryList", articleCategoryList);
        return CommonKey.WEB_PATH + "/article/edit";
    }

    /**
     * 保存文章
     *
     * @param article 文章对象
     * @return
     */
    @RequestMapping(value = "/save", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @ResponseBody
    public String add(Article article, HttpServletRequest request) {
        try {
            String fileIdStr = "";
            String[] fileIds = request.getParameterValues("fileId[]");
            if(null!=fileIds){
                if (fileIds.length > 0) {
                    for (int i = 0; i < fileIds.length; i++) {
                        String id =fileIds[i];
                        fileIdStr += i == 0 ? id : "," +id ;
                    }
                }
            }
            article.setFileIds(fileIdStr);
            String result = articleService.saveOrUpdate(article);
            return result;
        } catch (Exception e) {
            return CommonUtils.msgCallback(false, "操作失败，请重试！"+e.getMessage(), "", null);
        }
    }

    /**
     * 删除文章
     *
     * @param id 文章ID
     * @return 结果
     */
    @ResponseBody
    @RequestMapping(value = "/delete", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    public String delete(Long id) {
        try {
            articleService.delete(id);
            return CommonUtils.msgCallback(true, "删除文章成功", "", null);
        } catch (Exception ex) {
            return CommonUtils.msgCallback(false, "删除文章失败", "", null);

        }
    }
}
