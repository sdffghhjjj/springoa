package cn.tanghom.app.baiding.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Table(name = "t_dashboard")
@Data
public class Dashboard {
	enum Type{block,metric,danger,success,link}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true)	
	Long id;
	String name;
	String label;
	
	@Column(name = "type", length = 32)
	@Enumerated(EnumType.STRING)
	Type type;
	
	Double value;	
	String content;
	String format;
	String icon = "fa-anchor";
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	@Column(name = "create_time")
	Date createTime;

}
