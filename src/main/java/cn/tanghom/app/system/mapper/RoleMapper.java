package cn.tanghom.app.system.mapper;

import cn.tanghom.app.system.model.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色映射
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Repository
public interface RoleMapper {

    /**
     * 保存
     *
     * @param role
     */
    void save(Role role);

    /**
     * 更新
     *
     * @param role
     * @return
     */
    void update(Role role);

    /**
     * 单个删除
     *
     * @param id
     * @return
     */
    void delete(@Param(value = "id") Long id);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Role findById(@Param(value = "id") Long id);

    /**
     * 根据角色Key或名称查找角色
     *
     * @param key
     * @param name
     * @return
     */
    Role findByNameOrKey(@Param(value = "name") String name, @Param(value = "key") String key);

    /**
     * 查询所有
     *
     * @return
     */
    List<Role> findListByParams(@Param(value = "name") String name, @Param(value = "key") String key);

    /**
     * 查询分页
     *
     * @param name  角色名称
     * @param key   角色key
     * @param first 数据第几条查询
     * @param size  查询数量大小
     * @return
     */
    List<Role> findPageByParams(@Param(value = "name") String name, @Param(value = "key") String key, @Param(value = "first") Long first, @Param(value = "size") Integer size);

    /**
     * 分页返回条数
     *
     * @param name 角色名称
     * @param key  角色key
     * @return
     */
    Long countByParams(@Param(value = "name") String name, @Param(value = "key") String key);
    /**
     * 分页返回条数
     *
     * @param userId 用户Id
     * @return
     */
    List<Role> findUserRoles(@Param(value = "userId") Long userId);
}
