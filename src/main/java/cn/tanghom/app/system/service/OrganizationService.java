package cn.tanghom.app.system.service;

import cn.tanghom.app.system.model.Organization;
import cn.tanghom.support.tree.TreeObject;

import java.util.List;

/**
 * 组织机构管理逻辑处理层
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
public interface OrganizationService {
    /**
     * 保存组织机构
     *
     * @param organization
     */
    String saveOrUpdate(Organization organization);
    /**
     * 保存组织机构
     *
     * @param organization
     */
    void save(Organization organization);

    /**
     * 更新保存
     *
     * @param organization
     */
    void update(Organization organization);

    /**
     * 根据iD删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询文章
     *
     * @param id
     * @return
     */
    Organization findById(Long id);
    /**
     * 查询所有部门
     * @return
     */
    List<Organization> findList();

    /**
     * 根据父级ID查询组织机构
     * @param parentId
     * @return
     */
    List<Organization> queryListByParentId(Long parentId);

    /**
     * 返回tree结构
     * @return
     */
    List<TreeObject> findToSelectTree();
    /**
     *查询返回json
     * @param status
     * @return
     */
    String findListToSimpleJson(Integer status);
}
