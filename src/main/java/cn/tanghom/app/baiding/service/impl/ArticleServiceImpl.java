package cn.tanghom.app.baiding.service.impl;

import cn.tanghom.app.baiding.mapper.ArticleCategoryMapper;
import cn.tanghom.app.baiding.mapper.ArticleMapper;
import cn.tanghom.app.baiding.model.Article;
import cn.tanghom.app.baiding.model.ArticleCategory;
import cn.tanghom.app.baiding.service.ArticleService;
import cn.tanghom.app.system.model.User;
import cn.tanghom.support.page.Pager;
import cn.tanghom.support.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 描述
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/25
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    @Resource
    private ArticleMapper articleMapper;
    @Resource
    private ArticleCategoryMapper articleCategoryMapper;

    @Override
    public String saveOrUpdate(Article article) {
        try {
            ArticleCategory articleCategory = articleCategoryMapper.findById(article.getCategoryId());
            article.setCategoryId(articleCategory.getId());
            article.setCategoryName(articleCategory.getName());
            if (article.getId() == null) {// 添加
                User loger = CommonUtils.getUserSession();
                article.setCreaterId(loger.getId());
                article.setCreaterName(loger.getUserName());
                article.setCreateTime(new Date());
                articleMapper.save(article);
            } else {// 修改
                Article ac = articleMapper.findById(article.getId());
                ac.setTitle(article.getTitle());
                ac.setContent(article.getContent());
                ac.setCategoryId(article.getCategoryId());
                ac.setCategoryName(article.getCategoryName());
                ac.setSort(article.getSort());
                ac.setFileIds(article.getFileIds());
                ac.setStatus(article.getStatus());
                articleMapper.update(ac);
            }
            return CommonUtils.msgCallback(true, "操作成功", "", null);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void save(Article article) {
        articleMapper.save(article);
    }

    @Override
    public void update(Article article) {
        articleMapper.update(article);
    }

    @Override
    public void delete(Long id) {
        articleMapper.delete(id);
    }

    @Override
    public Article findById(Long id) {
        return articleMapper.findById(id);
    }

    @Override
    public List<Article> findListByParams(Long categoryId, Integer status) {
        return articleMapper.findListByParams(categoryId, status);
    }

    @Override
    public Pager<Article> findPageByParams(String title, Long categoryId, Integer status, Long index) {
        Pager<Article> pager = new Pager<Article>();
        pager.setIndex(index);
        pager.setSize(10);
        List<Article> articleList = articleMapper.findPageByParams(title, categoryId,status,pager.startRow(), pager.getSize());
        pager.setItems(articleList);
        Long totalRecord = articleMapper.countByParams(title, categoryId,status);
        pager.setTotalRecord(totalRecord);
        return pager;
    }
}
