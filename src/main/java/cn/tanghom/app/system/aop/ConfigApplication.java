package cn.tanghom.app.system.aop;

import java.util.Map;


import org.activiti.conf.CommonKey;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * appliction 设置
 * Created by tanghom on 2016/4/18.
 */
@Component
public class ConfigApplication implements ServletContextAware, ApplicationContextAware  {
	private static ApplicationContext applicationContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        String webRoot = CommonKey.application.getProperty("web_root");
        servletContext.setAttribute("WEB_ROOT",webRoot);
    }
    
   
    @Override
    public void setApplicationContext(ApplicationContext arg0)    throws BeansException {
        applicationContext = arg0;
    }
 
    /**
     * 获取applicationContext对象
     * @return
     */
    public static ApplicationContext getApplicationContext(){
        return applicationContext;
    }
     
    /**
     * 根据bean的id来查找对象
     * @param id
     * @return
     */
    public static Object getBean(String id){
        return applicationContext.getBean(id);
    }
     
    /**
     * 根据bean的class来查找对象
     * @param c
     * @return
     */
    public static Object getBean(Class c){
        return applicationContext.getBean(c);
    }
     
    /**
     * 根据bean的class来查找所有的对象(包括子类)
     * @param c
     * @return
     */
    public static Map getBeansOfType(Class c){
        return applicationContext.getBeansOfType(c);
    }
}
