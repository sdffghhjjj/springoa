package cn.tanghom.app.baiding.service;


import cn.tanghom.app.baiding.model.Article;
import cn.tanghom.support.page.Pager;

import java.util.List;

/**
 * 文章管理逻辑处理层
 *
 * @author tanghom<tanghom@qq.com> on 2016/5/16
 */
public interface ArticleService {
    /**
     * 保存文章
     *
     * @param article
     */
    String saveOrUpdate(Article article);

    /**
     * 保存文章
     *
     * @param article
     */
    void save(Article article);

    /**
     * 更新保存
     *
     * @param article
     */
    void update(Article article);

    /**
     * 根据iD删除文章
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据ID查询文章
     *
     * @param id
     * @return
     */
    Article findById(Long id);

    /**
     * 按条件查询所有文章
     *
     * @return
     */
    List<Article> findListByParams(Long categoryId, Integer status);
    /**
     * 按条件查询所有文章
     *
     * @return
     */
    Pager<Article> findPageByParams(String title, Long categoryId, Integer status, Long index);
}
