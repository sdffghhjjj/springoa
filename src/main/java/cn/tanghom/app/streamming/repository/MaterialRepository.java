package cn.tanghom.app.streamming.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.tanghom.app.streamming.model.Material;

public interface MaterialRepository extends JpaRepository<Material, Long> {
}