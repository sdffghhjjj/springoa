package cn.tanghom.app.system.controller;

import cn.tanghom.app.baiding.model.Dashboard;
import cn.tanghom.app.baiding.repository.DashboardRepository;
import cn.tanghom.app.system.model.Resources;
import cn.tanghom.app.system.model.User;
import cn.tanghom.app.system.service.ResourcesService;
import cn.tanghom.app.system.service.UserService;
import cn.tanghom.support.tree.TreeObject;
import cn.tanghom.support.tree.TreeUtil;
import cn.tanghom.support.utils.CommonUtils;

import com.mysql.jdbc.Connection;

import org.activiti.conf.CommonKey;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 首页控制器
 *
 * @author tanghom
 */
@Controller
@RequestMapping
public class MainController {
    @Resource
    private UserService userService;
    @Resource
    private ResourcesService resourcesService;
    
    @Resource
    DashboardRepository dashboardRepository;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String root(Model model) {
        Subject currentUser = SecurityUtils.getSubject();
        String loginName = (String)currentUser.getPrincipal();
        User loger = userService.findByLoginName(loginName);
        Session session = currentUser.getSession();
        session.setAttribute(CommonKey.USER_SESSION,loger);
        List<Resources> resources = new ArrayList<Resources>();
        if (1 == loger.getId()) {
            resources = resourcesService.findListByParams(1);
        } else {
            resources = resourcesService.findUserResourcess(loger.getId());
        }
        List<TreeObject> list = new ArrayList<TreeObject>();
        for (Resources res : resources) {
            TreeObject ts = new TreeObject();
            ts.setId(res.getId().intValue());
            ts.setName(res.getName());
            ts.setParentId(res.getParentId()==null?0:res.getParentId().intValue());
            ts.setResUrl(res.getUrl());
            ts.setResKey(res.getResKey());
            ts.setIcon(res.getIcon());
            list.add(ts);
        }
        TreeUtil treeUtil = new TreeUtil();
        List<TreeObject> menus = treeUtil.getChildTreeObjects(list, 0);
        model.addAttribute("menus", menus);
        return CommonKey.WEB_PATH + "/main";
    }
    
    
    /**
     * index
     *
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index( Model model) {
    	
    	Iterable<Dashboard> dashs = dashboardRepository.findAll();
    	model.addAttribute("dashboards", dashs);
    	
        return CommonKey.WEB_PATH + "/index";
    }

    /**
     * add@byron
     * 演示demo页面
     */    
    @RequestMapping(value = "main/index")
    public String index() {
        return "/main/index";
    }

    @RequestMapping(value = "main/welcome")
    public String welcome() {
        return "/main/welcome";
    }   
    
    //end@
    
    @RequestMapping(value = "install", method = RequestMethod.GET)
    @ResponseBody
    public String install() throws Exception {

        try {
            Properties props = org.apache.ibatis.io.Resources.getResourceAsProperties("jdbc.properties");
            String driver = props.getProperty("jdbc.driverClassName");
            String url = props.getProperty("jdbc.url");
            String username = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");
            Class.forName(driver).newInstance();
            Connection conn = (Connection) DriverManager.getConnection(url, username, password);
            ScriptRunner runner = new ScriptRunner(conn);
            runner.setErrorLogWriter(null);
            runner.setLogWriter(null);
            InputStream in = getClass().getResourceAsStream("/sql/db_install.sql");
            if(in!=null){
            	runner.runScript((new InputStreamReader(in,"UTF-8")));
            }
            else{
            	return "读取sql/db_install.sql文件失败！";
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "初始化失败！请联系管理员" + e;
        }

        return "初始化成功";
    }

}
