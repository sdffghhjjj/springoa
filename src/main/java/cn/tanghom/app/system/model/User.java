package cn.tanghom.app.system.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.tanghom.app.system.service.RoleService;
import cn.tanghom.app.system.service.impl.RoleServiceImpl;

/**
 * 用户对象
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
public class User implements Serializable {
    private static final long serialVersionUID = -8155028965166110406L;
    private Long id;//ID
    private String loginName;//登录帐号
    private String loginPwd;//登录密码
    
    private String userName;//姓名,realName 可以重复
    private String sex; // 性别
    private Long organizationId; //用户所属机构ID
    private String organizationName; //用户所属机构Name
    private String phone;//电话
    private String email;//电话
    private String avator;//头像地址
    private String description;//描述
    private Date createTime;//新建时间
    private Integer status = 2;//状态:0、禁用；1、正常；2、待审核, 默认：2、待审核
    private List<Role> roles = new ArrayList<Role>();//  关联角色集合

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvator() {
        return avator;
    }

    public void setAvator(String avator) {
        this.avator = avator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public boolean hasRole(String rolekey){    	
  		List<Role> userroles= this.getRoles();
  		if(userroles==null || userroles.size()==0)
  			return false;
  		
  		boolean flag=false;
  		for(Role r:userroles)
  		{
  			if(r.getRoleKey().equals(rolekey)){
  				flag=true;
  				break;
  			}
  		}
  		return flag;
    }
}
