<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://tanghom.cn/mytags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>运行中流程</title>
	<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
<section class="main-content">
	<div class="content-wrap">
		<section class="panel panel-default">
			<div class="panel-heading">
				<b>运行中流程</b>
				<small class="pull-right">
					<a href="javascript:window.location.reload();" class="pd-r-xs"><i class="fa fa-refresh"></i> 刷新</a>
				</small>
			</div>
			<div class="panel-body">
				<table class="table">
					<tr>
						<th>执行IDssss</th>
						<th>流程实例ID</th>
						<th>流程定义ID</th>
						<th>当前节点</th>
						<th>是否挂起</th>
						<th>操作</th>
					</tr>
					<tbody>
					<c:forEach items="${page.items }" var="p">
						<c:set var="pdid" value="${p.processDefinitionId }" />
						<c:set var="activityId" value="${p.activityId }" />
						<tr>
							<td>${p.id }</td>
							<td>${p.processInstanceId }</td>
							<td>${p.processDefinitionId }</td>
							<td><a class="trace" href='#' pid="${p.id }" pdid="${p.processDefinitionId}" title="点击查看流程图">${p.name}</a></td>
							<td>${p.suspended }</td>
							<td>
								<c:if test="${p.suspended }">
									<a href="update/active/${p.processInstanceId}">激活</a>
								</c:if>
								<c:if test="${!p.suspended }">
									<a href="update/suspend/${p.processInstanceId}">挂起</a>
								</c:if>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${empty page}">
						<td colspan="6" class="td-center">无相关数据</td>
					</c:if>
					</tbody>
				</table>
				<div class="clearfix">
					<div class="pull-right">
						<mt:pager pageSize="${page.size}" pageNo="${page.index}" url="" recordCount="${page.totalRecord}" />
					</div>
				</div>
			</div>
		</section>
	</div>
</section>
</body>
</html>
