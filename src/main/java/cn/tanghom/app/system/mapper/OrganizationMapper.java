package cn.tanghom.app.system.mapper;

import cn.tanghom.app.system.model.Organization;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 部门对象映射
 *
 * @author tanghom<tanghom@qq.com> 2016/4/24
 */
@Repository
public interface OrganizationMapper {

    /**
     * 保存一个对象
     *
     * @param organization
     */
    void save(Organization organization);

    /**
     * 修改一个对象
     *
     * @param organization
     */
    void update(Organization organization);

    /**
     * 删除一个对象
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 查询一个对象
     *
     * @param id
     * @return
     */
    Organization findById(Long id);

    /**
     * 查询全部对象
     *
     * @return
     */
    List<Organization> findListByParams(@Param(value = "status") Integer status);

    /**
     * 根据父级节点Id查询改父级节点Id下的节点
     *
     * @param parentId
     * @param status
     * @return
     */
    List<Organization> queryListByParentId(@Param(value = "parentId") Long parentId, @Param(value = "status") Integer status);
    
    /**
     * 根据父节点Name查询该节点Name的节点  
     * @return
     */
    Organization findByName(@Param(value = "name") String name,@Param(value = "parentId") Long parentId);
}
